//
//  Webservice+EndPoints.swift
//  NewProject
//
//  Created by Preeti dhankar on 30/08/18.
//  Copyright © 2018 Preeti dhankar. All rights reserved.
//

import Foundation
import UIKit
//let baseUrl = "http://karaoke.yilstaging.com/api/"
let baseUrl = "http://voicetm.com/api/"
let baseUrl_chat = "http://anothercz.yilstaging.com/chat/"
let tmdb_image_url = "___"

extension WebServices {
    
    enum EndPoint : String {
        
        case register_user
        case check_username_is_valid     = "check_username_is_valid"
        case check_email                 =  "check_email"
        case login
        case social_login
        case forget_password
        case send_otp
        case reset_password
        case change_password
        case get_content
        case get_user_profile
        case get_follower_profile
        case user_subscription_plan
        case set_subscription_plan
        case logout_delete
        case update_user_profile
        case get_follower_following
        case get_music_category
        case add_user_music_choice
        case upload_video
        case get_user_videos
        case get_video_details
        case get_home_data
        case favorite_add_remove
        case post_likes_unlike
        case report_post
        case get_comment_list
        case post_comment
        case post_comment_reply
        case comment_likes_unlike
        case comment_reply_likes_unlike
        case follow_unfollow
        case get_fav_list
        case get_friend_follower_following
        case report_user
        case block_unblock
        case get_block_list
        case get_notification_list
        case may_be_later
        case video_views
        case delete_video
        case explore_screen
        case get_song_list
        case search_song
        case get_like_list
        case get_sound_effects
        
        var path : String {
            let url = baseUrl
            return url + self.rawValue
        }
        
        var pathLocalServer : String {
            let url = tmdb_image_url
            return url + self.rawValue
        }        
    }
}

struct KeyShare {
//    let DynamicLinkDomain = "https://karaoke2.page.link/jdF1"
    let DynamicLinkDomain = "https://karaoke2.page.link"
    let AppStoreID = ""
    let AppStoreMinVersion = "1.0"
    let iosIdentifire = ""
    let PlayStoreMinVersion = "1.0"
    let androidIdentifire = ""
}
