//
//  VideoView.swift
//  Just Dance
//
//  Created by YATIN  KALRA on 20/11/19.
//  Copyright © 2019 Ankur. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
protocol VideoViewDelegate {
    func playVideFunc(AVPlayerLayer:AVPlayer)
    func errorFunc(AVPlayerLayer:AVPlayer)
    func otherFunc(AVPlayerLayer:AVPlayer)
}

class VideoView: UIView {
    
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    var isLoop: Bool = false
    var playbackSlider = CustomSlider()
    let btnHide: UIButton = UIButton()
    let btn: UIButton = UIButton()
    let btnPlay: UIButton = UIButton()
    let currentTimeLabel = UILabel()
    //    let remainingTimeLabel = UILabel()
    
    var delegate : VideoViewDelegate?
    private var playerItemContext = 0
    typealias currentTimerD = (Double,Double) -> Void
    var updateTimeStatus : currentTimerD?
    var videoPlaying = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        self.addCustomView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    func addCustomView() {
        
        currentTimeLabel.frame = CGRect(x: 10, y: self.frame.height - 30, width: 100, height: 20)
        currentTimeLabel.backgroundColor = .clear
        currentTimeLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        currentTimeLabel.text = ""
        currentTimeLabel.font = UIFont.systemFont(ofSize: 11)
        //        self.addSubview(currentTimeLabel)
        
        btnHide.frame = self.frame
        btnHide.addTarget(self, action: #selector(btnHideOther(_:)), for: .touchUpInside)
        btnHide.backgroundColor = .clear
        self.addSubview(btnHide)
        
        let heightb = (self.frame.width / 2) / 2
        btn.frame.size = CGSize(width: 40, height: 40)
        btn.center = self.center
        btn.center.x = heightb
        btn.addTarget(self, action: #selector(playbackback10Sec(_:)), for: .touchUpInside)
        btn.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        //        self.addSubview(btn)
        
        
        
        btnPlay.frame.size = CGSize(width: 40, height: 40)
        btnPlay.center = self.center
        btnPlay.setImage(#imageLiteral(resourceName: "loginPlay"), for: .normal)
        btnPlay.setImage(#imageLiteral(resourceName: "play3"), for: .selected)
        btnPlay.addTarget(self, action: #selector(playVideo(_:)), for: .touchUpInside)
        self.addSubview(btnPlay)
    }
    func configure(url: String,autoPlay:Bool = false) {
        if let videoURL = URL(string: url) {
            let playerItem:AVPlayerItem = AVPlayerItem(url: videoURL)
            //            player = AVPlayer(url: videoURL)
            player = AVPlayer(playerItem: playerItem)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = bounds
            //            playerLayer?.videoGravity = AVLayerVideoGravity.resize
            playerLayer?.videoGravity = .resizeAspectFill
            
            
            if nil != self.playerLayer {
                //            if let playerLayer = self.playerLayer {
                
                self.playerLayer?.frame.size.width = self.frame.size.width
                //                let height = self.frame.width * 9 / 16
                //                playerLayer.frame.size.height = height
                layer.addSublayer(self.playerLayer!)
                if autoPlay == true {
                    self.player?.play()
                    self.videoPlaying = true
                }
                self.addCustomView()
                
                self.playbackSlider.addTarget(self, action: #selector(playbackSliderValueChanged(_:)), for: .valueChanged)
                //            self.playbackSlider.addTarget(self, action: #selector(startEditingSlider), for: .touchDown)
                //            self.playbackSlider.addTarget(self, action: #selector(stopEditingSlider), for: [.touchUpInside, .touchUpOutside])
                self.playbackSlider.value = 0.0
                self.playbackSlider.trackHeight = 5
                self.playbackSlider.thumbRadius = 15
                self.playbackSlider.setThumbImage()
                self.playbackSlider.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
                playbackSlider.frame = CGRect(x:10, y:self.frame.size.height - 55, width:self.frame.size.width - 20, height:20)
                playbackSlider.minimumValue = 0
                
                let duration : CMTime = playerItem.asset.duration
                let seconds : Float64 = CMTimeGetSeconds(duration)
                
                playbackSlider.maximumValue = Float(seconds)
                playbackSlider.isContinuous = true
                //                self.addSubview(playbackSlider)
                self.updateTime()
                self.timerS(hide: true)
                playerItem.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old,.new], context: &playerItemContext)
            }
            NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
        }
    }
    
    
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        
        // Only handle observations for the playerItemContext
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath,
                               of: object,
                               change: change,
                               context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                self.delegate?.playVideFunc(AVPlayerLayer: player!)
                break
            case .failed:
                self.delegate?.errorFunc(AVPlayerLayer: player!)
                break
            // Player item failed. See error.
            case .unknown:
                self.delegate?.otherFunc(AVPlayerLayer: player!)
                break
            // Player item is not yet ready.
            @unknown default:
                self.delegate?.otherFunc(AVPlayerLayer: player!)
                break
            }
        }
    }
    @objc func startEditingSlider(){
        player?.pause()
        self.videoPlaying = false
    }
    @objc func playbackSliderValueChanged(_ playbackSlider1:CustomSlider)
    {
        let seconds : Int64 = Int64(playbackSlider1.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        
        player!.seek(to: targetTime)
        
        if player!.rate == 0 {
            player?.play()
            self.videoPlaying = true
        }
    }
    @objc func updateTime(){
        if let p = player?.currentItem {
            let currentTime = p.currentTime().seconds
            let duration = playbackSlider.maximumValue
            updateTimeStatus?(currentTime,Double(duration))
            //            let progress = duration - Float(currentTime)
            self.playbackSlider.value = Float(currentTime)
            //            self.playbackSlider.sendActions(for: .valueChanged)
            
            if self.videoPlaying == true {
                self.perform(#selector(updateTime), with: nil, afterDelay: 1.0)
            }
            let currentTimeS = Int(currentTime)
            //            self.currentTimeLabel.text = self.ytk_secondsToCounter(currentTimeS)
            self.currentTimeLabel.text = self.ytk_secondsToCounter(currentTimeS) + " / " + self.ytk_secondsToCounter(Int(duration))
        }
    }
    func paddedNumber(_ number : Int) -> String{
        if(number < 0){
            return "00"
        }else if(number < 10){
            return "0\(number)"
        }else{
            return String(number)
        }
    }
    func ytk_secondsToCounter(_ seconds : Int) -> String {
        
        let time = self.secondsToHoursMinutesSeconds(seconds)
        
        let minutesSeconds = "\(self.paddedNumber(time.minutes)):\(self.paddedNumber(time.seconds))"
        
        if(time.hours == 0){
            return minutesSeconds
        }else{
            return "\(self.paddedNumber(time.hours)):\(minutesSeconds)"
        }
        
    }
    @IBAction func playbackback10Sec(_ sender:UIButton)
    {
        var currentTime : Int64 = Int64(playbackSlider.value)
        if currentTime > 10 {
            currentTime = currentTime - 10
            playbackSlider.value = Float(currentTime)
            let targetTime:CMTime = CMTimeMake(value: currentTime, timescale: 1)
            player!.seek(to: targetTime)
            if player!.rate == 0 {
                player?.play()
                self.videoPlaying = true
            }
        }
    }
    @IBAction func btnHideOther(_ sender: UIButton){
        self.hideOther()
        self.timerS(hide: true)
    }
    var timerHide : Timer? = nil
    func timerS(hide:Bool)  {
        timerHide?.invalidate()
        timerHide = nil
        if hide == false {
            return
        }
        if !btnPlay.isHidden {
            timerHide = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (_) in
                self.hideOther()
            }
        }
    }
    func hideOther(){
        playbackSlider.isHidden = !playbackSlider.isHidden
        btn.isHidden = !btn.isHidden
        btnPlay.isHidden = !btnPlay.isHidden
        currentTimeLabel.isHidden = !currentTimeLabel.isHidden
    }
    
    func secondsToHoursMinutesSeconds (_ seconds : Int) -> (hours : Int, minutes : Int, seconds : Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    @IBAction func playVideo(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        if player?.timeControlStatus != AVPlayer.TimeControlStatus.playing {
            self.player?.play()
            self.videoPlaying = true
            //            sender.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        }else{
            self.player?.pause()
            self.videoPlaying = false
            //            sender.setImage(#imageLiteral(resourceName: "play3"), for: .normal)
        }
        timerS(hide: true)
    }
    @IBAction func pauseVideo(_ sender:UIButton) {
        self.player?.play()
        self.videoPlaying = true
        timerS(hide: true)
    }
    func play() {
        if player?.timeControlStatus != AVPlayer.TimeControlStatus.playing {
            player?.play()
            self.videoPlaying = true
            btnPlay.isSelected = true
            //            btnPlay.setImage(#imageLiteral(resourceName: "play3"), for: .normal)
            self.delegate?.playVideFunc(AVPlayerLayer: player!)
        }
    }
    func replay(){
        self.player?.seek(to: .zero)
        player?.play()
        self.videoPlaying = true
    }
    func pause() {
        player?.pause()
        self.videoPlaying = false
        btnPlay.isSelected = false
        //        btnPlay.setImage(#imageLiteral(resourceName: "play"), for: .normal)
    }
    
    func stop() {
        //        player?.pause()
        self.videoPlaying = false
        
        btnPlay.isSelected = false
        DispatchQueue.main.async {
            self.player?.pause()
            self.player?.seek(to: CMTime.zero)
            self.playerLayer?.removeFromSuperlayer()
        }
    }
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        if isLoop {
            player?.pause()
            self.videoPlaying = false
            player?.seek(to: CMTime.zero)
            player?.play()
            self.videoPlaying = true
        }
    }
}
