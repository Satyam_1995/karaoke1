
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "RejectionFilter.h"


@interface AudioController : NSObject {
    AudioUnit               _rioUnit;
    BOOL                    _audioChainIsBeingReconstructed;
    RejectionFilter*      _rejectionFilter;
}

@property (nonatomic, assign) BOOL muteAudio;
@property (nonatomic, assign, readonly) BOOL audioChainIsBeingReconstructed;

- (void)    startIOUnit;
- (OSStatus)    stopIOUnit;
- (double)      sessionSampleRate;

@end
