//
//  UIViewControllerExtension.swift
//  WashApp
//
//  Created by Saurabh Shukla on 19/09/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit
import AssetsLibrary
import AVFoundation
import Photos
import MobileCoreServices
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    
    typealias ImagePickerDelegateController = (UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate)
    
    func captureImage(delegate controller: ImagePickerDelegateController,
                      photoGallery: Bool = true,
                      camera: Bool = true,
                      mediaType : [String] = [kUTTypeImage as String]) {
        
        let chooseOptionText =  LocalizedString.chooseOptions.localized
        let alertController = UIAlertController(title: chooseOptionText, message: nil, preferredStyle: .actionSheet)
        
        if photoGallery {
            
            let chooseFromGalleryText =  LocalizedString.chooseFromGallery.localized
            let alertActionGallery = UIAlertAction(title: chooseFromGalleryText, style: .default) { _ in
                self.checkAndOpenLibrary(delegate: controller, mediaType: mediaType)
            }
            alertController.addAction(alertActionGallery)
        }
        
        if camera {
            
            let takePhotoText =  LocalizedString.takePhoto.localized
            let alertActionCamera = UIAlertAction(title: takePhotoText, style: .default) { action in
                self.checkAndOpenCamera(delegate: controller, mediaType: mediaType)
            }
            alertController.addAction(alertActionCamera)
        }
        
        let cancelText =  LocalizedString.cancel.localized
        let alertActionCancel = UIAlertAction(title: cancelText, style: .cancel) { _ in
        }
        alertController.addAction(alertActionCancel)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func checkAndOpenCamera(delegate controller: ImagePickerDelegateController,mediaType : [String] = [kUTTypeImage as String]) {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
            
        case .authorized:
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = controller
            imagePicker.mediaTypes = mediaType
            imagePicker.videoMaximumDuration = 15
            let sourceType = UIImagePickerController.SourceType.camera
            if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                
                imagePicker.sourceType = sourceType
                imagePicker.allowsEditing = false
                
                if imagePicker.sourceType == .camera {
                    imagePicker.showsCameraControls = true
                }
                controller.present(imagePicker, animated: true, completion: nil)
                
            } else {
                
                let cameraNotAvailableText = LocalizedString.cameraNotAvailable.localized
                self.showAlert(title: "Error", msg: cameraNotAvailableText)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { granted in
                
                if granted {
                    
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = controller
                        imagePicker.mediaTypes = mediaType
                        imagePicker.videoMaximumDuration = 15
                        
                        let sourceType = UIImagePickerController.SourceType.camera
                        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                            
                            imagePicker.sourceType = sourceType
                            if imagePicker.sourceType == .camera {
                                imagePicker.allowsEditing = false
                                imagePicker.showsCameraControls = true
                            }
                            controller.present(imagePicker, animated: true, completion: nil)
                            
                        } else {
                            let cameraNotAvailableText = LocalizedString.cameraNotAvailable.localized
                            self.showAlert(title: "Error", msg: cameraNotAvailableText)
                        }
                    }
                }
            })
            
        case .restricted:
            alertPromptToAllowCameraAccessViaSetting(LocalizedString.restrictedFromUsingCamera.localized)
            
        case .denied:
            alertPromptToAllowCameraAccessViaSetting(LocalizedString.changePrivacySettingAndAllowAccessToCamera.localized)
        }
    }
    
    func checkAndOpenLibrary(delegate controller: ImagePickerDelegateController,mediaType : [String] = [kUTTypeImage as String]) {
        
        let authStatus = PHPhotoLibrary.authorizationStatus()
        switch authStatus {
            
        case .notDetermined:
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = controller
            imagePicker.mediaTypes = mediaType
            let sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.sourceType = sourceType
            imagePicker.allowsEditing = false
            imagePicker.videoMaximumDuration = 15
            
            controller.present(imagePicker, animated: true, completion: nil)
            
        case .restricted:
            alertPromptToAllowCameraAccessViaSetting(LocalizedString.restrictedFromUsingLibrary.localized)
            
        case .denied:
            alertPromptToAllowCameraAccessViaSetting(LocalizedString.changePrivacySettingAndAllowAccessToLibrary.localized)
            
        case .authorized,.limited:
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = controller
            let sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.sourceType = sourceType
            imagePicker.allowsEditing = false
            imagePicker.mediaTypes = mediaType
            imagePicker.videoMaximumDuration = 15
            
            controller.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func alertPromptToAllowCameraAccessViaSetting(_ message: String) {
        
        let alertText = LocalizedString.alert.localized
        let cancelText = LocalizedString.cancel.localized
        let settingsText = LocalizedString.settings.localized
        
        let alert = UIAlertController(title: alertText, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: settingsText, style: .default, handler: { (action) in
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: cancelText, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///Adds Child View Controller to Parent View Controller
    func add(childViewController:UIViewController){
        
        self.addChild(childViewController)
        childViewController.view.frame = self.view.bounds
        self.view.addSubview(childViewController.view)
        childViewController.didMove(toParent: self)
    }
    
    ///Removes Child View Controller From Parent View Controller
    var removeFromParent:Void{
        
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    ///Updates navigation bar according to given values
    func updateNavigationBar(withTitle title:String? = nil, leftButton:UIBarButtonItem? = nil, rightButton:UIBarButtonItem? = nil, tintColor:UIColor? = nil, barTintColor:UIColor? = nil, titleTextAttributes: [NSAttributedString.Key : Any]? = nil){
        
        self.navigationController?.isNavigationBarHidden = false
        if let tColor = barTintColor{
            self.navigationController?.navigationBar.barTintColor = tColor
        }
        if let tColor = tintColor{
            self.navigationController?.navigationBar.tintColor = tColor
        }
        if let button = leftButton{
            self.navigationItem.leftBarButtonItem = button;
        }
        if let button = rightButton{
            self.navigationItem.rightBarButtonItem = button;
        }
        if let ttle = title{
            self.title = ttle
        }
        if let ttleTextAttributes = titleTextAttributes{
            self.navigationController?.navigationBar.titleTextAttributes =   ttleTextAttributes
        }
    }
    
    
    
    
    func popToSpecificViewController(atIndex index:Int, animated:Bool = true) {
        
        if let navVc = self.navigationController, navVc.viewControllers.count > index{
            
            _ = self.navigationController?.popToViewController(navVc.viewControllers[index], animated: animated)
        }
    }
    
    func showAlert( title : String = "", msg : String,_ completion : (()->())? = nil) {
        
        let alertViewController = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: LocalizedString.ok.localized, style: UIAlertAction.Style.default) { (action : UIAlertAction) -> Void in
            
            alertViewController.dismiss(animated: true, completion: nil)
            completion?()
        }
        
        alertViewController.addAction(okAction)
        
        self.present(alertViewController, animated: true, completion: nil)
        
    }
    
    /// Start Loader
    func startNYLoader() {
        startAnimating(CGSize(width: 50, height: 50), type: NVActivityIndicatorType.ballRotateChase, color: AppColors.loaderColor, backgroundColor: AppColors.loaderBkgroundColor)
    }
}
extension UIViewController {
    func  customizeColor(string: String, color: UIColor) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.foregroundColor : color ])
    }
    func  customizeFont(string: String, font: UIFont) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.font : font ])
    }
    func  customizeFont(string: String, font: UIFont,color: UIColor) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.font : font ,NSAttributedString.Key.foregroundColor : color ])
    }
    func shadowText(string: String) -> NSMutableAttributedString {
        
        let shadow = NSShadow()
        shadow.shadowBlurRadius = 3
        shadow.shadowOffset = CGSize(width: 3, height: 3)
        shadow.shadowColor = UIColor.gray
        
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.shadow : shadow ])
    }
    
    func strokeWidth(string: String) -> NSMutableAttributedString{
        return NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.strokeWidth: 2])
    }
    
    func background(string: String)-> NSMutableAttributedString {
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.backgroundColor : UIColor.lightGray ])
    }
    
    func underline(string: String) -> NSMutableAttributedString{
        return NSMutableAttributedString(string: string, attributes:
            [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.thick.rawValue ])
    }
}
extension UIViewController {
    func localToUTC(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = Calendar.current
        dateFormatter.timeZone = TimeZone.current
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "H:mm:ss"
        
            return dateFormatter.string(from: date)
        }
        return nil
    }

    func utcToLocal(dateStr: String, timeFormat: String = "H:mm:ss",to:String = "h:mm a") -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = timeFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = to
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    func abbreviationToUTC(dateStr: String,abbreviation:String) -> String? {

//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "h:mm a"
//        dateFormatter.calendar = Calendar.current
//        dateFormatter.timeZone = TimeZone(abbreviation: abbreviation)
//
//        if let date = dateFormatter.date(from: dateStr) {
//            dateFormatter.timeZone = TimeZone(abbreviation: "UTC+05:30")
//            dateFormatter.dateFormat = "H:mm:ss"
//
//            return dateFormatter.string(from: date)
//        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = Calendar.current
        dateFormatter.timeZone = TimeZone(abbreviation: abbreviation)

        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "H:mm:ss"

            return dateFormatter.string(from: date)
        }
        return nil
    }
    func UTCtoabbreviation(dateStr: String,abbreviation:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.calendar = Calendar.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone(abbreviation: abbreviation)
            dateFormatter.dateFormat = "h:mm a"
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
    func locatiToday(time:String , timeFor : String = "yyyy-MM-dd HH:mm:ss" ) -> Int? {
        
        let dateFormatte = DateFormatter()
        dateFormatte.dateFormat = timeFor
        let theFirstDate = Date()

        dateFormatte.timeZone = TimeZone(abbreviation: "UTC")
        
        if let theSecondDate = dateFormatte.date(from: time) {
            dateFormatte.timeZone = TimeZone.current
            dateFormatte.dateFormat = timeFor
            let theComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second ], from: theSecondDate, to: theFirstDate)
            if let theNumbe = theComponents.year, theNumbe > 0 {
                return theNumbe
            }else if let theNumbe = theComponents.month, theNumbe > 0 {
                return theNumbe
            }else if let theNumbe = theComponents.day, theNumbe > 0 {
                return theNumbe
            }else if let theNumbe = theComponents.hour, theNumbe > 0 {
                return theNumbe
            }else if let theNumbe = theComponents.minute, theNumbe > 0 {
                return theNumbe
            }else if let theNumbe = theComponents.second, theNumbe > 0 {
                return theNumbe
            }
        }
        return nil
    }
}
extension UIView{
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * -2)
        rotation.duration = 2
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
extension UIViewController {
    func stringToDate(date:String) -> String {
        if let dateStr = self.utcToLocal(dateStr: date, timeFormat: "yyyy-MM-dd HH:mm:ss",to:"yyyy-MM-dd HH:mm:ss") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let date = dateFormatter.date(from: dateStr) {
                return self.dayDifference123(from: date)
            }
        }
        return ""
    }
    func convertDateToString(inputDate: Date,former: String = "yyyy-MM-dd HH:mm:ss") -> String {
         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = former
        return olDateFormatter.string(from: inputDate)
    }
    func convertDateFormat(inputDate: String,former: String = "yyyy-MM-dd HH:mm:ss") -> Date {
         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = former
        return olDateFormatter.date(from: inputDate)!
    }
    func dayDifference(from interval : Date) -> String {
        let calendar = Calendar.current
//        let date = Date(timeIntervalSince1970: interval)
        let date = interval
        let startOfNow = calendar.startOfDay(for: Date())
        let startOfTimeStamp = calendar.startOfDay(for: date)
        let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
        let day = components.day!
        if abs(day) < 2 {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeStyle = .none
            formatter.doesRelativeDateFormatting = true
            return formatter.string(from: date)
        } else if day > 1 {
            return "In \(day) days"
        } else {
            return "\(-day) days ago"
        }
    }
    func dayDifference123(from interval : Date) -> String
    {
        let calendar = Calendar.current
//        let date = Date(timeIntervalSince1970: interval)
        let date = interval
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) {
            return Date.getCustomTimeOnly(formet: "hh:mm a", date: date)
//            let formatter = DateFormatter()
//            formatter.dateStyle = .short
//            formatter.timeStyle = .none
//            formatter.doesRelativeDateFormatting = true
//            return formatter.string(from: date)
//            return "Today"
        }
        else if calendar.isDateInTomorrow(date) { return "Tomorrow" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 {
                let components = calendar.dateComponents([.month], from: startOfNow, to: startOfTimeStamp)
                let mon = components.month ?? 0
                if mon == 0 {
                    return "\(-day) days ago"
                }else if mon > 0 && mon <= 12 {
                    return Date.getCustomTimeOnly(formet: "dd, MMM", date: date)
                }else if mon < 0 && mon >= -12 {
                    return Date.getCustomTimeOnly(formet: "dd, MMM", date: date)
                }else{
                    return Date.getCustomTimeOnly(formet: "MMM, yyyy", date: date)
                }
                return "\(-day) days ago"
            }
//            if day < 1 { return "\(-day) days ago" }
            else { return "In \(day) days" }
        }
    }
}
