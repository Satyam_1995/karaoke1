//
//  UIView+Extension.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 16/02/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    @IBInspectable var applyRedious: CGFloat {
        get {
            return self.layer.shadowRadius
        }
        set {
            self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = newValue
            self.layer.masksToBounds = false
            self.layer.cornerRadius = newValue
        }
    }
    func applyBackShadow(Radius:CGFloat = 15.0) {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = Radius
        self.layer.masksToBounds = false
        self.layer.cornerRadius = Radius
    }
}
extension UIView{
    //MARK:- To set the corner radius of Uiview.
    func CircleOfView()  {
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
    
    func dropShadow() {
           self.layer.shadowColor = UIColor.lightGray.cgColor
           self.layer.shadowOpacity = 1
           self.layer.shadowOffset = CGSize.zero
           self.layer.shadowRadius = 3
       }
    func borderOfView(border:CGFloat,color:CGColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))  {
        self.layer.borderColor = color
          self.layer.borderWidth = border
           self.clipsToBounds = true
       }
}
