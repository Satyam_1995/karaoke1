//
//  AppUserDefaults.swift
//  AppUserDefaults
//
//  Created by Gurdeep on 15/12/16.
//  Copyright © 2016 Gurdeep. All rights reserved.
//

import Foundation
import SwiftyJSON

enum AppUserDefaults {
    
    static func value(forKey key: Key,
                      file : String = #file,
                      line : Int = #line,
                      function : String = #function) -> JSON {
        
        guard let value = UserDefaults.standard.object(forKey: key.rawValue) else {
            
            debugPrint("No Value Found in UserDefaults\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
            
            return JSON.null
        }
        
        return JSON(value)
    }
    
//    static func value<T>(forKey key: Key,
//                      fallBackValue : T,
//                      file : String = #file,
//                      line : Int = #line,
//                      function : String = #function) -> JSON {
//
//        guard let value = UserDefaults.standard.object(forKey: key.rawValue) else {
//
//            debugPrint("No Value Found in UserDefaults\nFile : \(file) \nFunction : \(function)")
//            return JSON(fallBackValue)
//        }
//
//        return JSON(value)
//    }
    
    static func save(value : Any, forKey key : Key) {
        
        UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func removeValue(forKey key : Key) {
        
        UserDefaults.standard.removeObject(forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func removeAllValues() {
        
        let tutorialDisplayed = AppUserDefaults.value(forKey: .tutorialDisplayed).boolValue
        
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        UserDefaults.standard.synchronize()
        
        AppUserDefaults.save(value: tutorialDisplayed, forKey: .tutorialDisplayed)
    }
}

// MARK:- KEYS
//==============
extension AppUserDefaults {
    
    enum Key : String {
        
        case tutorialDisplayed
        case authorization
        //case userId
//        case accesstoken
        case fullUserProfile
        case actorDetailsCell
        case loginDetailsData
        
    }
}


//MARK:-


extension UserDefaults {
    enum Key : String {
        
        case loggedInUserId
        
        case userDetails
        case language
        case auth
        case isLanguageScreenDisplayed
        case currentCountryCode
        case countryiSOCode
        case recentPropertySearches
        case allPropertiesFilter
        case propertyApartmentsFilter
        case myPropertiesFilter
        case recentGroupSearchesForPaci
        case recentGroupSearchesForPropertyName
        case recentGroupSearchesForLlName
        case recentGroupSearchesForLocation
        case currentUserCookies
        
        case xAuthToken
    }
}
extension UserDefaults {

    // MARK: - User Defaults

    /**
     sets/adds object to NSUserDefaults

     - parameter aObject: object to be stored
     - parameter defaultName: key for object
     */
    class func setObject(_ value: AnyObject?, forKey defaultName: String) {
        UserDefaults.standard.set(value, forKey: defaultName)
        UserDefaults.standard.synchronize()
    }

    /**
     gives stored object in NSUserDefaults for a key

     - parameter defaultName: key for object

     - returns: stored object for key
     */
    class func objectForKey(_ defaultName: String) -> AnyObject? {
        return UserDefaults.standard.object(forKey: defaultName) as AnyObject?
    }

    /**
     removes object from NSUserDefault stored for a given key

     - parameter defaultName: key for object
     */
    class func removeObjectForKey(_ defaultName: String) {
        UserDefaults.standard.removeObject(forKey: defaultName)
        UserDefaults.standard.synchronize()
    }

    class func setBool(_ value: Bool?, forKey defaultName: String) {
        UserDefaults.standard.set(value, forKey: defaultName)
        UserDefaults.standard.synchronize()
    }
}


extension UserDefaults {

}
extension UserDefaults {
    
    ///Saves the given object with given key in the 'UserDefaults'
    class func setObject(_ object:Any?, forKey key:String?){
        
        guard let ky = key, let objct = object else { return }
        UserDefaults.standard.set(objct, forKey: ky)
        self.standard.synchronize()
    }
    
    ///Gets value ( if any ) from user defaults
    class func getObject(forKey key:String?)-> Any?{
        
        guard let ky = key else { return nil}
        return self.standard.object(forKey: ky)
    }
    ///Removes object ( if any ) from user defaults
    class func removeObject(forKey key:String?){
        
        guard let ky = key else { return }
        UserDefaults.standard.removeObject(forKey: ky)
        self.standard.synchronize()
    }
    
    ///Saves the given color with given key in the 'UserDefaults'
    class func setColor(_ color: UIColor?, forKey key: String?) {
        
        guard let ky = key, let clr = color else { return }
        self.standard.set(NSKeyedArchiver.archivedData(withRootObject: clr), forKey: ky)
        self.standard.synchronize()
    }
    
    ///Gets color ( if any ) from user defaults
    class func getColor(forKey key: String?) -> UIColor? {
        guard let ky = key, let data = self.standard.data(forKey: ky) else { return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? UIColor
    }
    class func clear() {
        
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        UserDefaults.standard.synchronize()
    }
    
    class func saveCustomObject(customObject object: Any?, forKey key: String?) {
        guard let ky = key, let obj = object else { return }
        self.standard.set(NSKeyedArchiver.archivedData(withRootObject: obj), forKey: ky)
        self.standard.synchronize()
    }
    
    class func getCustomObject(forKey key: String?) -> Any? {
        guard let ky = key, let data = self.standard.data(forKey: ky) else { return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: data)
    }
    
    func save<T:Codable>(customObject object: T, inKey key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object) {
            self.set(encoded, forKey: key)
        }
    }
    
    func retrieve<T:Codable>(objectType type:T.Type, fromKey key: String) -> T? {
      if let data = self.data(forKey: key) {
            let decoder = JSONDecoder()
            if let object = try? decoder.decode(type, from: data) {
                return object
            }else {
                print("Couldnt decode object")
                return nil
            }
        }else {
            print("Couldnt find key")
            return nil
        }
    }
}


