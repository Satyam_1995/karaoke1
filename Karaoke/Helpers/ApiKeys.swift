//
//  ApiKeys.swift
//  Caviar
//
//  Created by YATIN  KALRA on 20/08/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
import UIKit

enum ApiKey {
    static var code: String { return "CODE" }
    static var user_id : String { return "user_id" }
    
    static var mobile : String { return "mobile" }
    static var username : String { return "username" }
    static var name : String { return "name" }
    static var user_name : String { return "user_name" }
    static var email : String { return "email" }
    static var password : String { return "password" }
    static var old_password : String { return "old_password" }
    
    static var bio : String { return "bio" }
    static var user_image : String { return "user_image"}
    static var fcm_token : String { return "fcm_token" }
    
    static var social_id : String { return "social_id" }
    static var image : String { return "image" }
    static var social_type : String { return "social_type" }
    
    static var first_name : String { return "first_name" }
    static var last_name : String { return "last_name" }
    static var dob : String { return "dob" }
    static var birthdaypublic : String { return "birthdaypublic" }
    static var address : String { return "address" }
    static var lat : String { return "lat" }
    static var long : String { return "long" }
    static var gender : String { return "gender" }
    static var interestedin : String { return "interestedin" }
    static var Instagram : String { return "Instagram" }
    static var Photo1 : String { return "Photo1" }
    static var Photo2 : String { return "Photo2" }
    static var Photo3 : String { return "Photo3" }
    static var Photo4 : String { return "Photo4" }
    static var Photo5 : String { return "Photo5" }
    static var height : String { return "height" }
    static var type : String { return "type" }
    
    static var video : String { return "video" }
    static var lyrics : String { return "lyrics" }
    static var artist_name : String { return "artist_name" }
    static var song_name : String { return "song_name" }
    static var video_thumb : String { return "video_thumb" }
    static var old_video_id : String { return "old_video_id" }
    
    static var video_id : String { return "video_id" }
    static var min_range  : String { return "min_range" }
    static var max_range  : String { return "max_range" }
    static var favorite_status  : String { return "favorite_status" }
    static var like_status  : String { return "like_status" }
    static var followers_id  : String { return "followers_id" }
    static var comment  : String { return "comment" }
    static var comment_id  : String { return "comment_id" }
    static var reply_id  : String { return "reply_id" }
    static var follow_status  : String { return "follow_status" }
    static var friend_id  : String { return "friend_id" }
    static var music_cat_id  : String { return "music_cat_id" }
    static var music_category  : String { return "music_category" }
    static var reported_userid  : String { return "reported_userid" }
    static var report_msg  : String { return "report_msg" }
    static var followerid  : String { return "followerid" }
    static var last_id  : String { return "last_id" }
    static var notification_id  : String { return "notification_id" }
    static var status  : String { return "status" }
    static var planid  : String { return "planid" }
    static var tx_id  : String { return "tx_id" }
    static var txt  : String { return "txt" }
    static var check  : String { return "check" }
    static var expire_on  : String { return "expire_on" }
    
    
}
//MARK:- Api Code
//=======================
enum ApiCode {
    
    static var success: Int { return 200 } // Success
    static var unauthorizedRequest: Int { return 206 } // Unauthorized request
    static var headerMissing: Int { return 207 } // Header is missing
    static var phoneNumberAlreadyExist: Int { return 208 } // Phone number alredy exists
    static var requiredParametersMissing: Int { return 418 } // Required Parameter Missing or Invalid
    static var fileUploadFailed: Int { return 421 } // File Upload Failed
    static var pleaseTryAgain: Int { return 500 } // Please try again
    static var tokenExpired: Int { return 401 } // Token expired refresh token needed to be generated
    
}

//MARK:- Api Response
//=======================
enum ApiResponse {

    static var response: Bool { return true } // response
   
}

