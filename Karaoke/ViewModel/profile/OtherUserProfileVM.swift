//
//  OtherUserProfileVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 04/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation


class OtherUserProfileVM {
    
    weak var vc : OtherUserProfileVC?
    var Profiledetail : FriendProfileData?
    var userVideoList : [User_videos] = []

    func get_follower_profile(user_id:String = UserDetail.shared.getUserId(),friend_id:String)  {
//        http://karaoke.yilstaging.com/api/get_follower_profile
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        parameters[ApiKey.friend_id] = friend_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_follower_profile, loader: true) { (json) in
        } successData: { (data) in
            do {
                self.Profiledetail = try JSONDecoder().decode(FriendProfileData.self, from: data)
                if self.Profiledetail?.status == "success" {
                    self.vc?.setData(new:true)
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func get_user_videos(user_id:String = UserDetail.shared.getUserId())  {
//        http://karaoke.yilstaging.com/api/get_user_videos
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_user_videos, loader: true) { (json) in
            
        } successData: { (data) in
            do {
                let dict = try JSONDecoder().decode(userVideoListModel.self, from: data)
                
                if dict.status == "success" {
                    self.userVideoList = dict.user_videos ?? []
                    if dict.user_videos?.count ?? 0 > 0 {
                        self.vc?.updateVodeoList(new: true)
                    }
                    self.vc?.collect.reloadData()
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func block_unblock(friend_id:String)  {
//        Para:  user_id, followerid
//        URL - http://karaoke.yilstaging.com/api/block_unblock
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.followerid] = friend_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .block_unblock, loader: true) { (json) in
            
        } successData: { (data) in
            do {
                let dict = try JSONDecoder().decode(userVideoListModel.self, from: data)
                if dict.status == "success" {                   
                    self.vc?.updateBlockList(new: true)
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
}
