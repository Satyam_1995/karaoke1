//
//  OtherUserVideoVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 05/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class OtherUserVideoVM {
    
    weak var vc : OtherUserVideoVC?
    var videoDetail : Video_detail?
    
    func get_video_details(video_d:String)  {
//        Para:video_id,user_id
//        URL - http://karaoke.yilstaging.com/api/get_video_details
//        let loader = ProfileResponse == nil ? true : false
        WebServices.get_video_details(video_id: video_d) { (json) in
            print(json)
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(SingleVideoDetail.self, from: data)
                if d.status == "success" {
                    self.videoDetail = d.video_detail?.first
                    videoDetailCache[video_d] = self.videoDetail
                    self.vc?.upNewData(new: true)
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
}
