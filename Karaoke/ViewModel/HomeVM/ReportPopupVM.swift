//
//  ReportPopupVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 04/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class ReportPopupVM {
    
    weak var vc : ReportPopupVC?
    var result : Video_detail?

    func report_post(comment:String,followers_id:String,video_id:String)  {
//        Para:user_id,followers_id,video_id,comment
//        URL - http://karaoke.yilstaging.com/api/report_post        
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.followers_id] = followers_id
        parameters[ApiKey.video_id] = video_id
        parameters[ApiKey.comment] = comment
        
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .report_post, loader: true) { (json) in
        } successData: { (data) in
            self.vc?.updateViewData()
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
    func report_user(comment:String,reported_userid:String)  {
//        Para:user_id,reported_userid,report_msg
//        URL - http://karaoke.yilstaging.com/api/report_user
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.reported_userid] = reported_userid
        parameters[ApiKey.report_msg] = comment
        WebServices.commonPostAPI(parameters: parameters, endPoint: .report_user, loader: true) { (json) in
        } successData: { (data) in
            self.vc?.updateViewData()
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    

}
