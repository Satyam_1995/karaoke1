//
//  HomeVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 29/12/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
class HomeVM {
    
    weak var vc : HomeVC?
    var videoDetail : Video_detail?
    var latest_song_list : [Latest_song_list] = []
    var video_list : [Video_list] = []
    func get_home_data(lat:String, long:String)  {
//        Para:user_id,lat,long,min_range,max_range
//        URL - http://karaoke.yilstaging.com/api/get_home_data
//        let loader = ProfileResponse == nil ? true : false
        let user_id = UserDetail.shared.getUserId()
        
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        parameters[ApiKey.lat] = lat
        parameters[ApiKey.long] = long
        parameters[ApiKey.min_range] = "0"
        parameters[ApiKey.max_range] = "10000"
        parameters[ApiKey.fcm_token] = UserDefaults.standard.object(forKey: "fcm_token") as? String ?? "1"
//        parameters[ApiKey.fcm_token] = firebaseToken
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_home_data, loader: true) { (json) in
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(HomeVideoList.self, from: data)
                if d.status == "success" {
                    print(parameters,"Sucess")
                    self.latest_song_list = d.latest_song_list ?? []
                    self.video_list = d.video_list ?? []
//                    self.video_list = [d.video_list!.first!]
                    self.vc?.updatedata(new: true)
                    self.vc?.shouldShowLoadingCell = true
                }
            }
            catch {
                print(parameters,"false")
              print(error)
                self.vc?.refreshStatus = false
            }
        } failure: { (err) -> (Void) in
            print(parameters,"false")
            print(err.localizedDescription)
            self.vc?.refreshStatus = false
        }
    }
    func get_home_data2(lat:String, long:String)  {
//        Para:user_id,lat,long,min_range,max_range
//        URL - http://karaoke.yilstaging.com/api/get_home_data
//        let loader = ProfileResponse == nil ? true : false
        self.vc?.shouldShowLoadingCell = false
        let user_id = UserDetail.shared.getUserId()
        
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        parameters[ApiKey.lat] = lat
        parameters[ApiKey.long] = long
        parameters[ApiKey.min_range] = "0"
        parameters[ApiKey.max_range] = "10000"
        parameters[ApiKey.fcm_token] = UserDefaults.standard.object(forKey: "fcm_token") as? String ?? "1"
//        parameters[ApiKey.fcm_token] = firebaseToken
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_home_data, loader: true) { (json) in
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(HomeVideoList.self, from: data)
                if d.status == "success" {
                    let s = d.video_list ?? []
                    self.vc?.insertNewRowsInTableNode(newPosts: s)
                    self.vc?.shouldShowLoadingCell = s.count%10 == 0
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func video_views(video_id:String,complition:@escaping(Bool)->Void)  {
//        Para:user_id,video_id
//        URL -  http://karaoke.yilstaging.com/api/video_views
//        let loader = ProfileResponse == nil ? true : false
        let user_id = UserDetail.shared.getUserId()
        
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        parameters[ApiKey.video_id] = video_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .video_views, loader: false) { (json) in
            complition(true)
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
            complition(false)
        }
    }
}
