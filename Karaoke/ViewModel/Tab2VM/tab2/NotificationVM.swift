//
//  NotificationVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 11/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class NotificationVM {
    
    weak var vc : NotificationVC?
    var notificationData : NotificationModl?
    var notification_list : [Notification_list] = []
    var notification_list1 : [Notification_list] = []
    func get_notification_list(last_id:String,refresh: Bool = false)  {
//        Para:user_id,last_id
//        URL - http://karaoke.yilstaging.com/api/get_notification_list
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
//        parameters[ApiKey.user_id] = "96"
        parameters[ApiKey.last_id] = last_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_notification_list, loader: last_id == "0") { (json) in
        } successData: { (data) in
            do {
                self.notificationData = try JSONDecoder().decode(NotificationModl.self, from: data)
                if self.notificationData?.status == "success" {
                    if let s = self.notificationData?.notification_list {
                        //hit api
                        if self.vc?.page == 0 {
                            self.notification_list.removeAll()
                            }
                        self.notification_list.append(contentsOf: s)
                        
                        self.vc?.page = (self.vc?.page ?? 0) + 1
                        self.vc?.shouldShowLoadingCell = false
//                        if refresh {
//                            self.notification_list = s
//                        } else {
//                            for item in s {
//                                if !self.notification_list.contains(where: {$0.notification_id == item.notification_id}) {
//                                    self.notification_list.append(item)
//                                }
//                            }
//                        }
//                        self.notification_list.append(contentsOf: s)
                        
                        self.notification_list =  self.notification_list.uniques(by: \.notification_id)
                        if self.notification_list1.count == self.notification_list.count {
                            self.vc?.shouldShowLoadingCell = false
                        }else{
                            self.vc?.shouldShowLoadingCell = self.notification_list.count%5 == 0
                            self.notification_list1 = self.notification_list
                        }
                        self.vc?.updateData(new:true)
//                        self.vc?.table.reloadData()
//                        self.vc?.shouldShowLoadingCell = s.currentPage < s.numberOfPages
                    }
                }else{
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func get_notification_listNew(last_id:String,refresh: Bool = false)  {
//        Para:user_id,last_id
//        URL - http://karaoke.yilstaging.com/api/get_notification_list
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.last_id] = last_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_notification_list, loader: false) { (json) in
        } successData: { (data) in
            do {
                self.notificationData = try JSONDecoder().decode(NotificationModl.self, from: data)
                if self.notificationData?.status == "success" {
                    if let s = self.notificationData?.notification_list {
                        var tamp : [Notification_list] = []
                        for item in s {
                            if !self.notification_list.contains(where: {$0.notification_id == item.notification_id}) {
                                tamp.append(item)
                            }
                        }
                        tamp = tamp.reversed()
                        for item in tamp {
                            self.notification_list.insert(item, at: 0)
                        }
                        if tamp.count > 0 {
                            self.vc?.updateData(new:true)
                            CommonFunctions.showToastWithMessage("\(tamp.count) new notification", displayTime: 2, bottomSpace: 50) {
                            }
                        }
                    }
                }else{
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func may_be_later(notification_id:String,status:String = "1",index:Int)  {
//        Para:user_id,notification_id,status
//        URL -  http://karaoke.yilstaging.com/api/may_be_later
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.notification_id] = notification_id
        parameters[ApiKey.status] = status
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .may_be_later, loader: true) { (json) in
            self.vc?.updateMayBeLater(index:index)
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }

}
