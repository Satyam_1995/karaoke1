//
//  SeeAllVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 20/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class SeeAllVM {
    
    weak var vc : SeeAllVC?
    var explore_screenData : Explore_screenVM?
    var top_artist_videos : [Popular_videos] = []
    var top_artist_videos1 : [Popular_videos] = []
    

    func explore_screen(type:musicCategory,page:Int,min_range:Int, max_range:Int)  {
//        Para:user_id,type (recomended,popular,trending,top_artist,top_singer)  min_range,max_range
//        URL - http://karaoke.yilstaging.com/api/get_song_list
        let loader = self.top_artist_videos.count == 0 ? true : false
        
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.type] = type.rawValue
        parameters[ApiKey.min_range] = "\(min_range)"
        parameters[ApiKey.max_range] = "\(max_range)"
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_song_list, loader: loader) { (json) in
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(Explore_screenVM.self, from: data)
//                self.top_artist_videos = d.video_list ?? []
                if min_range == 0 {
                    self.top_artist_videos.removeAll()
                }
                self.top_artist_videos.append(contentsOf: d.video_list ?? [])
                
                if self.top_artist_videos1.count == self.top_artist_videos.count {
                    self.vc?.shouldShowLoadingCell = false
                }else{
                    self.vc?.shouldShowLoadingCell = self.top_artist_videos.count%5 == 0
                    self.top_artist_videos1 = self.top_artist_videos
                }
                
                self.vc?.updatedata(new: true)
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}

enum musicCategory : String {
    case recomended
    case popular
    case trending
    case top_artist
    case top_singer
}
