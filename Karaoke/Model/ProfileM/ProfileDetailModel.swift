

import Foundation
struct ProfileDetailModel : Codable {
	let status : String?
	let data : [ProfileDataModel]?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		data = try values.decodeIfPresent([ProfileDataModel].self, forKey: .data)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}

struct ProfileDataModel : Codable {
    let verified : String?
    let fcmtoken : String?
    let is_active : String?
    let bio : String?
    let password : String?
    let user_email : String?
    let total_favorite : Int?
    let notifications : String?
    let social_type : String?
    let user_name : String?
    let user_id : String?
    let planid : String?
    let total_follower : Int?
    let total_following : Int?
    let mobile : String?
    let gender : String?
    let createdat : String?
    let profile_img : String?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case verified = "verified"
        case fcmtoken = "fcmtoken"
        case is_active = "is_active"
        case bio = "bio"
        case password = "password"
        case user_email = "user_email"
        case total_favorite = "total_favorite"
        case notifications = "notifications"
        case social_type = "social_type"
        case user_name = "user_name"
        case user_id = "user_id"
        case planid = "planid"
        case total_follower = "total_follower"
        case total_following = "total_following"
        case mobile = "mobile"
        case gender = "gender"
        case createdat = "createdat"
        case profile_img = "profile_img"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        verified = try values.decodeIfPresent(String.self, forKey: .verified)
        fcmtoken = try values.decodeIfPresent(String.self, forKey: .fcmtoken)
        is_active = try values.decodeIfPresent(String.self, forKey: .is_active)
        bio = try values.decodeIfPresent(String.self, forKey: .bio)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        user_email = try values.decodeIfPresent(String.self, forKey: .user_email)
        total_favorite = try values.decodeIfPresent(Int.self, forKey: .total_favorite)
        notifications = try values.decodeIfPresent(String.self, forKey: .notifications)
        social_type = try values.decodeIfPresent(String.self, forKey: .social_type)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        planid = try values.decodeIfPresent(String.self, forKey: .planid)
        total_follower = try values.decodeIfPresent(Int.self, forKey: .total_follower)
        total_following = try values.decodeIfPresent(Int.self, forKey: .total_following)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        createdat = try values.decodeIfPresent(String.self, forKey: .createdat)
        profile_img = try values.decodeIfPresent(String.self, forKey: .profile_img)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}
