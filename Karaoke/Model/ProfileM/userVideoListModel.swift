

import Foundation
struct userVideoListModel : Codable {
    let user_videos : [User_videos]?
    let message : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case user_videos = "user_videos"
        case message = "message"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_videos = try values.decodeIfPresent([User_videos].self, forKey: .user_videos)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }

}

struct User_videos : Codable {
	let song_name : String?
	let lyrics : String?
	let video_thumb : String?
	let created_on : String?
	let total_reported : String?
	let video_id : String?
	let user_id : String?
	let total_comments : String?
	let artist_name : String?
	let total_likes : String?
	let video_file : String?
	let video_status : String?

	enum CodingKeys: String, CodingKey {

		case song_name = "song_name"
		case lyrics = "lyrics"
		case video_thumb = "video_thumb"
		case created_on = "created_on"
		case total_reported = "total_reported"
		case video_id = "video_id"
		case user_id = "user_id"
		case total_comments = "total_comments"
		case artist_name = "artist_name"
		case total_likes = "total_likes"
		case video_file = "video_file"
		case video_status = "video_status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
		lyrics = try values.decodeIfPresent(String.self, forKey: .lyrics)
		video_thumb = try values.decodeIfPresent(String.self, forKey: .video_thumb)
		created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
		total_reported = try values.decodeIfPresent(String.self, forKey: .total_reported)
		video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		total_comments = try values.decodeIfPresent(String.self, forKey: .total_comments)
		artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
		total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
		video_file = try values.decodeIfPresent(String.self, forKey: .video_file)
		video_status = try values.decodeIfPresent(String.self, forKey: .video_status)
	}

}
