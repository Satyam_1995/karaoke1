//
//  LikeModel.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 28/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
struct LikeModel : Codable {
    let like_list : [Like_list]?
    let status : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case like_list = "like_list"
        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        like_list = try values.decodeIfPresent([Like_list].self, forKey: .like_list)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
struct Like_list : Codable {
    let like_status : String?
    let user_id : String?
    let name : String?
    let profile_img : String?

    enum CodingKeys: String, CodingKey {

        case like_status = "like_status"
        case user_id = "user_id"
        case name = "name"
        case profile_img = "profile_img"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        like_status = try values.decodeIfPresent(String.self, forKey: .like_status)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        profile_img = try values.decodeIfPresent(String.self, forKey: .profile_img)
    }

}
