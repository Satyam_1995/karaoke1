

import Foundation



struct SoundEffect : Codable {
	let sound_list : [Sound_list]?
	let status : String?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case sound_list = "sound_list"
		case status = "status"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		sound_list = try values.decodeIfPresent([Sound_list].self, forKey: .sound_list)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}    
}
struct Sound_list : Codable {
    let sound_id : String?
    let effect_url : String?
    let effect_name : String?

    enum CodingKeys: String, CodingKey {

        case sound_id = "sound_id"
        case effect_url = "effect_url"
        case effect_name = "effect_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sound_id = try values.decodeIfPresent(String.self, forKey: .sound_id)
        effect_url = try values.decodeIfPresent(String.self, forKey: .effect_url)
        effect_name = try values.decodeIfPresent(String.self, forKey: .effect_name)
    }

}
