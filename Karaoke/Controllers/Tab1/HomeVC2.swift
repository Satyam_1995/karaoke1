//
//  HomeVC2.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 14/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

extension HomeVC : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeViewModel.video_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: shotTableViewCellIdentifier, for: indexPath) as! ShotTableViewCell
        let dict = homeViewModel.video_list[indexPath.item]
        
        let im = dict.video_thumb ?? "https://i.pinimg.com/videos/thumbnails/originals/77/4f/21/774f219598dde62c33389469f5c1b5d1-00001.jpg"
        let vi = dict.video_file ?? "https://v.pinimg.com/videos/720p/77/4f/21/774f219598dde62c33389469f5c1b5d1.mp4"
        cell.configureCell(imageUrl: im, description: "Video", videoUrl: vi)
        
        cell.cellData = dict
        cell.btnProfile.tag = indexPath.item
        cell.btnProfile.addTarget(self, action: #selector(self.btnProfile(_:)), for: .touchUpInside)
        cell.btn_Menu.tag = indexPath.item
        cell.btn_Menu.addTarget(self, action: #selector(self.btn_Menu(_:)), for: .touchUpInside)
        cell.btn_follow.tag = indexPath.item
        cell.btn_follow.addTarget(self, action: #selector(self.btnFollow(_:)), for: .touchUpInside)
        
        cell.bottomView.btnSing.tag = indexPath.item
        cell.bottomView.btnSing.addTarget(self, action: #selector(self.btnaddSing(_:)), for: .touchUpInside)
//        let d = cell.shotImageView.colorOfPoint(point: CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 100))
//        cell.bottomView.backgroundColor = self.getComplementaryForColor(color: d)
        cell.tabActionDetail = { [weak self] (ind) in
//            self?.moveToVideoDetail(indexPath: indexPath)
        }
        cell.tabActionLike = { [weak self] (ind) in
            self?.addToLike(index: indexPath.item, btn: cell.bottomView)
//            cell.liked = self?.homeViewModel.video_list[indexPath.item].user_like_status == 1 ? true : false
//            cell.cellData?.user_like_status = self?.homeViewModel.video_list[indexPath.item].user_like_status
            
        }
        if user_id == dict.user_id! {
            cell.btn_follow.isHidden = true
        }
        cell.bottomView.tabAction = { [weak self] (type,btn) in
            switch type {
            case .homeTab_likeBtnAction:
                print("homeTab_likeBtnAction")
                self?.addToLike(index: indexPath.item, btn: cell.bottomView)
                cell.liked = self?.homeViewModel.video_list[indexPath.item].user_like_status == 1 ? true : false
//                cell.cellData?.user_like_status = self?.homeViewModel.video_list[indexPath.item].user_like_status
                break
            case .homeTab_CommentBtnAction:
                print("homeTab_CommentBtnAction")
                self?.addCommect(index: indexPath.item)
                break
            case .homeTab_singItBtnAction:
//                self?.addSignIt(index: indexPath.item)
                print("homeTab_singItBtnAction")
                break
            case .homeTab_favouriteBtnAction:
                print("homeTab_favouriteBtnAction")
                self?.addToFav(index: indexPath.item, btn: cell.bottomView)
                break
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
            ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
        getMoreImages()
    }
    func getMoreImages(){
//        let last = homeViewModel.video_list.count
//        self.homeViewModel.get_home_data2(lat: "\(location.latitude)", long: "\(location.longitude)")
    }
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.homeViewModel.video_list.count - 1
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pausePlayeVideos()
//        let w : Int = Int(scrollView.contentOffset.y / self.view.frame.size.height)
        self.getTime()
        if let s = tableView.indexPathsForVisibleRows?.first {
            self.tableView.scrollToRow(at: s, at: .top, animated: true)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            pausePlayeVideos()
        }
    }
    
    func pausePlayeVideos(){
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: tableView)
    }
    
    @objc func appEnteredFromBackground() {
        guard visableViewCon else {
            return
        }
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: tableView, appEnteredFromBackground: true)
    }
    func pauseVideos2(){
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor2(tableView: tableView)
        self.stopTimer()
    }
    func stopTimer()  {
        timerPlayV?.invalidate()
        timerPlayV = nil
    }
    func getTime() {
        timerPlayV?.invalidate()
        timerPlayV = nil
        timerPlayV = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (t) in
            guard let cell = self.tableView.visibleCells.first,
                  let indexL = self.tableView.indexPathsForVisibleRows?.first else { return }
            
            guard let videoCell = cell as? ASAutoPlayVideoLayerContainer,let tim = videoCell.videoLayer.player?.currentTime().seconds else {
                return
            }
//            print(tim)
            if tim >= 10 && tim < 11 {
                if let dict = self.homeViewModel.video_list[indexL.row].video_id {
                    self.homeViewModel.video_views(video_id: dict) { (_) in
                        print("Done")
                    }
                }
            }
//            tim1.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
//                if tim1.currentItem?.status == .readyToPlay {
//                    let time : Float64 = CMTimeGetSeconds(tim1.currentTime());
//
//                }
//            }
//
//            let visisbleCells = self.tableView.visibleCells
//            var videoCellContainer: ASAutoPlayVideoLayerContainer?
//            var maxHeight: CGFloat = 0.0
//            for cellView in visisbleCells {
//                guard let containerCell = cellView as? ASAutoPlayVideoLayerContainer,
//                      let videoCellURL = containerCell.videoURL else {
//                    continue
//                }
//                let height = containerCell.visibleVideoHeight()
//                if maxHeight < height {
//                    maxHeight = height
//                    videoCellContainer = containerCell
//                }
//                //            pauseRemoveLayer(layer: containerCell.videoLayer, url: videoCellURL, layerHeight: height)
//
//            }
//            guard let videoCell = videoCellContainer,
//                  let videoCellURL = videoCell.videoURL else {
//                return
//            }
//            print(videoCell.videoLayer.player?.currentTime().seconds as Any)
        }
    }
}




extension UIViewController {
    func iScreenSizes() -> CGFloat {
        let height = UIScreen.main.bounds.size.height
        print("Device height: \(height)")
        switch height {
        case 480.0:
            print("iPhone 3 | iPhone 4 | iPhone 4S")
        case 568.0:
            print("iPhone 5 | iPhone 5S | iPhone 5C | iPhone SE")
        case 667.0:
            print("iPhone 6 | iPhone 7 | iPhone 8 | iPhone SE(2nd gen)")
        case 736.0:
            print("iPhone 6+ | iPhone 7+ | iPhone 8+")
        case 780.0:
            print("iPhone 12 Mini")
        case 812.0:
            print("iPhone X | iPhone XS | iPhone 11 Pro")
        case 844.0:
            print("iPhone 12 | iPhone 12 Pro")
        case 896.0:
            print("iPhone XR | iPhone XS Max | iPhone 11 | iPhone 11 Pro Max")
        case 926.0:
            print("iPhone 12 Pro Max")
        case 1024.0:
            print("iPad 1st gen | iPad 2 | iPad 3rd gen | iPad mini | iPad 4th gen | iPad Air | iPad mini 2 | iPad mini 3 | iPad Air 2 | iPad mini 4 | iPad 5th gen | iPad 6th gen | iPad  mini 5")
        case 1112.0:
            print("iPad Pro 2nd gen 10.5'' | iPad Air 3")
        case 1194.0:
            print("iPad Pro 3rd gen 11.0'' | iPad Pro 4th gen 11.0''")
        case 1366.0:
            print("iPad Pro 1st gen 12.9'' | iPad 2nd gen 12.9'' | iPad 3rd gen 12.9'' | iPad Pro 4th gen 12.9''")
        default:
            print("not listed in function")
            
        }
        return height
    }
}
extension UIView {
    func colorOfPoint(point: CGPoint) -> UIColor {
        let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)

        var pixelData: [UInt8] = [0, 0, 0, 0]

        let context = CGContext(data: &pixelData, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)

        context!.translateBy(x: -point.x, y: -point.y)

        self.layer.render(in: context!)

        let red: CGFloat = CGFloat(pixelData[0]) / CGFloat(255.0)
        let green: CGFloat = CGFloat(pixelData[1]) / CGFloat(255.0)
        let blue: CGFloat = CGFloat(pixelData[2]) / CGFloat(255.0)
        let alpha: CGFloat = CGFloat(pixelData[3]) / CGFloat(255.0)

        let color: UIColor = UIColor(red: red, green: green, blue: blue, alpha: alpha)

        return color
    }
}
extension UIViewController {
    // get a complementary color to this color:
    func getComplementaryForColor(color: UIColor) -> UIColor {
        
        let ciColor = CIColor(color: color)
        
        // get the current values and make the difference from white:
        let compRed: CGFloat = 1.0 - ciColor.red
        let compGreen: CGFloat = 1.0 - ciColor.green
        let compBlue: CGFloat = 1.0 - ciColor.blue
        
        return UIColor(red: compRed, green: compGreen, blue: compBlue, alpha: 1.0)
    }
}
