//
//  SongsInfoVC.swift
//  Karaoke
//
//  Created by Ankur  on 17/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import AVFoundation


class SongsInfoVC: UIViewController {
    
    @IBOutlet weak var preetiVideoView:VideoViewPreeti!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var lblSongLyers: UILabel!
//    var player : AVPlayer?
    
    var status:Int = 0
    var videoDetail : Video_detail?
    var video_id : String = ""
    var isPlayingVideo = false
    deinit {
        NotificationCenter.default.removeObserver(self)
        preetiVideoView?.player?.pause()
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if status == 1 {
            let vc = ReportPopupVC.instantiate(fromAppStoryboard: .Home)
            self.present(vc, animated: false, completion: nil)
        }
        self.get_video_details(video_d: video_id)
        btnPlay.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        self.btnPlay.setImage(#imageLiteral(resourceName: "play3"), for: .selected)
        self.loadPlayer()
        self.btnPlay.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        paushVideo()
        self.btnPlay.isSelected = false
    }
    
    @IBAction func btn_ReportSong(_ sender: UIButton){
        guard let friend_id = videoDetail?.user_id else {
            return
        }
        let vc = ReportPopupVC.instantiate(fromAppStoryboard: .Home)
        vc.status = 2
        vc.followers_id = friend_id
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_Back(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSing(_ sender:UIButton) {
        
        guard let dict1 = videoDetail else {
            return
        }
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.hidesBottomBarWhenPushed = true
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnPlayAction(_ sender:UIButton) {
        if isPlayingVideo {
            self.paushVideo()
        }else{
            self.playVideo()
        }
        self.btnPlay.isSelected = !self.btnPlay.isSelected
    }
    
    func upNewData(new: Bool) {
        guard let dict = videoDetail else {
            return
        }
        self.lblSongName.text = dict.song_name ?? ""
        self.lblSongLyers.text = dict.lyrics ?? ""
//        self.imgProfile.imageURL = dict.video_thumb ?? ""
        if let img = dict.video_thumb?.convert_to_url {
            self.imgProfile.af.setImage(withURL: img)
        }
        btnPlay.isEnabled = true
        if let vurl = dict.video_file?.convert_to_url {
            self.showPlayer(item: vurl)
        }
    }
    @IBAction func btnShare(_ sender:UIButton) {
        guard let dict = videoDetail else { return }
        let loading = LoadingView()
        loading.showActivityLoading(uiView: self.view, color: .black)
        self.btnPlayAction(UIButton())
        SharePost.shared.generateContentLink1(video_name: dict.song_name ?? "Karaoke", video_id: dict.video_id ?? "9", video_url: dict.video_file ?? "", video_image: dict.video_thumb ?? "") { (url) in
            if url != nil {
                
                let activityVC = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                    loading.hideActivityLoading(uiView: self.view)
                    self.btnPlayAction(UIButton())
                    if !completed {
                    }
                }
                self.present(activityVC, animated: true, completion: nil)
            }else{
                loading.hideActivityLoading(uiView: self.view)
            }
        }
    }
}
extension SongsInfoVC {
    func get_video_details(video_d:String)  {
//        Para:video_id,user_id
//        URL - http://karaoke.yilstaging.com/api/get_video_details
//        let loader = ProfileResponse == nil ? true : false
        WebServices.get_video_details(video_id: video_d) { (json) in
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(SingleVideoDetail.self, from: data)
                if d.status == "success" {
                    self.videoDetail = d.video_detail?.first
                    videoDetailCache[video_d] = self.videoDetail
                    self.upNewData(new: true)
                }
            }
            catch {
                print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func loadPlayer() {
        preetiVideoView?.contentMode = .scaleAspectFill
        preetiVideoView?.player?.isMuted = false
        preetiVideoView?.repeat = .loop
//        preetiVideoView?.url = self.videoURL!.convert_to_url!
//        preetiVideoView?.player?.play()
    }
    func showPlayer(item: URL) {
        preetiVideoView?.url = item
        preetiVideoView?.player?.play()
        preetiVideoView.actionEndPlayVideo = { [weak self] (tag) in
            print("actionEndPlayVideo")
        }
        preetiVideoView.actionErrorPlayVideo = { [weak self] (tag) in
          print("actionErrorPlayVideo")
//            self?.loading.hideActivityLoading(uiView: self?.view)
        }
        preetiVideoView.actionError = { [weak self] (tag) in
          print("actionError")
//            self?.loading.hideActivityLoading(uiView: self?.view)
        }
        preetiVideoView.actionPlaybackStalled = { [weak self] (tag) in
          print("actionPlaybackStalled")
            self?.btnPlay.isEnabled = true
            self?.isPlayingVideo = true
            self!.btnPlay.isSelected = true
//            self?.loading.hideActivityLoading(uiView: self?.view)
        }
        
//        player = AVPlayer(url: item)
//        let avPlayerLayer = AVPlayerLayer(player: player)
////        avPlayerLayer.videoGravity = .resizeAspect
//        avPlayerLayer.masksToBounds = true
//        avPlayerLayer.cornerRadius = 5
//        avPlayerLayer.videoGravity = .resizeAspectFill
//        avPlayerLayer.frame = imgProfile.layer.bounds
//        self.imgProfile.layer.addSublayer(avPlayerLayer)
////        if (player != nil) {
////            player!.play()
////        }
//        DispatchQueue.main.async {
//            avPlayerLayer.frame = self.imgProfile.frame
//        }
        
    }
    @objc func playerItemDidReachEnd(_ notification: Notification) {
//        player?.seek(to: CMTime.zero)
        self.playVideo()
    }
    func playVideo() {
        preetiVideoView?.player?.play()
        isPlayingVideo = true
    }
    func paushVideo() {
        preetiVideoView?.player?.pause()
        isPlayingVideo = false
    }
}
