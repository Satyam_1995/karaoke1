//
//  TermsVC.swift
//  Karaoke
//
//  Created by Ankur  on 04/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class TermsVC: UIViewController {
    
    @IBOutlet weak var TermsView : UIView!
    @IBOutlet weak var PrivacyView : UIView!
    @IBOutlet weak var btnTerms : UIButton!
    @IBOutlet weak var btnPrivacy : UIButton!
    
    @IBOutlet weak var lblPrivacyTitle : UILabel!
    @IBOutlet weak var lblPrivacyDis : UILabel!
    
    @IBOutlet weak var lblTermsTitle : UILabel!
    @IBOutlet weak var lblTermsDis : UILabel!
    
    var status:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if status == 1 {
            hideOtherView(viewC: TermsView, tag: status, btn: btnTerms)
        }else{
            hideOtherView(viewC: PrivacyView, tag: status, btn: btnPrivacy)
        }
        self.updatePass()
        self.setData()
    }
    func setData()  {
        guard let dict = UserDefaults.standard.object(forKey: "termsData") as? NSDictionary else {
            return
        }        
        if let da = dict["Privacy_policies"] as? NSDictionary {
            lblPrivacyTitle.text = da["page_name"] as? String ?? ""
            lblPrivacyDis.attributedText = (da["page_content"] as? String ?? "").htmlToAttributedString
        }
        if let da = dict["Terms_and_conditions"] as? NSDictionary {
            lblTermsTitle.text = da["page_name"] as? String ?? ""
            lblTermsDis.attributedText = (da["page_content"] as? String ?? "").htmlToAttributedString
        }
    }
    func updatePass() {
        
//        URL - http://karaoke.yilstaging.com/api/get_content
        let para : JSONDictionary = [:]
        WebServices.commonPostAPI(parameters: para, endPoint: .get_content, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            if let obj = dict["content_data"] as? NSDictionary {
            UserDefaults.standard.setValue(obj, forKey: "termsData")
            }
            self.setData()
        } successData: { (data) in
        } failure: { (err) -> (Void) in
        }
    }
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func hideOtherView(viewC:UIView,tag:Int,btn:UIButton) {
        TermsView.isHidden = true
        PrivacyView.isHidden = true
//        viewC.isHidden = false
        btnPrivacy.backgroundColor = .clear
        btnTerms.backgroundColor = .clear
        btnPrivacy.setTitleColor(#colorLiteral(red: 0.09803921569, green: 0.1019607843, blue: 0.3529411765, alpha: 1), for: .normal)
        btnTerms.setTitleColor(#colorLiteral(red: 0.09803921569, green: 0.1019607843, blue: 0.3529411765, alpha: 1), for: .normal)
        btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        btn.backgroundColor = #colorLiteral(red: 0.09803921569, green: 0.1019607843, blue: 0.3529411765, alpha: 1)
        if tag == 1{
            TermsView.isHidden = false
        }else{
            PrivacyView.isHidden = false
        }
    }
    
    @IBAction func btnClicks(_ sender:UIButton) {
        hideOtherView(viewC: TermsView, tag: sender.tag, btn: sender)
    }
}
