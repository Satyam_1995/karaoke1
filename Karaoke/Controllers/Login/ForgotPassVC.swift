//
//  ForgotPassVC.swift
//  Karaoke
//
//  Created by Ankur  on 04/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {
    
    @IBOutlet weak var txt_Email : UITextField!
    @IBOutlet weak var txt_NewPass: UITextField!
    @IBOutlet weak var txt_conPass: UITextField!
    
    @IBOutlet weak var OTPView : UIView!
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var ResetPassView : UIView!
    var enteredOtp: String = ""
    var otpStr: String = "1234"
    var user_id = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.OTPView.frame = self.view.frame
        self.view.addSubview(self.OTPView)
        self.OTPView.isHidden = true
        
        self.ResetPassView.frame = self.view.frame
        self.view.addSubview(self.ResetPassView)
        self.ResetPassView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.OTPView.frame = self.view.frame
        self.ResetPassView.frame = self.view.frame
    }
    
    // MARK:- IBActions
    //===================
    
    @IBAction func btn_SubmitEmail(_ sender: UIButton){
        self.view.endEditing(true)
        self.submitEmail(showresendMSG: false)
        
//        self.OTPView.isHidden = false
    }
    func submitEmail(showresendMSG : Bool) {
        
        if self.txt_Email.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.emailorMobile)
            return
        }
        if !self.isValidEmail(testStr: self.txt_Email.text!) {
            CommonFunctions.showToastWithMessage(MessageString.validEmail)
            return
        }
        
        //        http://karaoke.yilstaging.com/api/forget_password
        //            mobile/email
        var para : JSONDictionary = [:]
        para[ApiKey.email] = self.txt_Email.text!
       
        WebServices.commonPostAPI(parameters: para, endPoint: .forget_password, loader: true) { (json) in
            if let dict = json.dictionaryObject, let userid = dict["userid"] as? String,let otp = dict["otp"] as? String  {
                self.user_id = userid
                self.otpStr = otp
                self.OtpViewLoad()
                self.OTPView.isHidden = false
                if showresendMSG == true {
                    self.AlertControllerOnr(title: alertTitle.alert_message, message: "One Time Verification code has been sent to your entered email/phone.", BtnTitle: "OK")
                }
            }else{
                self.AlertControllerOnr(title: alertTitle.alert_error, message: MessageString.validEmail, BtnTitle: "OK")
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
        }
    }
    func validation() -> Bool {
        
        if self.txt_NewPass.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.newpassword)
            return false
        }
        if self.txt_conPass.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.confirmpassword)
            return false
        }
//        if !self.isPasswordValid(self.txt_NewPass.text!)  {
//            CommonFunctions.showToastWithMessage(MessageString.passwordValid)
//            return false
//        }
        if self.txt_conPass.text != self.txt_NewPass.text  {
            CommonFunctions.showToastWithMessage("Password and confirm password does not match.")
            return false
        }
        return true
    }
    func updatePass() {
//        URL - http://karaoke.yilstaging.com/api/reset_password
        guard validation() else {
            return
        }
        var para : JSONDictionary = [:]
        para[ApiKey.password] = self.txt_NewPass.text!
        para[ApiKey.user_id] = self.user_id
        
        WebServices.commonPostAPI(parameters: para, endPoint: .reset_password, loader: true) { (json) in
            
            self.dismiss(animated: true, completion: nil)
        } successData: { (data) in
        } failure: { (err) -> (Void) in
        }
    }
    @IBAction func btn_SubmitOtp(_ sender: UIButton){
        if enteredOtp.count < 4 {
            
        }
        let otp = enteredOtp
        if "" == otp {
            CommonFunctions.showToastWithMessage(MessageString.enterOTP)
            return
        }
        if self.otpStr != otp {
            CommonFunctions.showToastWithMessage(MessageString.validOTP)
            return
        }
        self.OTPView.isHidden = true
        self.ResetPassView.isHidden = false
    }
    @IBAction func btn_CreatePass(_ sender:UIButton){
        guard validation() else {
            return
        }
        self.updatePass()
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_close(_ sender: UIButton){
        if sender.tag == 1 {
            self.dismiss(animated: true, completion: nil)
        }else if sender.tag == 2 {
            self.OTPView.isHidden = true
        }else{
            self.ResetPassView.isHidden = true
            self.OTPView.isHidden = true
        }
        
    }
}

// MARK:- Otp Methods
//===================
extension ForgotPassVC: VPMOTPViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        enteredOtp = otpString
        print("OTPString: \(otpString)")
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return enteredOtp == otpStr
    }
    
    func OtpViewLoad(){
        otpView.otpFieldsCount = 4
        //        otpView.otpFieldDefaultBorderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        //        otpView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        otpView.otpFieldEnteredBorderColor = UIColor.green
        otpView.otpFieldErrorBorderColor = UIColor.red
        otpView.otpFieldBorderWidth = 0.5
        otpView.otpFieldDefaultBorderColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        otpView.delegate = self
        otpView.shouldAllowIntermediateEditing = false
        //        otpView.otpFieldSeparatorSpace = 20
        //        otpView.otpFieldSize = 60
        otpView.otpFieldSize = 40
        otpView.otpFieldInputType = .numeric
        otpView.otpFieldDisplayType = .square
        otpView.otpFieldDefaultBackgroundColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        //      otpView.otpFieldEnteredBorderColor = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        
        
        
        // Create the UI
        otpView.initializeUI()
    }
}
