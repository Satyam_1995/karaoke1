//
//  UploadVideoVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class UploadVideoVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnConfirm(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
            sender.tag = 1
        }else{
            sender.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            sender.tag = 0
        }
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpoload(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
