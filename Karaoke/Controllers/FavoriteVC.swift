//
//  FavoriteVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class FavoriteVC: UIViewController {
    
    @IBOutlet weak var collect:UICollectionView!
    
//    var imgArr = [#imageLiteral(resourceName: "i15"),#imageLiteral(resourceName: "i17"),#imageLiteral(resourceName: "i8"),#imageLiteral(resourceName: "i14"),#imageLiteral(resourceName: "i16")]
    let viewModel = FavoriteVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.vc = self
        viewModel.get_fav_list()
        Setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.get_fav_list()
    }
    func Setup(){
        collect.delegate = self
        collect.dataSource = self
        self.collect.register(UINib(nibName: "FavoriteCell", bundle: nil), forCellWithReuseIdentifier: "FavoriteCell")
        collect.register(Header.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Header.identifier)
    }
    
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func updateData(new:Bool)  {
        self.collect.reloadData()
    }
}


extension FavoriteVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.userVideoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "FavoriteCell", for: indexPath) as! FavoriteCell
        let dict = viewModel.userVideoList[indexPath.item]
        cell.cellData = dict
        cell.btnFavorite.tag = indexPath.row
        cell.btnFavorite.addTarget(self, action: #selector(btnFavorite(_:)), for: .touchUpInside)
        cell.btnComment.tag = indexPath.row
        cell.btnComment.addTarget(self, action: #selector(btnComment(_:)), for: .touchUpInside)
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = viewModel.userVideoList[indexPath.item]
//        let vc = UserVideoVC.instantiate(fromAppStoryboard: .Video)
        let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        if let videoURL =  dict.video_file?.convert_to_string, let v_id = dict.video_id {
            vc.videoURL = videoURL
            vc.video_id = v_id
//            vc.status = 0
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "Something went wrong with this video.")
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collect.frame.size.width/3 - 2.5
        let h = collect.frame.size.height/3.5
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collect.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Header.identifier, for: indexPath) as! Header
        header.Configure(str: "\(viewModel.userVideoList.count) Videos", txtColor: .darkGray, align: .left)
        return header
    }
    
    @IBAction func btnFavorite(_ sender:UIButton) {
        let dict = viewModel.userVideoList[sender.tag]
        guard let video_id = dict.video_id, video_id != "", let likeS = dict.user_fav_status else {
            return
        }
        var d : favourite_status = .favourite
        if likeS == 1 {
            d = .unfavourite
        }else{
            d = .favourite
        }
        WebServices.submit_favorite_unfavorite_video(video_id: video_id, favourite_status: d) { (json) in
            CommonFunctions.showToastWithMessage("Remove from favorite", displayTime: 2) {}
            self.viewModel.userVideoList.remove(at: sender.tag)
            self.collect.reloadData()
            NotificationCenter.default.post(name: NSNotification.Name("removeToFavourite"), object: video_id)
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err)
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
        }
    }
    @IBAction func btnLike(_ sender:UIButton) {
        let dict = viewModel.userVideoList[sender.tag]
        guard let video_id = dict.video_id, video_id != "", let likeS = dict.user_like_status else {
            return
        }
        var d : like_status = .like
        let cell = collect.cellForItem(at: IndexPath(item: sender.tag, section: 0)) as? FavoriteCell
        
        if likeS == 1 {
            d = .unLike
            viewModel.userVideoList[sender.tag].user_like_status = 0
            cell?.imgLike.image = #imageLiteral(resourceName: "heartW")
            cell?.removeLike()
        }else{
            d = .like
            viewModel.userVideoList[sender.tag].user_like_status = 1
            cell?.imgLike.image = #imageLiteral(resourceName: "heartR")
            cell?.AddLike()
        }
        
        WebServices.submit_post_likes_unlike_video(video_id: video_id, like: d) { (json) in
        } successData: { (data) in
            
        } failure: { (err) -> (Void) in
            print(err)
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
            if likeS != 1 {
                self.viewModel.userVideoList[sender.tag].user_like_status = 0
                cell?.imgLike.image = #imageLiteral(resourceName: "heartW")
                cell?.removeLike()
            }else{
                self.viewModel.userVideoList[sender.tag].user_like_status = 1
                cell?.imgLike.image = #imageLiteral(resourceName: "heartR")
                cell?.AddLike()
            }
        }
    }
    
    
    @IBAction func btnComment(_ sender:UIButton) {
        let dict = viewModel.userVideoList[sender.tag]
        guard let video_id = dict.video_id else {
            return
        }
        
        let vc = CommentVC.instantiate(fromAppStoryboard: .Home)
        //    vc.hidesBottomBarWhenPushed = true
        vc.video_id = video_id
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}
