//
//  SingItVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import MiniPlayer
import AVKit
import SwiftVideoRecorder
import AudioKit
import SoundpipeAudioKit

protocol RecordedVideoDelegate1 {
    func didUploadVideo(fileUrl: URL)
}

class SingItVC: UIViewController {
    
    var timeMin = 0
    var timeSec = 0
    weak var timer: Timer?
    
    @IBOutlet weak var waveform: FDWaveformView!
    @IBOutlet weak var collect:UICollectionView!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var lblTimer:UILabel!
    @IBOutlet weak var lblLyrics:UILabel!
    @IBOutlet weak var viewLayer:UIView!
    
    
    @IBOutlet weak var deleteSegmentButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    
    @IBOutlet weak var audioView:UIView!
    @IBOutlet weak var btnAudioApply:UIButton!
    @IBOutlet weak var btnAudioCancel:UIButton!
    @IBOutlet weak var miniPlayer: MiniPlayer!
    var timeObserverToken: Any?

    var lyrics : String = ""
    var song_name : String = ""
    var music_cat_id : String = ""
    var artist_name : String = ""
    var old_video_id : String = ""
    var myplan_details : [Myplan_details] = []
    var currentDeviceDate = Date()
    var user_have_plan = false
    
    var selectedIndex = IndexPath(item: -1, section: 0)
    var soundMurgeArray : [soundMergeList] = []
    
    var soundMurgeArray_runtime : [soundMergeList] = []
    var start_time : Double = 0
    var end_time : Double = 0
    var videoMurgeArray : [URL] = []
    var lyricsVideoURL : URL?
    var videoURL : URL?
    var micAudioURL : URL?
    
    var video_thumb : Data?
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    
    // MARK: Video outlet and varible
    var delegate: RecordedVideoDelegate?
    var audioPlayer: AVAudioPlayer?
    
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet var captureView: UIView!
//    var captureSession: AVCaptureSession!
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var videoDeviceInput: AVCaptureDeviceInput!
    let alert = UIAlertController(title: alertTitle.alert_message, message: "Please plug headphone", preferredStyle: .alert)
    // NEW
    var cameraConfig: CameraConfiguration!
    let imagePickerController = UIImagePickerController()
    var lblArr : [UIView] = []
    @IBOutlet weak var preetiVideoView:VideoViewPreeti!
    var loading = LoadingView()
    var videoRecordingStarted: Bool = false {
        didSet{
            if videoRecordingStarted {
                self.start()
            } else {
                self.stop()
            }
        }
    }
    var playerView: MediaPlayerView?
    @IBOutlet weak var previewVideoPlay: UIView!
    var sound_list : [Sound_list] = []
    var _filename = "kar"
    var _filenameCount = 0
    var videoDetail : Video_detail?
    var check_audioAgain = false
    var audioURLStr : URL?
    var slider: UISlider!
//    private var audioController = AudioController()
    let audioEngine = AudioEngine()
    var mic: AudioEngine.InputNode?
    var echoDelay:Delay?
    var reverbDelay:FlatFrequencyResponseReverb?
    let inputDevices = Settings.session.availableInputs
    var inputDeviceList = [String]()
    var audioRecorder:NodeRecorder?
    
    deinit {
        #if targetEnvironment(simulator)
        #else
        self.recorderView.removeFromSuperview()
        #endif
        self.stopVideo()
        for item in videoMurgeArray {
            documentsRemoveItem(fromDocumentsWithFileName: item)
        }
    }
    #if targetEnvironment(simulator)
    #else
    public var recorderView: RecorderView = {
        var recorder = RecorderView()
        recorder.translatesAutoresizingMaskIntoConstraints = false
        return recorder
    }()
    public var recorder: Recorder {
        return recorderView.recorder
    }
    #endif
    
    // MARK: audio
    var engine: AVAudioEngine!
    var playerNode : AVAudioPlayerNode!
    var audioFile : AVAudioFile!
    var reverb : AVAudioUnitReverb!
    var echo : AVAudioUnitDistortion!
    var completionFlag = false
    var offsetFrame = Int()
    var timerSound : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.cameraConfig = CameraConfiguration()
//        cameraConfig.setup { (error) in
//            if error != nil {
//                print(error!.localizedDescription)
//            }
//            try? self.cameraConfig.displayPreview(self.previewView)
//        }
        registerNotification()
//        self.cameraConfig.outputType = .video
        self.slider = UISlider()
        self.setUpMy()
        self.getSongList()
        //tarun
        let captureSession = AVCaptureSession()
        captureSession.startRunning()
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        view.layer.addSublayer(previewLayer)
        previewLayer.frame = view.frame
        
        
        DispatchQueue.main.async {
//            self.recordButton.setImage(#imageLiteral(resourceName: "iconCaptureVideo"), for: .selected)
        }
        #if targetEnvironment(simulator)
        if let url = Bundle.main.url(forResource: "demo2", withExtension: "mp4"), let url2 = Bundle.main.url(forResource: "demo3", withExtension: "mp4") {
            self.videoURL = url
            VideoCompositionWriter.shared.cropVideo(sourceURL1: url2, statTime: 0, endTime: 11) { (u) in
//            VideoCompositionWriter.shared.trimAudio(asset: AVAsset(url: url2), startTime: 0, stopTime: 11) { (u) in
//                self.soundMurgeArray.append(soundMergeList(url: u, time: 0.1, id: "-1"))
                print("audio",u as Any)
                self.audioURLStr = url2
                self.mergeSegmentsAndUpload { (ur) in
                    self.waveform.waveformType = .linear
                    self.waveform.audioURL = ur
                    self.waveform.delegate = self
                    self.presentPlayerView()
                    self.viewLayer.isHidden = false
                    self.addPeriodicTimeObserver()
                    self.stopVideo()
                    self.preetiVideoView?.player?.seek(to: .zero)
                }
            }
//            waveform.waveformType = .linear
//            waveform.audioURL = url
//            waveform.delegate = self
//            self.presentPlayerView()
//            self.addPeriodicTimeObserver()
        }
        #else
        #endif
        self.ishiddinColl(isHidden: true)
        self.user_subscription_plan()
        self.addrecordV()
        #if targetEnvironment(simulator)
        #else
        recorder.videoListeners.append { (url) in
//            self.saveToCameraRoll(url: url)
            print("video",url)
            self.videoURL = url
            self.generateThumbnail(url: url) { (da) in
            }
//            self.check_audioAgain = true
//            self.presentPlayerView()
            self.loading.showActivityLoading(uiView: self.view, color: .white)
            VideoCompositionWriter.shared.cropVideo(sourceURL1: self.lyricsVideoURL ?? url, statTime: self.start_time, endTime: self.end_time) { (output) in
                DispatchQueue.main.async {
                    self.loading.hideActivityLoading(uiView: self.view)
                }
                guard let output = output else {
                    return
                }
                print(output)
                self.audioURLStr = output
                
                //            VideoCompositionWriter.shared.trimAudio(asset: AVAsset(url: url2), startTime: 0, stopTime: 11) { (u) in
                
                self.mergeSegmentsAndUpload { (ur) in
                    self.videoURL = ur
                    self.check_audioAgain = true
                    DispatchQueue.main.async {
                        self.presentPlayerView()
                    }
                }
            }
        }
        #endif
        self.alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
            self.navigationController?.popViewController(animated: true)
        }))
        if !self.checkForHeadphone() {
            DispatchQueue.main.async {
                self.present(self.alert, animated: true) {
                }
            }
        }
        
        self.initAudioKit()
        self.addObserverForHeadphone(remove: false)
    }

    func initAudioKit() {
        if let input = audioEngine.input {
            mic = input
            if let inputAudio = mic {
//                echoDelay = Delay(inputAudio)
                reverbDelay = FlatFrequencyResponseReverb(inputAudio)
                
//                var timeDef = Delay.timeDef
//                timeDef.range = 0...1.0
//                echoDelay?.$time = NodeParameter(timeDef)
                
                
                let mixer = Mixer(inputAudio,reverbDelay!)
                
                do {
                    audioRecorder = try NodeRecorder(node: mixer)
                } catch let err {
                    fatalError("\(err)")
                }
                reverbDelay!.start()
//                echoDelay!.start()
                audioEngine.output = mixer
            }
        } else {
            mic = nil
            echo = nil
            audioEngine.output = Mixer()
        }
        if let existingInputs = inputDevices {
            for device in existingInputs {
                self.inputDeviceList.append(device.portName)
            }
        }
    }
    func checkForHeadphone() -> Bool {
        #if targetEnvironment(simulator)
        return true
        #else
        #endif
        let route = AVAudioSession.sharedInstance().currentRoute
        for port in route.outputs {
            if port.portType == AVAudioSession.Port.headphones {
                // Headphones located
                print("headphone plugged in")
                return true
            }
        }
        return false
    }
    func addObserverForHeadphone(remove:Bool){
        
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.routeChangeNotification, object: nil)
        if remove == true {
            return
        }
        NotificationCenter.default.addObserver(forName: AVAudioSession.routeChangeNotification, object: nil, queue: .main) { (notification) in
            let audioRouteChangeReason = notification.userInfo![AVAudioSessionRouteChangeReasonKey] as? UInt
            switch audioRouteChangeReason {
            case AVAudioSession.RouteChangeReason.newDeviceAvailable.rawValue:
                print("headphone plugged in")
                self.alert.dismiss(animated: true) {
                }
                if self.checkForHeadphone() {
                    DispatchQueue.main.async {
                        if self.preetiVideoView?.player?.timeControlStatus == .playing {
//                            self.preetiVideoView?.player?.pause()
                        } else if self.preetiVideoView?.player?.timeControlStatus == .paused {
                            self.playVideo()
                        }else{
                            self.playVideo()
                        }
                    }
                }
            case AVAudioSession.RouteChangeReason.oldDeviceUnavailable.rawValue:
                print("headphone pulled out")
                self.present(self.alert, animated: true) {
                }
                DispatchQueue.main.async {
                    self.stopVideo()
                }
            default:
                break
            }
        }
    }
    func addrecordV() {
        #if targetEnvironment(simulator)
        #else
        self.recorderView.frame = CGRect(x: 0, y: 0, width: self.previewView.frame.size.width, height: self.previewView.frame.size.height)
        DispatchQueue.main.async {
            
            self.recorderView.recorder.captureSession.startRunning()
            self.recorderView.recorder.captureSession.automaticallyConfiguresApplicationAudioSession = false

            self.recorderView.frame = CGRect(x: 0, y: 0, width: self.previewView.frame.size.width, height: self.previewView.frame.size.height)
            self.recorderView.center = self.previewView.center
        }
        self.previewView.addSubview(recorderView)
        self.previewView.sendSubviewToBack(recorderView)
        
        #endif
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
//        recorderView.recorder.captureSession.startRunning()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.captureSession.stopRunning()
        #if targetEnvironment(simulator)
        #else
        recorderView.recorder.captureSession.stopRunning()
        #endif
        self.removePeriodicTimeObserver()
//        self.cameraConfig.captureSession?.stopRunning()
        self.audioPlayer = nil
        playerView?.pause()
        stopVideo()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        audioEngine.stop()
        
        playerView?.pause()
//        self.playerView?.removeFromSuperview()
//        self.playerView = nil

        stopVideo()
//        preetiVideoView?.player = nil
    }
    func checkPermission(completion: @escaping ()->Void) {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized,.limited:
            print("Access is granted by user")
            completion()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    completion()
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        @unknown default:
            break
        }
    }
    
    fileprivate func registerNotification() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: NSNotification.Name(rawValue: "App is going background"), object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(appCameToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func appMovedToBackground() {
        if videoRecordingStarted {
            videoRecordingStarted = false
//            self.cameraConfig.stopRecording { (error) in
//                print(error ?? "Video recording error")
//            }
        }
    }
    func playVideo(url: URL?) {
        guard let url = url else {
            return
        }
        preetiVideoView?.url = url
//        preetiVideoView?.player?.play()
    }
    
    func stopVideo() {
        if preetiVideoView?.player?.timeControlStatus != .paused {
            DispatchQueue.main.async {
                self.preetiVideoView?.player?.pause()
            }
        }
    }
    func playVideo() {
        if preetiVideoView?.player?.timeControlStatus != .playing {
            DispatchQueue.main.async {
                self.preetiVideoView?.player?.play()
            }
        }
    }
    func loadPlayer(url:String,urlurl:URL?) {
//        preetiVideoView?.contentMode = .scaleAspectFit
//        preetiVideoView?.player?.isMuted = false
        preetiVideoView?.repeat = .once
        if urlurl != nil {
            preetiVideoView?.url = urlurl!
        }else{
            preetiVideoView?.url = url.convert_to_url!
        }
        if self.checkForHeadphone() {
            DispatchQueue.main.async {
                self.playVideo()
            }
        }
        
        preetiVideoView.actionEndPlayVideo = { [weak self] (tag) in
            print("actionEndPlayVideo")
            self?.stopTimer()
            self?.tappedRecord(UIButton())
        }
        preetiVideoView.actionErrorPlayVideo = { [weak self] (tag) in
          print("actionErrorPlayVideo")
            self?.loading.hideActivityLoading(uiView: self?.view)
        }
        preetiVideoView.actionPlaybackStalled = { [weak self] (tag) in
          print("actionPlaybackStalled")
            self?.loading.hideActivityLoading(uiView: self?.view)
        }
        preetiVideoView.actionError = { [weak self] (tag) in
          print("actionError")
            self?.loading.hideActivityLoading(uiView: self?.view)
        }
        preetiVideoView?.player?.isMuted = false
        preetiVideoView?.player?.volume = 0.5
        do {
//            try AVAudioSession.sharedInstance().setCategory(.playAndRecord)
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord,  options: [.mixWithOthers,.defaultToSpeaker,.allowBluetooth])

            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error {
            print("Load Player error ==>\(error.localizedDescription)")
        }
    }
    @objc func appCameToForeground() {
        print("app enters foreground")
    }
    func presentPlayerView(){
        if let url = videoURL {
            if playerView == nil {
                playerView = MediaPlayerView(frame: previewVideoPlay.frame, videoURL: url)
                previewVideoPlay.addSubview(playerView!)
                playerView?.play()
            }else{
                playerView?.replaceCurrentItem(urlStr: url, complition: { (_) in
                })
                playerView?.play()
            }
            waveform.waveformType = .linear
            waveform.audioURL = url
            waveform.delegate = self
            self.addPeriodicTimeObserver()
        }
    }
    func start() {
        self.animateRecordButton()
        self.btnEnable(isHidden: true)
        self.btnEnablefilter(isHidden: false)
        startTimer()
        start_time = preetiVideoView.player?.currentTime().seconds ?? 0.0
        
//        lycricsTextView.stopLycrics()
//        preetiVideoView?.player?.play()
    }
    func stop() {
        self.stopAnimatingRecordButton()
        self.btnEnable(isHidden: false)
        self.btnEnablefilter(isHidden: true)
        stopTimer()
        end_time = preetiVideoView.player?.currentTime().seconds ?? 0.0
        self.stopVideo()
        preetiVideoView?.player?.seek(to: .zero)
    }
    
    func btnEnable(isHidden:Bool) {
        self.doneButton.isHidden = isHidden
        self.deleteSegmentButton.isHidden = isHidden
        self.collect.isHidden = isHidden
        self.viewLayer.isHidden = isHidden
        //Remove Effect
        self.collect.isHidden = true
    }
    
    func btnEnablefilter(isHidden:Bool) {
        self.collect.isHidden = isHidden
        //Remove Effect
        self.collect.isHidden = true
    }
    func ishiddinColl(isHidden:Bool) {
        self.collect.isHidden = isHidden
        //Remove Effect
        self.collect.isHidden = true
        self.viewLayer.isHidden = isHidden
    }
    @objc fileprivate func showToastForSaved() {
        CommonFunctions.showToastWithMessage("Saved!", displayTime: 2) { }
    }
    
    @objc fileprivate func showToastForRecordingStopped() {
        CommonFunctions.showToastWithMessage("Recording Stopped!", displayTime: 2) { }
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showToast(message: "Could not save!! \n\(error)", fontSize: 12)
        } else {
            showToast(message: "Saved", fontSize: 12.0)
        }
    }
    
    @objc func video(_ video: String, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showToast(message: "Could not save!! \n\(error)", fontSize: 12)
        } else {
            showToast(message: "Saved", fontSize: 12.0)
        }
        print(video)
    }
    func showToast(message:String,fontSize:Float)  {
        CommonFunctions.showToastWithMessage(message, displayTime: 2) {
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        #if targetEnvironment(simulator)
        #else
        try? audioEngine.start()
        self.recorderView.frame = CGRect(x: 0, y: 0, width: self.previewView.frame.size.width, height: self.previewView.frame.size.height)
        self.recorderView.center = self.previewView.center
        #endif
    }
    
    func animateRecordButton() {
        let cornerRadiusAnimation = CASpringAnimation(keyPath: "cornerRadius")
        cornerRadiusAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
        cornerRadiusAnimation.fromValue = 40.0
        cornerRadiusAnimation.toValue = 0.0
        cornerRadiusAnimation.duration = 1.0;
        self.recordButton.layer.cornerRadius = 0.0
        
        
        let pulse1 = CASpringAnimation(keyPath: "transform.scale")
        pulse1.duration = 0.6
        pulse1.fromValue = 1.0
        pulse1.toValue = 1.12
        pulse1.autoreverses = true
        pulse1.repeatCount = 1
        pulse1.initialVelocity = 0.5
        pulse1.damping = 0.8
        
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = 2.0
        animationGroup.repeatCount = HUGE
        animationGroup.animations = [pulse1]
        
        self.recordButton.layer.add(animationGroup, forKey: "pulse")
        self.recordButton.layer.add(cornerRadiusAnimation, forKey: "cornerRadius")
    }
    
    func stopAnimatingRecordButton() {
        self.recordButton.layer.removeAllAnimations()
        self.recordButton.layer.cornerRadius = 40.0
    }
    
    @IBAction func tappedRecord(_ sender: Any) {
        if videoRecordingStarted {
            videoRecordingStarted = false
            self.recordButton.isSelected = false
//            self.cameraConfig.stopRecording { (error) in
//                print(error ?? "Video recording error")
//            }
            #if targetEnvironment(simulator)
            #else
            self.stopMyAudio()
            recorder.stopRecording()
            #endif
        } else if !videoRecordingStarted {
            if videoURL == nil {
                self.recordAgain()
            }else{
                if check_audioAgain {
                    self.AlertControllerCuston(title: "Restart ?", message: "Restart the same song", BtnTitle: ["Cancel","Restart"]) { (str) in
                        if "Cancel" != str{
                            #if targetEnvironment(simulator)
                            #else
                            self.recorderView.recorder.captureSession.startRunning()
                            #endif
                            self.playerView?.pause()
                            self.playerView?.removeFromSuperview()
                            self.playerView = nil
                            self.playVideo()
                            self.btnEnable(isHidden: true)
                        }
                    }
                    
                    check_audioAgain = false
                    return
                }
//                self.AlertControllerCuston(title: "Restart ?", message: "Restart the same song", BtnTitle: ["Cancel","Restart"]) { (str) in
//                    if "Cancel" != str{
//                        #if targetEnvironment(simulator)
//                        #else
//                        self.recorderView.recorder.captureSession.startRunning()
//                        #endif
//                        self.playerView?.pause()
//                        self.playerView?.removeFromSuperview()
//                        self.playerView = nil
                        self.recordAgain()
//                    }
//                }
            }
        }
    }
    @IBAction func tappedRecord2(_ sender: Any) {
        if videoRecordingStarted {
            videoRecordingStarted = false
            self.recordButton.isSelected = false
//            self.cameraConfig.stopRecording { (error) in
//                print(error ?? "Video recording error")
//            }
        } else if !videoRecordingStarted {
            if videoURL == nil {
                self.recordAgain()
            }else{
                self.AlertControllerCuston(title: "Restart ?", message: "Restart the same song", BtnTitle: ["Cancel","Restart"]) { (str) in
                    if "Cancel" != str{
                        self.playerView?.pause()
                        self.playerView?.removeFromSuperview()
                        self.playerView = nil
                        self.recordAgain()
                    }
                }
            }
        }
    }
    func recordAgain()  {
        removePeriodicTimeObserver()
//        self.willStartRecording()
        self.recordButton.isSelected = true
        self._filenameCount = self._filenameCount + 1
        let name = self._filename + "\(self._filenameCount)"
        #if targetEnvironment(simulator)
        #else
        self.startMyAudio()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
            self.recorder.startRecording(name: name)
        }
        #endif
        videoRecordingStarted = true
//        self.recorder.startRecording()
//        self.cameraConfig.recordVideo(name: name) { (url, error) in
//            guard let url = url else {
//                print(error ?? "Video recording error")
//                return
//            }
////            self.endStopRecording()
//            self.videoURL = url
//            self.generateThumbnail(url: url) { (da) in
//            }
//            self.presentPlayerView()
//
////            UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, #selector(self.video(_:didFinishSavingWithError:contextInfo:)), nil)
//        }
    }
    
    func startMyAudio() {
//        audioController.startIOUnit()
        do {
            NodeRecorder.removeTempFiles()
            try audioRecorder?.record()
        } catch let err { Log(err) }
    }

    func stopMyAudio() {
//        audioController.stopIOUnit()
        let fileURL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: .userDomainMask).first!
        let fileNaMe  = "RecordedFile.caf"
        self.micAudioURL = fileURL.appendingPathComponent(fileNaMe)
        try? FileManager.default.removeItem(at: self.micAudioURL!)
        print("File path ==>\(self.micAudioURL!)")
        audioRecorder?.stop()
        if let file = audioRecorder?.audioFile {
            print("Temp files ==>\(NodeRecorder.removeTempFiles())")
            file.extract(to: self.micAudioURL!, from: 0.0, to: file.duration)
            try? audioRecorder?.reset()
        }
    }
    
    @IBAction func tappedCancel(_ sender: Any) {
        if self.videoURL == nil {
            self.navigationController?.popViewController(animated: true)
        }else{
            let vc = AlertVC.instantiate(fromAppStoryboard: .Video)
            vc.titleStr = "Discard entire video?"
            vc.messageStr = "If you go back now, you will lose all of the clips that have been added to your video."
            vc.btnList.append(AlertVCBtn(titleBtn: "Discard", textColor: .red, tag: 1))
            vc.btnList.append(AlertVCBtn(titleBtn: "Keep"))
            vc.select = { [weak self] (titleBtn,tag) in
                if titleBtn == "Discard" {
                    self?.navigationController?.popViewController(animated: true)
                }
            }
            self.present(vc, animated: true) {
            }
        }
    }
    
    @IBAction func tappedDone(_ sender: Any) {
//        self.mergeSegmentsAndUpload(clips: self.clips)
        self.btnRight(sender as! UIButton)
    }
    
    @IBAction func pllllll(_ sender: UIButton) {
        
    }
    
    
    
    @IBAction func btnAudioApplyAction(_ sender: Any) {
        self.audioView.isHidden = true
        guard let time = self.playerView?.player.currentTime().seconds else {
            return
        }
        let dict = sound_list[selectedIndex.item]
        if let url = self.read_url(fromDocumentsWithFileName: dict.sound_id! + ".mp3"){
            soundMurgeArray.append(soundMergeList(url: url, time: time, id: dict.sound_id!))
            self.mergeSegmentsAndUpload()
        }
        playerView?.play()
    }
    @IBAction func btnAudioCancelAction(_ sender: Any) {
        self.miniPlayer.stop()
        self.audioView.isHidden = true
        playerView?.play()
    }
    
    
    func generateThumbnail(url:URL,complition:@escaping(Data)-> Void) {
        AVAsset(url: url).generateThumbnail { [weak self] (image) in
            DispatchQueue.main.async {
                guard let image = image else { return }
                self?.video_thumb = image.jpegData(compressionQuality: 1)
                complition((self?.video_thumb)!)
            }
        }
    }
    
    func setUpMy()  {
        collect.delegate = self
        collect.dataSource = self
        audioView.frame = self.view.frame
        audioView.center = self.view.center
        audioView.isHidden = true
        self.view.addSubview(audioView)
        self.miniPlayer.delegate = self
        self.miniPlayer.tintColor = UIColor.white
        self.miniPlayer.backgroundColor = UIColor.init(red: 204/255.0, green: 221/255.0, blue: 255/255.0, alpha: 1)
        self.miniPlayer.timeLabelVisible = true
        self.miniPlayer.timerColor = UIColor.white
        self.miniPlayer.activeTimerColor = UIColor.white
        self.miniPlayer.activeTrackColor = UIColor.init(red: 204/255.0, green: 170/255.0, blue: 255/255.0, alpha: 1)
        self.miniPlayer.durationTimeInSec = 0
    }
    @IBAction func btnRight(_ sender:UIButton) {
//        self.captureSession.stopRunning()
        if self.videoURL != nil {
            if video_thumb == nil {
                self.generateThumbnail(url: self.videoURL!) { (da) in
                    self.btnRight(sender)
                }
                return
            }
            stopVideo()
            let vc = PreviewVideoVC.instantiate(fromAppStoryboard: .Home)
            vc.hidesBottomBarWhenPushed = true
            vc.videoURL = videoURL!
            vc.lyrics = self.lyrics
            vc.song_name = self.song_name
            vc.music_cat_id = self.music_cat_id
            vc.artist_name = self.artist_name
            vc.old_video_id = self.old_video_id
            vc.video_thumb = video_thumb
            
            vc.soundMurgeArray = self.soundMurgeArray
            vc.videoMurgeArray = self.videoMurgeArray
            vc.sound_list = self.sound_list
            vc._filenameCount = self._filenameCount
            
            vc.select = { [weak self] (str,status) in
                if status == 1 {
                    self?.navigationController?.popViewController(animated: true)
                    self?.select?("",1)
                }else{
//                    self?.cameraConfig.captureSession?.startRunning()
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.AlertControllerCuston(title: alertTitle.alert_error, message: MessageString.video_record, BtnTitle: ["OK"]) { (_) in
            }
        }
    }
    @IBAction func tappedDeleteSegment(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
    }
}


extension SingItVC : AVCaptureVideoDataOutputSampleBufferDelegate {

    func mergeSegmentsAndUpload() {
        DispatchQueue.main.async { [self] in
            if let urlV = self.videoURL {
                self._filenameCount = self._filenameCount + 1
                let name = self._filename + "\(self._filenameCount)"
                self.loading.showActivityLoading(uiView: self.view)
                
                VideoCompositionWriter.shared.pavan_mergeVideoAndAudio(videoUrl: urlV, audioUrl: self.audioURLStr!, audioArrayList: soundMurgeArray, shouldFlipHorizontally: false, filename: name) { (error, url) in
                    DispatchQueue.main.async {
                        self.loading.hideActivityLoading(uiView: self.view)
                    }
                    guard let url = url else {
                        return
                    }
                    print(url)
                    self.videoMurgeArray.append(url)
                    DispatchQueue.main.async {
                        self.createLbl()
                    }
                }
//                VideoCompositionWriter.shared.pavan_mergeVideoAndAudio(videoUrl: urlV, audioArrayList: soundMurgeArray, shouldFlipHorizontally: false, filename: name) { (error, url) in
//                    DispatchQueue.main.async {
//                        self.loading.hideActivityLoading(uiView: self.view)
//                    }
//                    guard let url = url else {
//                        return
//                    }
//                    print(url)
//                    self.videoMurgeArray.append(url)
//                    DispatchQueue.main.async {
//                        self.createLbl()
//                    }
//                }
            }
        }
    }
    func mergeSegmentsAndUpload(complition:@escaping(URL) -> Void) {
        DispatchQueue.main.async { [self] in
            if let urlV = self.videoURL {
                self._filenameCount = self._filenameCount + 1
                let name = self._filename + "\(self._filenameCount)"
                self.loading.showActivityLoading(uiView: self.view)
                guard let micAudio = self.micAudioURL else {
                    return
                }
                
                VideoCompositionWriter.shared.dk_mergeVideoAndAudio(videoUrl: urlV, audioUrl: self.audioURLStr!, micAudioURL: micAudio, audioArrayList: soundMurgeArray, filename: name) { error, url in
                    DispatchQueue.main.async {
                        self.loading.hideActivityLoading(uiView: self.view)
                    }
                    guard let url = url else {
                        print("\(#function) Export URL Not Found  Line No \(#line) ==> \(error?.localizedDescription ?? "No Error")")
                        return
                    }
                    print(url)

                    self.videoMurgeArray.append(url)
                    DispatchQueue.main.async {
                        self.createLbl()
                        complition(url)
                    }
                }
            }
        }
    }
}

extension SingItVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sound_list.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "SingItCell", for: indexPath) as! SingItCell
        cell.lbl_txt.text = sound_list[indexPath.item].effect_name
        cell.lbl_txt.textAlignment = .center
        cell.backgroundColor = UIColor(named: "audioFilterBackground") ?? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        if self.selectedIndex == indexPath {
            cell.backgroundColor = UIColor(named: "audioFilterBackgroundSelected") ?? #colorLiteral(red: 0.9294117647, green: 0.2784313725, blue: 0.3411764706, alpha: 1)
        }
        cell.layer.cornerRadius = 10
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedIndex == indexPath {
            selectedIndex = IndexPath(row: -1, section: 0)
//            self.reverbControl(onoff: false)
            collect.reloadData()
            return
        }
        
        selectedIndex = indexPath
        collect.reloadData()
        
//        if selectedIndex.row != -1 {
//            self.reverbControl(onoff: false)
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            if indexPath.row == 0 {
//                self.echoControl(onoff: true)
//            }
//            if indexPath.row == 1 {
//                self.reverbControl(onoff: true)
//            }
//        }
//        return
        let dict = sound_list[indexPath.item]
        if let url = self.read_url(fromDocumentsWithFileName: dict.sound_id! + ".mp3"){
            if let ti = preetiVideoView.player?.currentTime().seconds {
                let newTi = ti - start_time
                if newTi > 0 {
                    soundMurgeArray.append(soundMergeList(url: url, time: newTi, id: dict.sound_id!))
                    print("added_sound_",newTi)
                }
            }
//            self.displayView(url: url)
        }else{
//            AppNetworking.DOWNLOAD(endPoint: dict.effect_url!, fileName: dict.sound_id!, parameters: [:], mediaType: ".mp3", loader: false) { (status) in
//            } successData: { (url) in
//                if let url = url {
//                    self.displayView(url: url)
//                }
//            } failure: { (error) in
//                print(error)
//            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        selectedIndex = IndexPath(row: -1, section: 0)
        self.reverbControl(onoff: false)
    }
    func displayView(url:URL){
            self.playerView?.pause()
        self.audioView.isHidden = false
        let song = AVPlayerItem(asset: AVAsset(url: url), automaticallyLoadedAssetKeys: ["playable"])
        self.miniPlayer.soundTrack = song
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tt = self.sound_list[indexPath.item]
        let lbl = UILabel()
        lbl.text = tt.effect_name ?? ""
        let pp = lbl.textWidth() + 50
        let h = collect.frame.size.height
        return CGSize(width: pp , height: h)
    }
}
extension SingItVC {
    func playAudioFile() {
//        return
        if audioPlayer == nil {
            guard let url = Bundle.main.url(forResource: "Body_Language", withExtension: "mp3") else { return }
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.record)
                try AVAudioSession.sharedInstance().setActive(true)

                // For iOS 11
                audioPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

                // For iOS versions < 11
                audioPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.m4a.rawValue)

                guard let aPlayer = audioPlayer else { return }
                aPlayer.play()

            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            self.audioPlayer?.play()
        }
    }
    func pauseAudio() {
        audioPlayer?.pause()
    }
}
extension SingItVC {
    // MARK:- Timer Functions
    fileprivate func startTimer(){
         timeSec = 0
         timeMin = 0
        let timeNow = String(format: "%02d", timeSec)
        lblTimer.text = timeNow
        stopTimer()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            self?.timerTick()
        }
        JDB.log("Start Timer ==>%@", timeSec)
    }
    @objc fileprivate func timerTick(){
         timeSec += 1
         if timeSec == 60{
             timeSec = 0
             timeMin += 1
         }
        if timeMin == 0 {
            lblTimer.text = String(format: "%02d", timeSec)
        }else{
            let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
           lblTimer.text = timeNow
        }
        if self.user_have_plan == false {
            if timeSec >= 30 {
                self.stopTimer()
                self.tappedRecord(UIButton())
                
                let vc = AlertVC.instantiate(fromAppStoryboard: .Video)
                vc.titleStr = alertTitle.alert_message
                vc.messageStr = "Upgrade to a paid plan to upload a video of more than 30 second."
                vc.btnList.append(AlertVCBtn(titleBtn: "Cancel", textColor: .red, tag: 1))
                vc.btnList.append(AlertVCBtn(titleBtn: "Upgrade"))
                vc.select = { [weak self] (titleBtn,tag) in
                    if titleBtn == "Upgrade" {
                        self?.navigationController?.popViewController(animated: false)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            self?.select?("",2)
                        }
                    }
                }
                self.present(vc, animated: true) {
                }
            }
        }
    }
    @objc fileprivate func resetTimerToZero(){
         timeSec = 0
         timeMin = 0
         stopTimer()
    }
    @objc fileprivate func resetTimerAndLabel() {
         resetTimerToZero()
        lblTimer.text = String(format: "%02d", timeSec)
//        lblTimer.text = String(format: "%02d:%02d", timeMin, timeSec)
    }
    // stops the timer at it's current time
    @objc fileprivate func stopTimer(){
         timer?.invalidate()
    }
    
}

extension SingItVC: MiniPlayerDelegate {
    func didPlay(player: MiniPlayer) {
    }
    func didStop(player: MiniPlayer) {
    }
    func didPause(player: MiniPlayer) {
    }
}


extension SingItVC: FDWaveformViewDelegate {
    func waveformViewWillRender(_ waveformView: FDWaveformView) {
    }
    func waveformViewDidRender(_ waveformView: FDWaveformView) {
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            waveformView.alpha = 1.0
        })
    }
    func waveformViewWillLoad(_ waveformView: FDWaveformView) {
    }
    func waveformViewDidLoad(_ waveformView: FDWaveformView) {
    }
    func addPeriodicTimeObserver() {
        removePeriodicTimeObserver()
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.1, preferredTimescale: timeScale)
        var total = -1.0
        timeObserverToken = playerView?.player.addPeriodicTimeObserver(forInterval: time,
                                                          queue: .main) {
            [weak self] time in
            if total == -1 || total.isNaN {
                if let tot = self!.playerView?.player.currentItem?.duration.seconds, tot != 0 {
                   let totalSamples = self!.waveform.totalSamples
                    total = Double(totalSamples) / tot
                }
            }
            if !total.isNaN && total != -1 {
                let rem = self?.playerView?.player.currentItem?.currentTime().seconds ?? 1
                let new  = rem * total
                let n = Int(new)
                if n > 0 {
                    self?.waveform.highlightedSamples = 0 ..< Int(n)
                }
            }
        }
    }
    func removePeriodicTimeObserver() {
//        if let timeObserverToken = timeObserverToken {
//            if playerView?.player.rate == 1.0{
//            playerView?.player.removeTimeObserver(timeObserverToken)
//            self.timeObserverToken = nil
//            }
//        }
    }
    func createLbl() {

        for item in lblArr {
            item.removeFromSuperview()
        }
        lblArr.removeAll()
        let d_wirth = self.view.frame.size.width
        var tag = 0
        for item in soundMurgeArray {
            if let tot = self.playerView?.player.currentItem?.duration.seconds {
                let lable = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: viewLayer.frame.size.height))
                lable.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.3003042936, alpha: 0.65)
                let total = Double(d_wirth) / tot
                lable.frame.origin.x = CGFloat(total * item.time)
                let audioAsset = AVURLAsset(url: item.url, options: nil)
                let audioDuration = audioAsset.duration
                let audioDurationSeconds = Double(CMTimeGetSeconds(audioDuration))
                lable.frame.size.width = CGFloat(total * audioDurationSeconds)
                self.viewLayer.addSubview(lable)
                lblArr.append(lable)
                let btn = UIButton(frame: CGRect(x: lable.frame.size.width-viewLayer.frame.size.height, y: 0, width: viewLayer.frame.size.height, height: viewLayer.frame.size.height))
                btn.setImage(#imageLiteral(resourceName: "cross2"), for: .normal)
                btn.tag = tag
                btn.addTarget(self, action: #selector(actionDeleteEffect(_:)), for: .touchUpInside)
                tag = tag + 1
//                lable.addSubview(btn)
            }
        }
    }
    @IBAction func actionDeleteEffect(_ sender:UIButton) {
        soundMurgeArray.remove(at: sender.tag)
        self.mergeSegmentsAndUpload()
    }
}
//tarun
extension SingItVC: AVCaptureAudioDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection) {
        output.connection(with: AVMediaType(rawValue: AVAudioSession.Port.builtInSpeaker.rawValue))
    }
}

struct souldList {
    var name : String
    var url : URL
}
struct soundMergeList {
    var url : URL
    var time : Double
    var id : String
}
