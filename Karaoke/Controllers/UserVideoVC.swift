//
//  UserVideoVC.swift
//  Karaoke
//
//  Created by Ankur  on 17/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class UserVideoVC: UIViewController {
    
    @IBOutlet weak var bottomTab:HomeTab!
    @IBOutlet weak var btnDelete:UIButton!
//    @IBOutlet weak var playVideo:VideoView!
    
    @IBOutlet weak var preetiVideoView:VideoViewPreeti!
    @IBOutlet weak var imageBackground:UIImageView!
    
    var status:Int = 0
    var videoURL : String?
    var video_id : String?
    let addProAPI = UserVideoVM()
    
    deinit {
        preetiVideoView?.player?.pause()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
        self.updateUI()
        self.setUpVideo()
        addProAPI.vc = self
        if nil != video_id {
            self.addProAPI.get_video_details(video_d: video_id!)
        }else{
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "send video id")
        }
        self.updateUI()
        imageBackground.image = #imageLiteral(resourceName: "imgBack")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
        preetiVideoView?.player?.play()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        playVideo.stop()
        preetiVideoView?.player?.pause()
    }
    func playVideo(url: URL?) {
        guard let url = url else {
            return
        }
        preetiVideoView?.url = url
        preetiVideoView?.player?.play()
    }
    
    func stopVideo() {
        preetiVideoView?.player?.pause()
    }
    func setUp() {
        bottomTab.favoriteView.isHidden = true
        bottomTab.lblSing.text = "Sing It Again"
        bottomTab.btnComment.addTarget(self, action: #selector(btnComment(_:)), for: .touchUpInside)
        bottomTab.btnSing.addTarget(self, action: #selector(btnSingIt(_:)), for: .touchUpInside)
        bottomTab.btnLike.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
    }
    func setUpVideo() {
        if nil != self.videoURL {
            self.loadPlayer()
        }
        
//        self.playVideo.isLoop = true
//        self.playVideo.playbackSlider.isHidden = true
//        if nil != videoURL {
//            self.playVideo.configure(url: videoURL!)
//        }
    }
    func loadPlayer() {
        preetiVideoView?.contentMode = .scaleAspectFill
        preetiVideoView?.player?.isMuted = false
        preetiVideoView?.repeat = .loop
        preetiVideoView?.url = self.videoURL!.convert_to_url!
        preetiVideoView?.player?.play()
        let loading = LoadingView()
        loading.showActivityLoading(uiView: self.view)
        preetiVideoView.actionEndPlayVideo = { [weak self] (tag) in
            print("actionEndPlayVideo")
        }
        preetiVideoView.actionErrorPlayVideo = { [weak self] (tag) in
          print("actionErrorPlayVideo")
            loading.hideActivityLoading(uiView: self?.view)
        }
        preetiVideoView.actionPlaybackStalled = { [weak self] (tag) in
          print("actionPlaybackStalled")
            loading.hideActivityLoading(uiView: self?.view)
        }
        preetiVideoView.actionError = { [weak self] (tag) in
          print("actionError")
            loading.hideActivityLoading(uiView: self?.view)
        }
        
//        if preetiVideoView != nil {
//            imageBackground.addSubview(preetiVideoView!)
//        }
//        self.getPlayerPlay(viewFram: self.view, urlStr: self.videoURL!.convert_to_url!) { (_) in
//            self.imageBackground.layer.addSublayer(mainavPlayerLayer)
//            if mainavPlayerLayer != nil {
//                mainavPlayerLayer.player!.play()
//            }
//        }
    }
    func updateUI() {
        if status == 0 {
            btnDelete.isHidden = true
        }else{
            btnDelete.isHidden = false
        }
    }
    func upNewData(new:Bool) {
        if let dict = videoDetailCache[video_id!] {
            bottomTab.updateLike = dict.total_likes ?? "0"
            bottomTab.updateComment = dict.total_comments ?? "0"
//            bottomView.imgFavorite.image = dict.user_fav_status == 0 ? #imageLiteral(resourceName: "fav") : #imageLiteral(resourceName: "favR")
//            bottomView.imgLike.image = dict.user_like_status == 0 ? #imageLiteral(resourceName: "heartW") : #imageLiteral(resourceName: "heartR")
        }
    }
    @IBAction func btn_Back(_ sender: UIButton){
//        playVideo.stop()
        preetiVideoView?.player?.pause()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Delete(_ sender: UIButton){
        preetiVideoView?.player?.pause()
        let vc = DeletePopupVC.instantiate(fromAppStoryboard: .Video)
        vc.video_id = video_id!
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.navigationController?.popViewController(animated: true)
            }else{
                self?.preetiVideoView?.player?.play()
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnComment(_ sender:UIButton) {
        let vc = CommentVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.video_id = video_id!
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnSingIt(_ sender:UIButton) {        
        preetiVideoView?.player?.pause()
        guard let dict1 = videoDetailCache[video_id!] else {
            return
        }
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLike(_ sender:UIButton) {
        let vc = LikeVC.instantiate(fromAppStoryboard: .Profile)
        vc.hidesBottomBarWhenPushed = true
        vc.video_id = video_id!
        self.navigationController?.pushViewController(vc, animated: true)
//        if bottomTab.imgLike.image == #imageLiteral(resourceName: "heartR") && sender.tag == 0 {
//            bottomTab.imgLike.image = #imageLiteral(resourceName: "heartW")
//            sender.tag = 1
//        }else{
//            bottomTab.imgLike.image = #imageLiteral(resourceName: "heartR")
//            sender.tag = 0
//        }
    }
}
