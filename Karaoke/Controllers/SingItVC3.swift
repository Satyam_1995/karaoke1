//
//  SingItVC3.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 25/04/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit
import AVFoundation

extension SingItVC {
    func audioSetup() {
    
        engine = AVAudioEngine()

        playerNode = AVAudioPlayerNode()

        reverb = AVAudioUnitReverb()
        reverb.loadFactoryPreset(.largeHall2)
        reverb.wetDryMix = 50

        
        // node for echo
        echo = AVAudioUnitDistortion()
        echo.loadFactoryPreset(.multiEcho1)
        
        
        engine.attach(playerNode)
        engine.attach(reverb)
        engine.attach(echo)
        do {
            let bun = Bundle.main.path(forResource: "demo3", ofType: "mp4")!
            let uu = URL(fileURLWithPath: bun)
            audioFile = try AVAudioFile(forReading: self.audioURLStr!)
            engine.connect(playerNode, to: engine.mainMixerNode, format: audioFile.processingFormat)
            
//            audioFile = try AVAudioFile(forReading: URL(fileURLWithPath: Bundle.main.path(forResource: "demo3", ofType: "mp4")!))
        } catch {
            print(error)
        }
        offsetFrame = 0
    }
    func reverbControl(onoff: Bool) {
        
        if playerNode == nil {
            print("Player node is not null")
            return
        }
        
        let status = playerNode.isPlaying

        offsetFrame = Int(self.slider.value)
        
        if status {
        
            playerNode.pause()

            playerNode.stop()

            engine.stop()

        }

        if onoff {

            engine.connect(playerNode, to: reverb, format: audioFile.processingFormat)
            engine.connect(reverb, to: engine.mainMixerNode, format: audioFile.processingFormat)

            playerNode.scheduleSegment(audioFile,
                startingFrame: AVAudioFramePosition(self.slider.value),
                frameCount: AVAudioFrameCount(audioFile.length) - UInt32(self.slider.value),
                at: nil,
                completionHandler: self.completion)

        } else {

            engine.connect(playerNode, to: engine.mainMixerNode, format: audioFile.processingFormat)

            playerNode.scheduleSegment(audioFile,
                startingFrame: AVAudioFramePosition(self.slider.value),
                frameCount: AVAudioFrameCount(audioFile.length) - UInt32(self.slider.value),
                at: nil,
                completionHandler: self.completion)
        }

        if status {

            do {
                try engine.start()
            } catch {
                print(error)
            }

            playerNode.play()
        }
    }
    func echoControl(onoff: Bool) {

        let status = playerNode.isPlaying

        offsetFrame = Int(self.slider.value)
        
        if status {
        
            playerNode.pause()

            playerNode.stop()

            engine.stop()

        }

        if onoff {

            engine.connect(playerNode, to: echo, format: audioFile.processingFormat)
            engine.connect(echo, to: engine.mainMixerNode, format: audioFile.processingFormat)

            playerNode.scheduleSegment(audioFile,
                startingFrame: AVAudioFramePosition(self.slider.value),
                frameCount: AVAudioFrameCount(audioFile.length) - UInt32(self.slider.value),
                at: nil,
                completionHandler: self.completion)

        } else {

            engine.connect(playerNode, to: engine.mainMixerNode, format: audioFile.processingFormat)

            playerNode.scheduleSegment(audioFile,
                startingFrame: AVAudioFramePosition(self.slider.value),
                frameCount: AVAudioFrameCount(audioFile.length) - UInt32(self.slider.value),
                at: nil,
                completionHandler: self.completion)
        }

        if status {

            do {
                try engine.start()
            } catch {
                print(error)
            }

            playerNode.play()
        }
    }
    func play(onoff: Bool) {
    
        if onoff {

            playerNode.scheduleSegment(audioFile,
                startingFrame: AVAudioFramePosition(self.slider.value),
                frameCount: AVAudioFrameCount(audioFile.length) - UInt32(self.slider.value),
                at: nil,
                completionHandler: self.completion)

            do {

                try engine.start()

                playerNode.play()

//                self.playButton.setImage(UIImage(named: "Stop.png"), for: .normal)

                timerSound = Timer.scheduledTimer(timeInterval: 0.05,
                target: self, selector: #selector(self.intervalTimer), userInfo: nil, repeats: true)

                self.slider.maximumValue = Float(audioFile.length)

            } catch {
                print(error)
            }

        } else {

            if playerNode.isPlaying {

                playerNode.pause()

                playerNode.stop()

                engine.stop()
                
                offsetFrame = Int(self.slider.value)
            }

            timerSound.invalidate()

//            self.playButton.setImage(UIImage(named: "Play.png"), for: .normal)
        }
    }
    func completion() {

        if playerNode.isPlaying {

            completionFlag = true

        }

    }
    
    @objc func intervalTimer() {
    
        if completionFlag {

            self.play(onoff: false)
            
            completionFlag = false
            
            self.slider.value = 0
            
            offsetFrame = 0
            
            return

        }

        if playerNode.isPlaying {

            if let nodeTime = playerNode.lastRenderTime {
                self.slider.value = Float((playerNode.playerTime(forNodeTime: nodeTime)?.sampleTime)!) + Float(offsetFrame)
            }
        }
    }
}

