//
//  FollowerVC.swift
//  Karaoke
//
//  Created by Ankur  on 19/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class FollowerVC: UIViewController {
    
    @IBOutlet weak var FollowingView : UIView!
    @IBOutlet weak var FollowersView : UIView!
    @IBOutlet weak var btnFollower : UIButton!
    @IBOutlet weak var btnFollowing : UIButton!
    
    @IBOutlet weak var txtSearch : UITextField!
    
    @IBOutlet weak var table:UITableView!
    @IBOutlet weak var table1:UITableView!
    var follower_id = ""
    var status:Int = 0
    let addProAPI = FollowerVM()
    var fromVC : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        if status == 1 {
            hideOtherView(viewC: FollowingView, tag: status, btn: btnFollowing)
        }else{
            hideOtherView(viewC: FollowersView, tag: status, btn: btnFollower)
        }
        Setup()
        addProAPI.vc = self
        if fromVC != 0 {
            addProAPI.get_friend_follower_following(user_id: UserDetail.shared.getUserId(), friend_id: follower_id)
        }else{
            addProAPI.get_follower_following(user_id: follower_id)
        }
    }    
    
    func Setup(){
        
        self.table.register(UINib(nibName: "FollowerCell", bundle: nil), forCellReuseIdentifier: "FollowerCell")
        self.table1.register(UINib(nibName: "FollowerCell", bundle: nil), forCellReuseIdentifier: "FollowerCell")
        table.delegate = self
        table.dataSource = self
        table1.delegate = self
        table1.dataSource = self
        txtSearch.delegate = self
    }
    
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func hideOtherView(viewC:UIView,tag:Int,btn:UIButton) {
        FollowingView.isHidden = true
        FollowersView.isHidden = true
        //        viewC.isHidden = false
        btnFollower.backgroundColor = .clear
        btnFollowing.backgroundColor = .clear
        btnFollower.setTitleColor(#colorLiteral(red: 0.09803921569, green: 0.1019607843, blue: 0.3529411765, alpha: 1), for: .normal)
        btnFollowing.setTitleColor(#colorLiteral(red: 0.09803921569, green: 0.1019607843, blue: 0.3529411765, alpha: 1), for: .normal)
        btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        btn.backgroundColor = #colorLiteral(red: 0.09803921569, green: 0.1019607843, blue: 0.3529411765, alpha: 1)
        if tag == 1{
            FollowingView.isHidden = false
            self.addProAPI.following_list = self.addProAPI.following_list_all
            self.table.reloadData()
        }else{
            FollowersView.isHidden = false
            self.addProAPI.follower_list = self.addProAPI.follower_list_all
            self.table1.reloadData()
        }
        txtSearch.text = ""        
    }
    
    @IBAction func btnClicks(_ sender:UIButton) {
        hideOtherView(viewC: FollowingView, tag: sender.tag, btn: sender)
    }
    func updateData() {
        self.table.reloadData()
        self.table1.reloadData()
    }
}


extension FollowerVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table {
            return self.addProAPI.following_list.count
        }else{
            return self.addProAPI.follower_list.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table {
            let cell = table.dequeueReusableCell(withIdentifier: "FollowerCell", for: indexPath) as! FollowerCell
            let dict = self.addProAPI.following_list[indexPath.row]
            cell.lblUserName.text = dict.user_name
            cell.lbltotalFollowers.text = "\(dict.total_follower ?? 0)".formatPoints() + " Followers"
            if let url = dict.user_profile?.convert_to_url {
                cell.imgUSer.af.setImage(withURL: url)
            }
            cell.dict = dict
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.addTarget(self, action: #selector(btnFollow(_:)), for: .touchUpInside)
            return cell
        }else{
            
            let cell = table1.dequeueReusableCell(withIdentifier: "FollowerCell", for: indexPath) as! FollowerCell
            let dict = self.addProAPI.follower_list[indexPath.item]
            cell.lblUserName.text = dict.user_name
            cell.lbltotalFollowers.text = "\(dict.total_follower ?? 0)".formatPoints() + " Followers"
            if let url = dict.user_profile?.convert_to_url {
                cell.imgUSer.af.setImage(withURL: url)
            }
            cell.btnFollow.tag = indexPath.row
            if fromVC != 0 {
                cell.dict = dict
                cell.btnFollow.addTarget(self, action: #selector(btnFollow2(_:)), for: .touchUpInside)
            }else{
                cell.dictFollowers = dict
                cell.btnFollow.addTarget(self, action: #selector(btnFollow1(_:)), for: .touchUpInside)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = .clear
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width-20, height: headerView.frame.height)
        if tableView == table{
            label.text = "\(self.addProAPI.following_list.count) People"
        }else{
            label.text = "\(self.addProAPI.follower_list.count) People"
        }
        label.textColor = .darkGray
        label.font = UIFont(name: "Poppins-Medium", size: 10.0)
        label.textAlignment = .left
        headerView.addSubview(label)
        return headerView
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict : Follower? = nil
        if tableView == table {
             dict = self.addProAPI.following_list[indexPath.row]
        }else{
            dict = self.addProAPI.follower_list[indexPath.item]
        }
        guard let friend_id = dict?.friend_id else { return }
        let vc = OtherUserProfileVC.instantiate(fromAppStoryboard: .Home)
        vc.friend_id = friend_id
        vc.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnFollow(_ sender:UIButton) {
        
        let dict = self.addProAPI.following_list[sender.tag]
        guard let friend_id = dict.friend_id else {
            return
        }
        guard let cell = table.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? FollowerCell else{
            return
        }
        var follow : follow_status = .follow
        if dict.user_follow_status == 0 {
            follow = .follow
            cell.btnFollow.setTitle("Unfollow", for: .normal)
            cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
            cell.btnFollow.setTitleColor(.darkGray, for: .normal)
        }else{
            follow = .unFollow
            cell.btnFollow.setTitle("Follow", for: .normal)
            cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
            cell.btnFollow.setTitleColor(.white, for: .normal)
        }
        WebServices.follow_unfollow(friend_id: friend_id, follow_type: follow, loader: false) { (json) in
        } successData: { (data) in
            if dict.user_follow_status == 0 {
                self.addProAPI.following_list[sender.tag].user_follow_status = 1
            }else{
                self.addProAPI.following_list[sender.tag].user_follow_status = 0
            }
        } failure: { (err) -> (Void) in
            print(err)
            if dict.friend_follow_status != 0 {
                cell.btnFollow.setTitle("Unfollow", for: .normal)
                cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
                cell.btnFollow.setTitleColor(.darkGray, for: .normal)
            }else{
                cell.btnFollow.setTitle("Follow", for: .normal)
                cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
                cell.btnFollow.setTitleColor(.white, for: .normal)
            }
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
        }
    }
    
    @IBAction func btnFollow1(_ sender:UIButton) {
        let dict = self.addProAPI.follower_list[sender.tag]
        guard let friend_id = dict.friend_id else {
            return
        }
        let cell = table1.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? FollowerCell
        var follow : follow_status = .follow
        if dict.user_follow_status == 0 {
            follow = .follow
            cell?.btnFollow.setTitle("Unfollow", for: .normal)
            cell?.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
            cell?.btnFollow.setTitleColor(.darkGray, for: .normal)
        }else{
            follow = .unFollow
            cell?.btnFollow.setTitle("Follow Back", for: .normal)
            cell?.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
            cell?.btnFollow.setTitleColor(.white, for: .normal)
        }
        WebServices.follow_unfollow(friend_id: friend_id, follow_type: follow, loader: false) { (json) in
        } successData: { (data) in
            if dict.user_follow_status == 0 {
                self.addProAPI.follower_list[sender.tag].user_follow_status = 1
            }else{
                self.addProAPI.follower_list[sender.tag].user_follow_status = 0
            }
        } failure: { (err) -> (Void) in
            print(err)
            if dict.friend_follow_status != 0 {
                cell?.btnFollow.setTitle("Unfollow", for: .normal)
                cell?.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
                cell?.btnFollow.setTitleColor(.darkGray, for: .normal)
            }else{
                cell?.btnFollow.setTitle("Follow Back", for: .normal)
                cell?.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
                cell?.btnFollow.setTitleColor(.white, for: .normal)
            }
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
        }
    }
    @IBAction func btnFollow2(_ sender:UIButton) {
        
        let dict = self.addProAPI.follower_list[sender.tag]
        guard let friend_id = dict.friend_id else {
            return
        }
        guard let cell = table1.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? FollowerCell else{
            return
        }
        var follow : follow_status = .follow
        if dict.user_follow_status == 0 {
            follow = .follow
            cell.btnFollow.setTitle("Unfollow", for: .normal)
            cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
            cell.btnFollow.setTitleColor(.darkGray, for: .normal)
        }else{
            follow = .unFollow
            cell.btnFollow.setTitle("Follow", for: .normal)
            cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
            cell.btnFollow.setTitleColor(.white, for: .normal)
        }
        WebServices.follow_unfollow(friend_id: friend_id, follow_type: follow, loader: false) { (json) in
        } successData: { (data) in
            if dict.user_follow_status == 0 {
                self.addProAPI.follower_list[sender.tag].user_follow_status = 1
            }else{
                self.addProAPI.follower_list[sender.tag].user_follow_status = 0
            }
        } failure: { (err) -> (Void) in
            print(err)
            if dict.friend_follow_status != 0 {
                cell.btnFollow.setTitle("Unfollow", for: .normal)
                cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
                cell.btnFollow.setTitleColor(.darkGray, for: .normal)
            }else{
                cell.btnFollow.setTitle("Follow", for: .normal)
                cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
                cell.btnFollow.setTitleColor(.white, for: .normal)
            }
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
        }
    }
}

extension FollowerVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if txtSearch == textField {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            if FollowingView.isHidden == false {
                self.filterDataFollowing(newString: newString)
            }else{
                self.filterDataFollowers(newString: newString)
            }
        }
        return true
    }
    func filterDataFollowing(newString:String) {
        if newString == "" {
            self.addProAPI.following_list = self.addProAPI.following_list_all
            table.reloadData()
            return
        }
        let channelDescriptors3 = self.addProAPI.following_list_all
        self.addProAPI.following_list = []
        self.addProAPI.following_list = channelDescriptors3.filter({(DiscoverModel) -> Bool in
            var dataString = DiscoverModel.user_name ?? ""
            dataString = dataString + " " + (DiscoverModel.name ?? "")
            if dataString != " " {
                return dataString.lowercased().range(of: newString.lowercased(), options: .caseInsensitive) != nil
            }else{
                return true
            }
        })
        table.reloadData()
    }
    func filterDataFollowers(newString:String) {
        if newString == "" {
            self.addProAPI.follower_list = self.addProAPI.follower_list_all
            table1.reloadData()
            return
        }
        let channelDescriptors3 = self.addProAPI.follower_list_all
        self.addProAPI.follower_list = []
        self.addProAPI.follower_list = channelDescriptors3.filter({(DiscoverModel) -> Bool in
            var dataString = DiscoverModel.user_name ?? ""
            dataString = dataString + " " + (DiscoverModel.name ?? "")
            if dataString != " " {
                return dataString.lowercased().range(of: newString.lowercased(), options: .caseInsensitive) != nil
            }else{
                return true
            }
        })
        table1.reloadData()
    }
}
