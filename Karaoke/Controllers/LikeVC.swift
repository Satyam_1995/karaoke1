//
//  LikeVC.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 28/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit

class LikeVC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    var video_id = ""
    var status:Int = 0
    let addProAPI = LikeVM()
    var fromVC : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Setup()
        addProAPI.vc = self
        addProAPI.get_like_list(video_id:video_id)
    }
    
    func Setup(){
        self.table.register(UINib(nibName: "FollowerCell", bundle: nil), forCellReuseIdentifier: "FollowerCell")
        table.delegate = self
        table.dataSource = self
    }
    
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func updateData() {
        self.table.reloadData()
    }
}
extension LikeVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.addProAPI.like_list.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "FollowerCell", for: indexPath) as! FollowerCell
        let dict = self.addProAPI.like_list[indexPath.row]
        cell.lblUserName.text = dict.name
//            cell.lbltotalFollowers.text = "\(dict.total_follower ?? 0)".formatPoints() + " Followers"
        cell.lbltotalFollowers.isHidden = true
        if let url = dict.profile_img?.convert_to_url {
            cell.imgUSer.af.setImage(withURL: url)
        }
        if let url = dict.profile_img?.convert_to_url {
            cell.imgUSer.af.setImage(withURL: url)
        }
        //            cell.dict = dict
        //            cell.btnFollow.tag = indexPath.row
        //            cell.btnFollow.addTarget(self, action: #selector(btnFollow(_:)), for: .touchUpInside)
        cell.btnFollow.isHidden = true
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = .clear
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width-20, height: headerView.frame.height)
        if tableView == table{
            label.text = "\(self.addProAPI.like_list.count) People"
        }else{
            label.text = "\(self.addProAPI.like_list.count) People"
        }
        label.textColor = .darkGray
        label.font = UIFont(name: "Poppins-Medium", size: 10.0)
        label.textAlignment = .left
        headerView.addSubview(label)
        return headerView
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.addProAPI.like_list[indexPath.row]
        
        guard let friend_id = dict.user_id else { return }
        let vc = OtherUserProfileVC.instantiate(fromAppStoryboard: .Home)
        vc.friend_id = friend_id
        vc.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    @IBAction func btnFollow(_ sender:UIButton) {
//
//        let dict = self.addProAPI.like_list[sender.tag]
//        guard let friend_id = dict.user_id else {
//            return
//        }
//        guard let cell = table.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? FollowerCell else{
//            return
//        }
//        var follow : follow_status = .follow
//        if dict.user_follow_status == 0 {
//            follow = .follow
//            cell.btnFollow.setTitle("Unfollow", for: .normal)
//            cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
//            cell.btnFollow.setTitleColor(.darkGray, for: .normal)
//        }else{
//            follow = .unFollow
//            cell.btnFollow.setTitle("Follow", for: .normal)
//            cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
//            cell.btnFollow.setTitleColor(.white, for: .normal)
//        }
//        WebServices.follow_unfollow(friend_id: friend_id, follow_type: follow, loader: false) { (json) in
//        } successData: { (data) in
//            if dict.user_follow_status == 0 {
//                self.addProAPI.following_list[sender.tag].user_follow_status = 1
//            }else{
//                self.addProAPI.following_list[sender.tag].user_follow_status = 0
//            }
//        } failure: { (err) -> (Void) in
//            print(err)
//            if dict.friend_follow_status != 0 {
//                cell.btnFollow.setTitle("Unfollow", for: .normal)
//                cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
//                cell.btnFollow.setTitleColor(.darkGray, for: .normal)
//            }else{
//                cell.btnFollow.setTitle("Follow", for: .normal)
//                cell.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
//                cell.btnFollow.setTitleColor(.white, for: .normal)
//            }
//            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
//            }
//        }
//    }
}
