//
//  ExploreVC.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class ExploreVC: UIViewController {
    
    @IBOutlet weak var collect1:UICollectionView!
    @IBOutlet weak var collect2:UICollectionView!
    @IBOutlet weak var collect3:UICollectionView!
    @IBOutlet weak var collect4:UICollectionView!
    
    let imgArr = [#imageLiteral(resourceName: "i14"),#imageLiteral(resourceName: "i8"),#imageLiteral(resourceName: "i9"),#imageLiteral(resourceName: "i7")]
    let imgArr1 = [#imageLiteral(resourceName: "i15"),#imageLiteral(resourceName: "i9"),#imageLiteral(resourceName: "i7"),#imageLiteral(resourceName: "s3")]
    let viewModel = ExploreVM()
    var forst = true
    override func viewDidLoad() {
        super.viewDidLoad()
        Setup()
        viewModel.vc = self
        viewModel.explore_screen()
//        if UIDevice.current.hasNotch {
//            let ss = self.iScreenSizes()
//            tabBarController!.tabBar.frame = CGRect(x: 10, y: ss - 30, width: tabBarController!.tabBar.frame.size.width, height: 80.0)
//        }
    }
    @IBAction func btn_Back(_ sender: UIButton){
        
        tabBarController?.selectedIndex = 0
    }
    @IBAction func btn_Search(_ sender: UIButton){
        let vc = SongBookPageVC.instantiate(fromAppStoryboard: .Home)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        if !forst && viewModel.trending_videos.count == 0{
            viewModel.explore_screen()
        }
        forst = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        tabBarController?.tabBar.isHidden = false
    }
    @IBAction func btnSeeAll1(_ sender:UIButton) {
//        let vc = SingItVC1.instantiate(fromAppStoryboard: .Home)
//        vc.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(vc, animated: true)
        let vc = SeeAllVC.instantiate(fromAppStoryboard: .Home)
        vc.Str = "New Trending"
        vc.musicCat = .trending
//        self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnSeeAll2(_ sender:UIButton) {
        let vc = SeeAllVC.instantiate(fromAppStoryboard: .Home)
        vc.Str = "Recommended"
        vc.musicCat = .recomended
//        self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSeeAll3(_ sender:UIButton) {
        let vc = SeeAllVC.instantiate(fromAppStoryboard: .Home)
        vc.Str = "Top Singers"
        vc.musicCat = .top_singer
//        self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSeeAll4(_ sender:UIButton) {
        let vc = SeeAllVC.instantiate(fromAppStoryboard: .Home)
        vc.Str = "Top Artists"
        vc.musicCat = .top_artist
//        self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func Setup(){
        collect1.delegate = self
        collect1.dataSource = self
        collect2.delegate = self
        collect2.dataSource = self
        collect3.delegate = self
        collect3.dataSource = self
        collect4.delegate = self
        collect4.dataSource = self

        collect1.register(UINib(nibName: "ExploreCell", bundle: nil), forCellWithReuseIdentifier: "ExploreCell")
        collect2.register(UINib(nibName: "ExploreCell", bundle: nil), forCellWithReuseIdentifier: "ExploreCell")
        collect3.register(UINib(nibName: "ExploreCell", bundle: nil), forCellWithReuseIdentifier: "ExploreCell")
        collect4.register(UINib(nibName: "ExploreCell", bundle: nil), forCellWithReuseIdentifier: "ExploreCell")
    }
    func updatedata(new:Bool)  {
        DispatchQueue.main.async {
            self.reloadAllData()
        }
    }
    func reloadAllData(name : [Int] = [1,2,3,4])  {
        for item in name {
            if item == 1 {
                self.collect1.reloadData()
            }
            if item == 2 {
                self.collect2.reloadData()
            }
            if item == 3 {
                self.collect3.reloadData()
            }
            if item == 4 {
                self.collect4.reloadData()
            }
        }
    }
}

extension ExploreVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collect1 {
            return viewModel.trending_videos.count
            return imgArr.count
        }else if collectionView == collect2 {
            return viewModel.recomended_videos.count
            return imgArr1.count
        }else if collectionView == collect3 {
            return viewModel.top_singer_videos.count
            return imgArr.count
        }else{
            return viewModel.top_artist_videos.count
            return imgArr1.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCell", for: indexPath) as! ExploreCell
        let dict : Popular_videos?
        
        if collectionView == collect1 {
            dict = viewModel.trending_videos[indexPath.item]
            cell1.btn_share.accessibilityHint = "1"
        }else if collectionView == collect2 {
            dict = viewModel.recomended_videos[indexPath.item]
            cell1.btn_share.accessibilityHint = "2"
        }else if collectionView == collect3 {
            dict = viewModel.top_singer_videos[indexPath.item]
            cell1.btn_share.accessibilityHint = "3"
        }else{
            dict = viewModel.top_artist_videos[indexPath.item]
            cell1.btn_share.accessibilityHint = "4"
        }
        cell1.viewCon = self
        cell1.updateData = dict
        
        cell1.btn_Sing.tag = indexPath.row
        
        cell1.btn_Sing.accessibilityElements = [collectionView]
        cell1.btn_share.tag = indexPath.row
        cell1.btn_Sing.addTarget(self, action: #selector(btnSing(_:)), for: .touchUpInside)
        cell1.btn_share.addTarget(self, action: #selector(btnShare(_:)), for: .touchUpInside)
        return cell1
        
        if collectionView == collect1 {
            cell1.imgSong.image = imgArr[indexPath.row]
            cell1.btn_Sing.tag = indexPath.row
            cell1.btn_share.tag = indexPath.row
            cell1.btn_Sing.addTarget(self, action: #selector(btnSing(_:)), for: .touchUpInside)
            cell1.btn_share.addTarget(self, action: #selector(btnShare(_:)), for: .touchUpInside)
            return cell1
        }else if collectionView == collect2 {
            let cell2 = collect2.dequeueReusableCell(withReuseIdentifier: "ExploreCell", for: indexPath) as! ExploreCell
            DispatchQueue.main.async {
                cell2.imgSong.image = self.imgArr1[indexPath.row]
                cell2.btn_Sing.tag = indexPath.row
                cell2.btn_share.tag = indexPath.row
                cell2.btn_Sing.addTarget(self, action: #selector(self.btnSing(_:)), for: .touchUpInside)
                cell2.btn_share.addTarget(self, action: #selector(self.btnShare(_:)), for: .touchUpInside)
            }
            return cell2
        }else if collectionView == collect3 {
            let cell3 = collect3.dequeueReusableCell(withReuseIdentifier: "ExploreCell", for: indexPath) as! ExploreCell
            DispatchQueue.main.async {
                cell3.updateFocusIfNeeded()
                
                cell3.imgSong.image = self.imgArr[indexPath.row]
                cell3.btn_Sing.tag = indexPath.row
                cell3.btn_share.tag = indexPath.row
                cell3.btn_Sing.addTarget(self, action: #selector(self.btnSing(_:)), for: .touchUpInside)
                cell3.btn_share.addTarget(self, action: #selector(self.btnShare(_:)), for: .touchUpInside)
            }
            return cell3
        }else{
            let cell4 = collect4.dequeueReusableCell(withReuseIdentifier: "ExploreCell", for: indexPath) as! ExploreCell
            cell4.imgSong.image = imgArr1[indexPath.row]
            cell4.btn_Sing.tag = indexPath.row
            cell4.btn_share.tag = indexPath.row
            cell4.btn_Sing.addTarget(self, action: #selector(self.btnSing(_:)), for: .touchUpInside)
            cell4.btn_share.addTarget(self, action: #selector(self.btnShare(_:)), for: .touchUpInside)
            return cell4
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var dict : Popular_videos?
        if collectionView == collect1 {
            dict = viewModel.trending_videos[indexPath.item]
        }else if collectionView == collect2 {
            dict = viewModel.recomended_videos[indexPath.item]
        }else if collectionView == collect3 {
            dict = viewModel.top_singer_videos[indexPath.item]
        }else if collectionView == collect4 {
            dict = viewModel.top_artist_videos[indexPath.item]
        }
        guard let dict1 = dict else {
            return
        }
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.hidesBottomBarWhenPushed = true
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)

        return
        guard let videoURL = dict?.video_file, let v_id = dict?.video_id  else {
            return
        }
        
        self.moveToVideoDetail(videoURL: videoURL, v_id: v_id, collect: collectionView, index: indexPath.item)
        return
        
        if collectionView == collect1 {
            //            let vc = SongsInfoVC.instantiate(fromAppStoryboard: .Home)
            //            vc.status = 0
            //            vc.hidesBottomBarWhenPushed = true
            //            self.navigationController?.pushViewController(vc, animated: true)
            let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
            if let videoURL =  dict1.video_file?.convert_to_string, let v_id = dict1.video_id {
                vc.videoURL = videoURL
                vc.video_id = v_id
            }
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView == collect2 {
            let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
            if let videoURL =  dict1.video_file?.convert_to_string, let v_id = dict1.video_id {
                vc.videoURL = videoURL
                vc.video_id = v_id
            }
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else if collectionView == collect3 {
            let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
            if let videoURL =  dict1.video_file?.convert_to_string, let v_id = dict1.video_id {
                vc.videoURL = videoURL
                vc.video_id = v_id
            }
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else{
            let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
            if let videoURL =  dict1.video_file?.convert_to_string, let v_id = dict1.video_id {
                vc.videoURL = videoURL
                vc.video_id = v_id
            }
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collect1 {
            let w = collect1.frame.size.width/2.8
            let h = collect1.frame.size.height
            
            return CGSize(width: w, height: h)
        }else if collectionView == collect2 {
            let w = collect2.frame.size.width/2.8
            let h = collect2.frame.size.height
            
            return CGSize(width: w, height: h)
        }else if collectionView == collect3 {
            let w = collect3.frame.size.width/2.8
            let h = collect3.frame.size.height
            
            return CGSize(width: w, height: h)
        }else{
            let w = collect4.frame.size.width/2.8
            let h = collect4.frame.size.height
            return CGSize(width: w, height: h)
        }
        
    }
    
    
    @IBAction func btnSing(_ sender:UIButton) {
       
        guard let collectionView = sender.accessibilityElements?.first as? UICollectionView else {
            return
        }
        
        var dict : Popular_videos?
        
        if collectionView == collect1 {
            dict = viewModel.trending_videos[sender.tag]
        }else if collectionView == collect2 {
            dict = viewModel.recomended_videos[sender.tag]
        }else if collectionView == collect3 {
            dict = viewModel.top_singer_videos[sender.tag]
        }else if collectionView == collect4 {
            dict = viewModel.top_artist_videos[sender.tag]
        }
//        guard let videoURL = dict?.video_file, let v_id = dict?.video_id  else {
//            return
//        }
        guard let dict1 = dict else {
            return
        }
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func moveToVideoDetail(videoURL:String,v_id:String,collect:UICollectionView,index:Int) {
        
        let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        if let videoURL =  videoURL.convert_to_string {
            vc.videoURL = videoURL
            vc.video_id = v_id
            vc.actionVideo = { [weak self] (msg,v_id) in
                if collect == self?.collect1 {
                    print("collect1")
                    self?.viewModel.trending_videos.remove(at: index)
                }else if collect == self?.collect2 {
                    print("collect2")
                    self?.viewModel.recomended_videos.remove(at: index)
                }else if collect == self?.collect3 {
                    print("collect3")
                    self?.viewModel.top_singer_videos.remove(at: index)
                }else if collect == self?.collect4 {
                    print("collect4")
                    self?.viewModel.top_artist_videos.remove(at: index)
                }
                collect.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "Something went wrong with this video.")
        }
    }
    @IBAction func btnShare(_ sender:UIButton) {
        guard let collectionView = sender.accessibilityHint else {
            return
        }
        
        var dict : Popular_videos?
        
        if collectionView == "1" {
            dict = viewModel.trending_videos[sender.tag]
        }else if collectionView == "2" {
            dict = viewModel.recomended_videos[sender.tag]
        }else if collectionView == "3" {
            dict = viewModel.top_singer_videos[sender.tag]
        }else if collectionView == "4" {
            dict = viewModel.top_artist_videos[sender.tag]
        }
//        guard let videoURL = dict?.video_file, let v_id = dict?.video_id  else {
//            return
//        }
        guard let dict1 = dict else {
            return
        }
        let loading = LoadingView()
        loading.showActivityLoading(uiView: self.view,color: .black)
        SharePost.shared.generateContentLink1(video_name: dict1.song_name ?? "Karaoke", video_id: dict1.video_id ?? "9", video_url: dict1.video_file ?? "", video_image: dict1.video_thumb ?? "") { (url) in
            if url != nil {
                
                let activityVC = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                    loading.hideActivityLoading(uiView: self.view)
                    if !completed {
                        
                    }
                }
                self.present(activityVC, animated: true, completion: nil)
            }else{
                loading.hideActivityLoading(uiView: self.view)
            }
        }
    }
}
