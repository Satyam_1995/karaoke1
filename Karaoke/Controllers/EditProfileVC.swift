//
//  EditProfileVC.swift
//  Karaoke
//
//  Created by Ankur  on 19/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {
    
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var txt_bio: UITextView!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var txt_username:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    
    var imagePicker1: ImagePicker!
    var imageaAtt : imageArray?
    var pStatus:Int = 0
    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    var viewModel = EditProfileVM()
    var placeHolder = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        placeHolder = txt_bio.text!
        if pStatus == 0 {
            lbl_heading.text = "Create Profile"
            btn_Submit.setTitle("Submit", for: .normal)
            imgProfile.image = #imageLiteral(resourceName: "dummyProfile")
        }else{
            lbl_heading.text = "Edit Profile"
            btn_Submit.setTitle("Update", for: .normal)
            imgProfile.image = #imageLiteral(resourceName: "i15")
        }
        txt_bio.delegate = self
        imagePicker1 = ImagePicker(presentationController: self, delegate: self)
        self.setData(new: false)
        self.viewModel.vc = self
        self.viewModel.getData()
        self.txt_username.isEnabled = true
    }
    func setData(new:Bool) {
        var data = viewModel.Profiledetail?.data?.first
        if new == false {
            let sign = singletonClass.createsingleton()
            if let dat = sign?.userProfileData {
                data = dat.data?.first
            }
        }
        guard data != nil else {
            return
        }
        print(data as Any)

        txt_name.text = data?.name ?? ""
        txt_username.text = data!.user_name!
        txt_username.tag = 1
        txt_email.text = data?.user_email ?? ""
        txt_email.tag = 1
        if let bio = data?.bio, bio != "" {
            txt_bio.text = bio
        }
        
        if let url = data?.profile_img?.convert_to_url {
            imgProfile.af.setImage(withURL: url, completion:  { (result) in
                if let l = result.value {
                    let d = l.pngData()
                    self.imageaAtt = imageArray(image: l, data: d, url: "\(url)", image_id: nil)
                }
            })
        }
    }
    @IBAction func btnTerms(_ sender:UIButton) {
        let vc = TermsVC.instantiate(fromAppStoryboard: .Main)
        vc.status = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPrivacy(_ sender:UIButton) {
        let vc = TermsVC.instantiate(fromAppStoryboard: .Main)
        vc.status = 2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSubmit(_ sender:UIButton) {
        
        if pStatus == 0 {
//            self.navigationController?.popViewController(animated: true)
//            self.select?("s",1)
            self.updateProfile()
        }else{
            self.updateProfile()
//            UserDefaults.standard.set(1, forKey: "start_singing")
        }
    }
    func updateProfile() {
        guard self.validation() else {
            return
        }
        //user_id, name, user_name, mobile, email, password, bio, user_image
        var para : JSONDictionary = [:]
        para[ApiKey.user_id] = UserDetail.shared.getUserId()
        para[ApiKey.name] = self.txt_name.text!
        para[ApiKey.user_name] = txt_username.text!
        para[ApiKey.mobile] = ""
        para[ApiKey.email] = txt_email.text!
        para[ApiKey.bio] = txt_bio.text!
        
        var imgPara : [UploadFileParameter] = []
        if imageaAtt?.data != nil {
            let na = Date.getCurrentDateForName()
            let exte = ".png"
            imgPara.append(UploadFileParameter(fileName: na + exte, key: ApiKey.user_image, data: imageaAtt!.data!, mimeType: exte))
        }
        self.viewModel.updateProfileDat(para: para, imgPara: imgPara) { (re) in
            self.navigationController?.popViewController(animated: true)
            self.select?("s",0)
        }
    }
    @IBAction func btnChangePassword(_ sender:UIButton) {
        let data = viewModel.Profiledetail?.data?.first
        if (data?.social_type) != nil {
            self.AlertControllerOnr(title: alertTitle.alert_message, message: "You logged in with social media can not change password.")
        }else{
            let vc = ChangePassVC.instantiate(fromAppStoryboard: .Main)
            vc.hidesBottomBarWhenPushed = true
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDelete(_ sender:UIButton) {
//        let vc = LoginVC.instantiate(fromAppStoryboard: .Main)
//        vc.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(vc, animated: true)
        self.AlertControllerCuston(title: alertTitle.alert_warning, message: "Are you sure you want to delete your account", BtnTitle: ["Delete","Cancel"]) { (str) in
            if str != "Cancel" {
                WebServices.logout_delete(status: .Delete, success: { (_) in
                    self.delete()
                }, failure: { (_) -> (Void) in
                })
            }
        }
    }
    func delete() {
        UserDetail.shared.removeUserId()
        let vc = SignupVC.instantiate(fromAppStoryboard: .Main)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        let sing = singletonClass.createsingleton()
        sing?.userProfileData = nil
    }
    @IBAction func btn_takePhoto(_ sender:UIButton) {
        imagePicker1.present(from: sender)
    }
}

extension EditProfileVC : ImagePickerDelegate {
    func didSelect(image: UIImage?, tag: Int) {
        guard let img = image else {
            return
        }
        img.resizeByByte(maxMB: 1) { [self] (data) in
            imageaAtt = imageArray(image: img, data: data, url: nil, image_id: nil)
        }
        self.imgProfile.image = img
    }
}

extension EditProfileVC : UITextViewDelegate{
    @IBAction func editEmail(_ textField: UITextField) {
        if textField.text == "" {
           return
        }
        if textField == txt_username {
            self.viewModel.check_username()
        }
        if textField == txt_email {
//            self.check_email()
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_bio.text == placeHolder {
            txt_bio.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = placeHolder
        }
    }
    func validation() -> Bool {
        if self.imageaAtt?.data == nil  {
            CommonFunctions.showToastWithMessage(MessageString.addPhoto)
            return false
        }
        if self.txt_name.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.name)
            return false
        }
        if self.txt_username.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.username)
            return false
        }
        if self.txt_username.tag != 1  {
            CommonFunctions.showToastWithMessage(MessageString.username_already)
            return false
        }
        if self.txt_email.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.emailorMobile)
            return false
        }
        if !self.isValidEmail(testStr: self.txt_email.text!) {
            CommonFunctions.showToastWithMessage(MessageString.validEmail)
            return false
        }
        
        if self.txt_email.tag != 1  {
            CommonFunctions.showToastWithMessage(MessageString.Email_already_exists)
            return false
        }
        if txt_bio.text == placeHolder || txt_bio.text == "" {
            CommonFunctions.showToastWithMessage(MessageString.bio)
            return false
        }
        return true
    }
}
