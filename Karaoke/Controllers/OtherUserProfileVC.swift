//
//  OtherUserProfileVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import DropDown

class OtherUserProfileVC: UIViewController {
    
    @IBOutlet weak var collect:UICollectionView!
    
    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var lbl_userName:UILabel!
    @IBOutlet weak var lbl_description:UILabel!
    @IBOutlet weak var img_Profile:UIImageView!
    
    @IBOutlet weak var lbl_Following:UILabel!
    @IBOutlet weak var lbl_Followers:UILabel!
    @IBOutlet weak var btnFollow:UIButton!
    
    var friend_id : String?
    let addProAPI = OtherUserProfileVM()
    let user_id = UserDetail.shared.getUserId()
    //MARK: - DropDown's
    
    let menuDrop = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Setup()
        guard friend_id != nil else {
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "please send friend_id parameter")
            return
        }
        addProAPI.get_follower_profile(friend_id: friend_id!)
        addProAPI.get_user_videos(user_id: friend_id!)
    }
    
    @IBAction func btn_Menu(_ sender: UIButton){
        guard let dict = addProAPI.Profiledetail?.data else {
            return
        }
        
        if friend_id! == UserDetail.shared.getUserId() {
            self.AlertControllerOnr(title: alertTitle.alert_message, message: "it's your video")
            return
        }
        
        var list : [String] = []
        if dict.user_block_status == 1 {
            list = ["Unblock this Person","Report Abuse","Cancel"]
        }else{
            list = ["Block this Person","Report Abuse","Cancel"]
        }
        self.menuDrop.anchorView = sender
        self.menuDrop.bottomOffset = CGPoint(x: -140, y: sender.bounds.height + 10)
        self.menuDrop.textColor = .black
        self.menuDrop.separatorColor = .clear
        self.menuDrop.selectionBackgroundColor = .clear
        self.menuDrop.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.menuDrop.dataSource.removeAll()
        self.menuDrop.cellHeight = 35
        self.menuDrop.dataSource.append(contentsOf: list)
        self.menuDrop.selectionAction = { [unowned self] (index, item) in
            if index == 0 {
                addProAPI.block_unblock(friend_id: self.friend_id!)
//                self.AlertControllerOnr(title: nil, message: "Blocked User")
            }else if index == 1{
                let vc = ReportPopupVC.instantiate(fromAppStoryboard: .Home)
                vc.status = 1
                vc.followers_id = friend_id!
                self.present(vc, animated: true, completion: nil)
            }else{
            }
        }
        self.menuDrop.show()
    }
    
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFollow(_ sender:UIButton) {
        
        guard let dict = addProAPI.Profiledetail?.data else {
            return
        }
        guard let friend_id = dict.friend_id else {
            return
        }
        if friend_id == UserDetail.shared.getUserId() {
            self.AlertControllerOnr(title: alertTitle.alert_message, message: "it's your profile")
            return
        }
        
        var follow : follow_status = .follow
        if dict.user_follow_status == 0 {
            follow = .follow
            self.btnFollow.setTitle("Unfollow", for: .normal)
        }else{
            follow = .unFollow
            self.btnFollow.setTitle("Follow", for: .normal)
        }
        
        WebServices.follow_unfollow(friend_id: friend_id, follow_type: follow, loader: false) { (json) in
        } successData: { (data) in
            if dict.user_follow_status == 0 {
                self.addProAPI.Profiledetail?.data?.user_follow_status = 1
            }else{
                self.addProAPI.Profiledetail?.data?.user_follow_status = 0
            }
        } failure: { (err) -> (Void) in
            print(err)
            if dict.friend_follow_status != 0 {
                self.btnFollow.setTitle("Unfollow", for: .normal)
            }else{
                self.btnFollow.setTitle("Follow", for: .normal)
            }
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
        }
    }
    
    @IBAction func btn_Following(_ sender: UIButton){
        let vc = FollowerVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.status = 1
        vc.follower_id = friend_id!
        vc.fromVC = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Followers(_ sender: UIButton){
        let vc = FollowerVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.status = 0
        vc.follower_id = friend_id!
        vc.fromVC = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func Setup(){
        addProAPI.vc = self
        collect.delegate = self
        collect.dataSource = self
        self.collect.register(UINib(nibName: "VideoCell", bundle: nil), forCellWithReuseIdentifier: "VideoCell")
    }
    func setData(new:Bool) {
        
        guard let dict = addProAPI.Profiledetail?.data else {
            return
        }
        lbl_Following.text = "\(dict.total_following ?? 0)".formatPoints()
        lbl_Followers.text = "\(dict.total_follower ?? 0)".formatPoints()
        lbl_name.text = dict.name ?? ""
        lbl_userName.text = dict.user_name ?? ""
        lbl_description.text = dict.user_bio ?? ""
        if let url = dict.user_profile?.convert_to_url {
            img_Profile.af.setImage(withURL: url)
        }
        
        if dict.user_follow_status == 1 {
            self.btnFollow.tag = 0
            self.btnFollow.setTitle("Unfollow", for: .normal)
        }else{
            self.btnFollow.tag = 1
            self.btnFollow.setTitle("Follow", for: .normal)
        }
    }
    func updateVodeoList(new:Bool)  {
        self.collect.reloadData()
    }
    func updateBlockList(new:Bool)  {
        guard let dict = addProAPI.Profiledetail?.data else {
            return
        }
        if dict.user_block_status == 1 {
            addProAPI.Profiledetail?.data?.user_block_status = 0
            self.AlertControllerOnr(title: alertTitle.alert_message, message: "User unblock successfully.", BtnTitle: "OK")
        }else{
            addProAPI.Profiledetail?.data?.user_block_status = 1
            self.AlertControllerOnr(title: alertTitle.alert_message, message: "User blocked successfully.", BtnTitle: "OK")
        }
    }
}

extension OtherUserProfileVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addProAPI.userVideoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        let dict = addProAPI.userVideoList[indexPath.item]
        if let img = dict.video_thumb?.convert_to_url {
            cell.imgSong.af.setImage(withURL: img)
        }
        cell.lblLike.text = dict.total_likes ?? "0"
        cell.lblComment.text = dict.total_comments ?? "0"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = addProAPI.userVideoList[indexPath.item]
        let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        if let videoURL =  dict.video_file?.convert_to_string, let v_id = dict.video_id {
            vc.videoURL = videoURL
            vc.video_id = v_id
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "Something went wrong with this video.")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collect.frame.size.width/3 - 2.5
        let h = collect.frame.size.height/2.3
        return CGSize(width: w, height: h)
    }
}
