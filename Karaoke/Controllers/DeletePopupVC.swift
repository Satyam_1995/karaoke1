//
//  DeletePopupVC.swift
//  Karaoke
//
//  Created by Ankur  on 17/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class DeletePopupVC: UIViewController {
    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    var video_id : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        guard video_id != nil else {
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "send video_id")
            return
        }
    }
    
    
    @IBAction func btn_Delete(_ sender: UIButton){
        self.delete_video { (s) in
            if s {
                self.dismiss(animated: true, completion: {
                    self.select!("s",1)
                })
            }else{
                self.btn_Delete(sender)
            }
        }
    }
    
    @IBAction func btn_Close(_ sender: UIButton){
        self.dismiss(animated: true, completion: {
            self.select!("s",0)
        })
    }
    func delete_video(complition:@escaping(Bool)->Void)  {
//        Para:user_id,video_id
//        URL - http://karaoke.yilstaging.com/api/delete_video
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.video_id] = video_id!
        WebServices.commonPostAPI(parameters: parameters, endPoint: .delete_video, loader: true) { (json) in
            complition(true)
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
            complition(false)
        }
    }
    
}
