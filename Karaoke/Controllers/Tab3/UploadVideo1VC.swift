//
//  UploadVideo1VC.swift
//  Karaoke
//
//  Created by Ankur  on 30/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import DropDown

class UploadVideo1VC: UIViewController {
    @IBOutlet weak var txt_songname : UITextField!
    @IBOutlet weak var txt_artistname : UITextField!
    @IBOutlet weak var txt_musicCat : UITextField!
    @IBOutlet weak var txt_lyrics : UITextView!
    
    @IBOutlet weak var btnVerify : UIButton!
    @IBOutlet weak var imgPlaceholder : UIImageView!
    var imgPlaceholderImage : UIImage?
    var videpPicker : ImagePickerDhakad?
    var videoData : Data?
    var  video_thumb : Data?
    var music_category : [Music_category] = []
    let dropCatList = DropDown()
    var music_cat_id = ""
    var placeHolder = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        videpPicker = ImagePickerDhakad(presentationController: self, delegate: self)
        self.get_music_category()
        self.setUp()
        self.txt_lyrics.isHidden = true
        btnVerify.tag = 0
        imgPlaceholderImage = imgPlaceholder.image
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.viewControllers = [self]
//        showALert()
    }
    func setUp()  {
        
        self.placeHolder = txt_lyrics.text
        txt_lyrics.textColor = .gray
        txt_lyrics.delegate = self
        
        self.dropCatList.anchorView = txt_musicCat
        dropCatList.bottomOffset = CGPoint(x: 0, y:(dropCatList.anchorView?.plainView.bounds.height)!)

        self.dropCatList.textColor = .black
        self.dropCatList.separatorColor = .clear
        self.dropCatList.selectionBackgroundColor = .clear
        self.dropCatList.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.dropCatList.dataSource.removeAll()
        self.dropCatList.cellHeight = 35
        self.dropCatList.dataSource.append(contentsOf: [])
        self.dropCatList.selectionAction = { [unowned self] (index, item) in
            music_cat_id = self.music_category[index].music_category_id ?? ""
            self.txt_musicCat.text = item
        }
        
    }
    @IBAction func btnConfirm(_ sender: UIButton) {
        
        if sender.tag == 0 {
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
            btnVerify.tag = 1
        }else{
            sender.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            btnVerify.tag = 0
        }
    }
    @IBAction func getVideo(_ sender:UIButton) {
        videpPicker?.showVideoPicker(titleStr: "Video", optionIdentifier: 1)
    }
    @IBAction func btnSelectCat(_ sender: UIButton) {
        let s = music_category.map({$0.category_name!})
        self.dropCatList.anchorView = txt_musicCat
//        self.dropCatList.bottomOffset = CGPoint(x: -140, y: txt_musicCat.bounds.height + 10)
        dropCatList.bottomOffset = CGPoint(x: 0, y:(dropCatList.anchorView?.plainView.bounds.height)!)
        self.dropCatList.dataSource.removeAll()
        self.dropCatList.dataSource.append(contentsOf: s)
        self.dropCatList.show()
    }
    @IBAction func btnCancel(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.selectedIndex = 0
        NotificationCenter.default.post(name: Notification.Name("hideView"), object: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpoload(_ sender: UIButton) {
        guard (checkAppInstall(viewCon: self) != nil) else {
            return
        }
        
        uploadVideo()
//        self.navigationController?.popViewController(animated: true)
//        self.tabBarController?.selectedIndex = 0
//         self.tabBarController?.selectedIndex = 0
    }
    func uploadVideo() {
        guard videoData != nil, video_thumb != nil  else {
            CommonFunctions.showToastWithMessage(MessageString.video_select)
            return
        }
        if txt_songname.text == "" {
            CommonFunctions.showToastWithMessage(MessageString.songName)
            return
        }
        if txt_artistname.text == "" {
            CommonFunctions.showToastWithMessage(MessageString.artiseName)
            return
        }
        if music_cat_id == "" {
            CommonFunctions.showToastWithMessage(MessageString.music_categoriy)
            return
        }
        if btnVerify.tag != 0 {
            CommonFunctions.showToastWithMessage(MessageString.checkbox)
            return
        }
        
//        if txt_lyrics.text == "" || txt_lyrics.text == self.placeHolder {
//            CommonFunctions.showToastWithMessage(MessageString.music_categoriy)
//            return
//        }
        
//        http://karaoke.yilstaging.com/api/upload_video
//        user_id,video data, lyrics ,artist name, song name, video thumbnail
        let para : JSONDictionary = [ApiKey.user_id:UserDetail.shared.getUserId(),
                                     ApiKey.lyrics:"",
                                     ApiKey.song_name:txt_songname.text!,
                                     ApiKey.music_cat_id:music_cat_id,
                                     ApiKey.artist_name: txt_artistname.text!,
                                     ApiKey.old_video_id:""]
        
        var imgPara : [UploadFileParameter] = []
        let imgName = Date.getCurrentDateForName()
        imgPara.append(UploadFileParameter(fileName: imgName + ".mp4", key: ApiKey.video, data: videoData!, mimeType: ".mp4"))
        imgPara.append(UploadFileParameter(fileName: imgName + ".png", key: ApiKey.video_thumb, data: video_thumb!, mimeType: ".png"))
        self.showALert()
        WebServices.commonPostAPI_data(parameters: para, files: imgPara, endPoint: .upload_video, loader: false) { (json) in
            
        } successData: { (data) in
            self.alertShow.dismiss(animated: true) {
                self.tabBarController?.selectedIndex = 4
                self.txt_lyrics.text = self.placeHolder
                self.txt_songname.text = ""
                self.music_cat_id = ""
                self.txt_musicCat.text = ""
                self.txt_artistname.text = ""
                self.imgPlaceholder.image = self.imgPlaceholderImage
            }
        } progress: { (pro) in
            print(pro)
            self.lbl.text = "\(Int(pro * 100))"
            self.progressView.progress = Float(pro)
        } failure: { (error) -> (Void) in
            print(error)
            self.alertShow.dismiss(animated: true) {
                self.AlertControllerOnr(title: alertTitle.alert_error, message: error.localizedDescription)
            }
        }
    }
    func get_music_category() {
//        http://karaoke.yilstaging.com/api/get_music_category
        let para : JSONDictionary = [:]
        WebServices.commonPostAPI(parameters: para, endPoint: .get_music_category, loader: true) { (json) in
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(Music_category_all.self, from: data)
                if let cat = d.music_category {
                    
                    let cat2 = cat.sorted(by: { (Obj1, Obj2) -> Bool in
                           let Obj1_Name = Obj1.category_name ?? ""
                           let Obj2_Name = Obj2.category_name ?? ""
                           return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
                        })
                    self.music_category = cat2
                }
            }catch{
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    var alertShow = UIAlertController()
    let lbl = UILabel()
    var progressView = UIProgressView()
    func showALert() {
        alertShow = UIAlertController(title: nil, message: NSLocalizedString("Uploading...", comment: ""), preferredStyle: .alert)
        
        
        let s = UIAlertAction(title: "", style: .cancel, handler: nil)
        s.isEnabled = false
        alertShow.addAction(s)
                              
        self.present(alertShow, animated: true) {
            self.lbl.frame = CGRect(x: 0, y: 5, width: self.alertShow.view.frame.size.width, height: 100)
//            self.lbl.center.x = self.alertShow.view.center.x
//            self.lbl.center.y = self.alertShow.view.center.y
            self.lbl.text = "0"
            
            self.lbl.textAlignment = .center
            self.alertShow.view.addSubview(self.lbl)
//            Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (t) in
//
//            }
            let margin:CGFloat = 8.0
            let rect = CGRect(x: margin, y: 80, width: self.alertShow.view.frame.width - margin * 2.0 , height: 30)
            self.progressView = UIProgressView(frame: rect)
    //            self.progressView.translatesAutoresizingMaskIntoConstraints = false
            self.progressView.tintColor = #colorLiteral(red: 0.257733025, green: 0.2725596179, blue: 1, alpha: 1)
            self.alertShow.view.addSubview(self.progressView)
            self.progressView.progress = 0.0
        }
    }
}
extension UploadVideo1VC: imagePickerDelegate {
    func didSelect(image: UIImage?, tag: Int, data: Data?) {
    }
    func didSelectList(list: [imageArray]) {
    }
    
    func didVideo(image: UIImage?, tag: Int, data: Data?) {
        guard let img = image, let videodara = data else {
            return
        }
        videoData = videodara
        img.resizeByByte(maxMB: 0.25) { (d) in
            self.video_thumb = d
        }
        self.imgPlaceholder.image = img
    }
    func imagePicker_Error(tag:Int) {
        
    }
}
extension UploadVideo1VC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text == self.placeHolder {
            textView.text = ""
            textView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text == self.placeHolder {
            textView.text = placeHolder
            textView.textColor = .gray
        }
    }
}

extension UIViewController {
    func checkAppInstall(viewCon:UIViewController, showAlert : Bool = true, currentDate:Date = Date()) -> String? {
        guard let receivedData = KeyChain.load(key: "karaoke_first_time") else {
            return nil
        }
        let result = receivedData.to(type: Date.self)
        print("result: ", result)
        let modifiedDate = Calendar.current.date(byAdding: .day, value: 31, to: result)!
        if modifiedDate < currentDate {
            if showAlert == true {
                self.AlertControllerCuston(title: alertTitle.alert_message, message: "Please upgrade to a paid plan to upload video.", BtnTitle: ["Upgrade","Cancel"]) { (str) in
                    if str == "Upgrade" {
                        viewCon.tabBarController?.selectedIndex = 4
                    }
                }
            }
            return nil
        }
        return "true"
    }
    
}
