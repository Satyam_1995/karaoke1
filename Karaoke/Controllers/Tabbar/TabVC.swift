//
//  TabVC.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class TabVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    
    // MARK:- IBOutlet
    //==================
    
    
    @IBOutlet weak var btnView : UIView!
    @IBOutlet weak var btnTab1: UIButton!
    @IBOutlet weak var btnTab2: UIButton!
    @IBOutlet weak var btnTab3: UIButton!
    @IBOutlet weak var btnTab4: UIButton!
    @IBOutlet weak var btnTab5: UIButton!
    @IBOutlet weak var imagTab1: UIImageView!
    @IBOutlet weak var imagTab2: UIImageView!
    @IBOutlet weak var imagTab3: UIImageView!
    @IBOutlet weak var imagTab4: UIImageView!
    @IBOutlet weak var imagTab5: UIImageView!
    
    
    
    // MARK:- Variable
    //==================
    
    var tab1VC:HomeVC! = nil
    var tab2VC:NotificationVC! = nil
    var tab3VC:SongBookPageVC! = nil
    var tab4VC:ExploreVC! = nil
    var tab5VC:AddProfileVC! = nil
    
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    var SelectedCurrentPage: Int = -1
    
    // MARK:- Life Cycle
    //====================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        currentPage = 0
        btnTab1.tag = 1
        btnTab2.tag = 2
        btnTab3.tag = 3
        btnTab4.tag = 4
        btnTab5.tag = 5
        
        createPageViewController()
        
        //        NotificationCenter.default.addObserver(forName: NSNotification.Name(NotificationProfileBtn.Profile), object: nil, queue: nil) { (Noti) in
        //                 self.btnOptionClicked(btn: self.btnTab2)
        //             }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        NSLog("fffff")
        //        print("Current page:",SelectedCurrentPage)
        //        if SelectedCurrentPage > -1 {
        self.resetTabBarForTag(tag: currentPage)
        //            SelectedCurrentPage = -1
        //        }else if SelectedCurrentPage == -3 {
        //            tabBarController?.selectedIndex = 3
        //        }else if SelectedCurrentPage == -2 {
        //            tabBarController?.selectedIndex = 2
        //        }
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    
    
    //MARK: - IBaction Methods
    //====================
    @IBAction private func btnOptionClicked(btn: UIButton) {
        
        pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewController.NavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        
        resetTabBarForTag(tag: btn.tag-1)
    }
    
    
    //MARK: - CreatePagination
    //====================
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let poi = self.btnTab1.frame.size.height
            //            let frame = self.view.convert(self.view.frame, from:self.btnTab1)
            let frame = self.view.convert(self.btnTab1.frame, from:self.btnView)
            
            let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
            print(topBarHeight)
            //            CGPoint point = [subview1 convertPoint:subview2.frame.origin toView:self.view];
            
            
            //            self.pageController.view.frame = CGRect(x: 0, y: poi + frame.origin.y + 16 + 0 , width: self.view.frame.size.width, height: self.view.frame.size.height  - poi - frame.origin.y - 16 - 0)
            //            self.pageController.view.frame = CGRect(x: 0, y: 0 , width: self.view.frame.size.width, height: self.view.frame.size.height  - 120)
            //            self.pageController.view.frame = CGRect(x: 0, y: 0 , width: self.view.frame.size.width, height: self.view.frame.size.height  - 70)
            
            let hh = self.btnView.frame.size.height
            
            if UIDevice.current.hasNotch {
                let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
                
                self.pageController.view.frame = CGRect(x: 0, y: 0 , width: self.view.frame.size.width, height: self.view.frame.size.height  - (bottom! + hh + (hh * 0.04)))
            }else{
                
                self.pageController.view.frame = CGRect(x: 0, y: 0 , width: self.view.frame.size.width, height: self.view.frame.size.height  - (hh + (hh * 0.04)))
            }
        }
        
        let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
        //        tab1VC = homeStoryboard.instantiateViewController(withIdentifier: "Tab1VC") as? Tab1VC
        tab1VC = homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
        tab2VC = homeStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
        tab3VC = homeStoryboard.instantiateViewController(withIdentifier: "SongBookPageVC") as? SongBookPageVC
        tab4VC = homeStoryboard.instantiateViewController(withIdentifier: "ExploreVC") as? ExploreVC
        tab5VC = homeStoryboard.instantiateViewController(withIdentifier: "AddProfileVC") as? AddProfileVC
        
        arrVC = [tab1VC, tab2VC, tab3VC, tab4VC, tab5VC]
        
        pageController.setViewControllers([tab1VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        
        self.addChild(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParent: self)
    }
    
    
    private func selectedButton(btn: UIButton) {
        
        btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btn.backgroundColor = .clear
        imagTab1.image = #imageLiteral(resourceName: "Home")
        imagTab2.image = #imageLiteral(resourceName: "Notification")
        imagTab3.image = #imageLiteral(resourceName: "plusR")
        imagTab4.image = #imageLiteral(resourceName: "Explore")
        imagTab5.image = #imageLiteral(resourceName: "pro3")
        
        if btn.tag == 1 {
            imagTab1.image = #imageLiteral(resourceName: "HomeR")
            
        }
        if btn.tag == 2 {
            imagTab2.image = #imageLiteral(resourceName: "NotificationR")
            
        }
        if btn.tag == 3 {
            imagTab3.image = #imageLiteral(resourceName: "plusR")
            
        }
        if btn.tag == 4 {
            imagTab4.image = #imageLiteral(resourceName: "ExploreR")
            
        }
        if btn.tag == 5 {
            imagTab5.image = #imageLiteral(resourceName: "ProfileR")
            
        }
        
        
        
        
        //        constantViewLeft.constant = btn.frame.origin.x
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        //       let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)!
        //            + UIApplication.shared.statusBarFrame.height
        //        print(navBarHeight)
    }
    
    private func unSelectedButton(btn: UIButton) {
        //        btn.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btn.backgroundColor = .clear
        
    }
    
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    //====================
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    
    private func resetTabBarForTag(tag: Int) {
        
        var sender: UIButton!
        var selectedView : UIView!
        if(tag == 0) {
            sender = btnTab1
            imagTab1.image = #imageLiteral(resourceName: "HomeR")
            
        }
        else if(tag == 1) {
            sender = btnTab2
            imagTab2.image = #imageLiteral(resourceName: "NotificationR")
        }
        else if(tag == 2) {
            sender = btnTab3
            imagTab3.image = #imageLiteral(resourceName: "plusR")
            
        }
        else if(tag == 3) {
            sender = btnTab4
            imagTab4.image = #imageLiteral(resourceName: "ExploreR")
        }
            
        else if(tag == 4) {
            sender = btnTab4
            imagTab5.image = #imageLiteral(resourceName: "ProfileR")
        }
        
        
        currentPage = tag
        
        unSelectedButton(btn: btnTab1)
        unSelectedButton(btn: btnTab2)
        unSelectedButton(btn: btnTab3)
        unSelectedButton(btn: btnTab4)
        unSelectedButton(btn: btnTab5)
        
        
        
        selectedButton(btn: sender)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //        let xFromCenter: CGFloat = self.view.frame.size.width-scrollView.contentOffset.x
        //        let xCoor: CGFloat = CGFloat(viewLine.frame.size.width) * CGFloat(currentPage)
        //        let xPosition: CGFloat = xCoor - xFromCenter/CGFloat(arrVC.count)
        //        constantViewLeft.constant = xPosition
    }
    
}
