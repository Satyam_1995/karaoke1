//
//  LikeFeed.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 04/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDynamicLinks


class SharePost: NSObject {
    static var shared = SharePost()
    private var uniqueConversationName = "s"
    
    func generateContentLink1(video_name:String, video_id:String,video_url:String,video_image:String, complitation : @escaping(URL?) -> Void) {
        let baseURL = URL(string: "https://karaoke2.page.link")!
        
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "www.example.com"
        components.path = "/video"
        
        let resipeIdWueryItem = URLQueryItem(name: "video_id", value: video_id)
        let resipeIdWueryItem2 = URLQueryItem(name: "video_url", value: video_url)
        
        components.queryItems = [resipeIdWueryItem,resipeIdWueryItem2]
        
        guard let linkpara = components.url else {
            complitation(nil)
            return
        }
        print("i am sharing" + linkpara.absoluteString)
        let domain = "https://karaoke2.page.link"
        let linkBuilder = DynamicLinkComponents.init(link: linkpara, domainURIPrefix: domain)
        
//        let linkBuilder = DynamicLinkComponents(link: baseURL, domainURIPrefix: domain)
//        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "Yesitlabs.Karaoke")
        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.voicetm.karaoke")
        
        linkBuilder?.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder?.socialMetaTagParameters?.title = video_name
        linkBuilder?.socialMetaTagParameters?.descriptionText = video_name
        linkBuilder?.socialMetaTagParameters?.imageURL = URL(string: video_image)
        guard let longURL = linkBuilder?.url else{
            complitation(nil)
            return
        }
        print(longURL.absoluteString)
        linkBuilder?.shorten(completion: { (url, warriong, error) in
            if let err = error {
                print(err)
                complitation(nil)
                return
            }
            if let warrio = warriong {
                for item in warrio {
                    print(item)
                }
            }
            print(url as Any)
            complitation(url)
        })
//        return linkBuilder?.link ?? baseURL
    }
    func generateContentLink(video_id:String) -> URL {
        let baseURL = URL(string: "https://karaoke2.page.link/mVFa")!
        //        let baseURL = URL(string: "https://\(video_id)")!
        let domain = "https://karaoke2.page.link"
        //        let domain = "https://your-app.page.link"
        
        let linkBuilder = DynamicLinkComponents(link: baseURL, domainURIPrefix: domain)
//        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "Yesitlabs.Karaoke")
        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.voicetm.karaoke")
        //        linkBuilder.iOSParameters.appStoreID = "123456789"
        //        linkBuilder.iOSParameters.minimumAppVersion = "1.2.3"
        
        //        linkBuilder.androidParameters =
        //            DynamicLinkAndroidParameters(packageName: "com.your.packageName")
        //        linkBuilder.androidParameters.minimumVersion = 123
        
        return linkBuilder?.link ?? baseURL
    }
}
