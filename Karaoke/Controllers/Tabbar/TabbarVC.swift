//
//  TabbarVC.swift
//  Karaoke
//
//  Created by Ankur  on 11/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController {
    
    let width = UIScreen.main.bounds.size.width
    let hight = UIScreen.main.bounds.size.height
    
    var notificationData : NotificationModl?
    var notification_list1 : [Notification_list] = []
    var notification_list : [Notification_list] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getsoundList()
        tabBar.backgroundImage = UIImage()
        changeNotificationImage(notifiShow:true)
        
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (tim) in
            self.get_notification_listNew(last_id: "0")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UIDevice.current.hasNotch {
//            tabBar.frame = CGRect(x: 10, y: tabBar.frame.origin.y - 30, width: tabBar.frame.size.width, height: 80.0)
        }
    }
    override func viewWillLayoutSubviews() {
        
        func chcekingForIhophon8(str:String) -> Bool {
            if str.contains("8") {
                print("hai")
                return true
            }else {
                print("Ni hai")
               return false
            }
        }
        if chcekingForIhophon8(str: UIDevice.current.name) {
                        //        tabBar.frame = CGRect(x: 10, y: hight - tabBar.frame.size.height - 30, width: tabBar.frame.size.width, height: tabBar.frame.size.height * 1.33)
                        tabBar.frame = CGRect(x: 10, y: tabBar.frame.origin.y - 40, width: tabBar.frame.size.width, height: 80.0)
                        //        tabBar.cornerRadius = Double(tabBar.frame.size.height/2)
                        tabBar.backgroundColor = .clear
                        tabBar.clipsToBounds = true
            
                        let Container = UIView()
                        //        Container.backgroundColor = .clear
                        Container.alpha = 0.7
                        //        Container.frame = CGRect(x: 0, y: -10, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
                        Container.frame = CGRect(x: 0, y: 15, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
                        Container.clipsToBounds = true
                        self.tabBar.addSubview(Container)
                        self.tabBar.sendSubviewToBack(Container)
            
                        let first = UIView()
                        //            first.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.3215686275, alpha: 1)
                        first.backgroundColor = #colorLiteral(red: 0.08941803128, green: 0.08959422261, blue: 0.3016236722, alpha: 1)
                        first.alpha = 0.7
                        //        first.frame = CGRect(x: 0, y: 10, width: Container.frame.size.width - 20, height: Container.frame.size.height - 20)
                        first.frame = CGRect(x: 10, y: 16, width: Container.frame.size.width - 20, height: Container.frame.size.height - 25)
            
                        first.layer.cornerRadius = (Container.frame.size.height - 30)/2
                        first.clipsToBounds = true
                        Container.addSubview(first)
        } else if UIDevice.current.hasNotch {
            let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
            print(bottom!)
            
            tabBar.frame = CGRect(x: 10, y: tabBar.frame.origin.y - 30 , width: tabBar.frame.size.width, height: 80)
//            if UIDevice.current.hasNotch {
//                let s2s = UIViewController().iScreenSizes()
//                tabBar.frame = CGRect(x: 10, y: s2s - 30, width: tabBar.frame.size.width, height: 80.0)
//            }
            //        tabBar.cornerRadius = Double(tabBar.frame.size.height/2)
            tabBar.backgroundColor = .clear
            tabBar.clipsToBounds = true
            
            let Container = UIView()
            //        Container.backgroundColor = .clear
            Container.alpha = 0.7
            //        Container.frame = CGRect(x: 0, y: -10, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
            Container.frame = CGRect(x: 0, y: 16, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
            Container.clipsToBounds = true
//            Container.center = self.tabBar.center
            self.tabBar.addSubview(Container)
            self.tabBar.sendSubviewToBack(Container)
            
                let first = UIView()
                //            first.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.3215686275, alpha: 1)
                first.backgroundColor = #colorLiteral(red: 0.08941803128, green: 0.08959422261, blue: 0.3016236722, alpha: 1)
                first.alpha = 0.7
                //        first.frame = CGRect(x: 0, y: 10, width: Container.frame.size.width - 20, height: Container.frame.size.height - 20)
                first.frame = CGRect(x: 10, y: 5, width: Container.frame.size.width - 20, height: Container.frame.size.height - 20)
                
                first.layer.cornerRadius = (Container.frame.size.height - 30)/2
                first.clipsToBounds = true
                Container.addSubview(first)
        } else if  UIDevice.current.name == "iPhone 6"{
            
            //        tabBar.frame = CGRect(x: 10, y: hight - tabBar.frame.size.height - 30, width: tabBar.frame.size.width, height: tabBar.frame.size.height * 1.33)
            tabBar.frame = CGRect(x: 10, y: tabBar.frame.origin.y - 40, width: tabBar.frame.size.width, height: 80.0)
            //        tabBar.cornerRadius = Double(tabBar.frame.size.height/2)
            tabBar.backgroundColor = .clear
            tabBar.clipsToBounds = true
            
            let Container = UIView()
            //        Container.backgroundColor = .clear
            Container.alpha = 0.7
            //        Container.frame = CGRect(x: 0, y: -10, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
            Container.frame = CGRect(x: 0, y: 15, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
            Container.clipsToBounds = true
            self.tabBar.addSubview(Container)
            self.tabBar.sendSubviewToBack(Container)
            
            let first = UIView()
            //            first.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.3215686275, alpha: 1)
            first.backgroundColor = #colorLiteral(red: 0.08941803128, green: 0.08959422261, blue: 0.3016236722, alpha: 1)
            first.alpha = 0.7
            //        first.frame = CGRect(x: 0, y: 10, width: Container.frame.size.width - 20, height: Container.frame.size.height - 20)
            first.frame = CGRect(x: 0, y: 0, width: Container.frame.size.width - 20, height: Container.frame.size.height - 30)
            
            first.layer.cornerRadius = (Container.frame.size.height - 30)/2
            first.clipsToBounds = true
            Container.addSubview(first)
        }else {
            tabBar.frame = CGRect(x: 10, y: tabBar.frame.origin.y - 40, width: tabBar.frame.size.width, height: 80.0)
            //        tabBar.cornerRadius = Double(tabBar.frame.size.height/2)
            tabBar.backgroundColor = .clear
            tabBar.clipsToBounds = true
            
            let Container = UIView()
            //        Container.backgroundColor = .clear
            Container.alpha = 0.7
            //        Container.frame = CGRect(x: 0, y: -10, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
            Container.frame = CGRect(x: 0, y: 15, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
            Container.clipsToBounds = true
            self.tabBar.addSubview(Container)
            self.tabBar.sendSubviewToBack(Container)
            
            let first = UIView()
            //            first.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.3215686275, alpha: 1)
            first.backgroundColor = #colorLiteral(red: 0.08941803128, green: 0.08959422261, blue: 0.3016236722, alpha: 1)
            first.alpha = 0.7
            //        first.frame = CGRect(x: 0, y: 10, width: Container.frame.size.width - 20, height: Container.frame.size.height - 20)
            first.frame = CGRect(x: 0, y: 0, width: Container.frame.size.width - 20, height: Container.frame.size.height - 30)
            
            first.layer.cornerRadius = (Container.frame.size.height - 30)/2
            first.clipsToBounds = true
            Container.addSubview(first)
//            print(UIDevice.current.name)
//            //        tabBar.frame = CGRect(x: 10, y: hight - tabBar.frame.size.height - 30, width: tabBar.frame.size.width, height: tabBar.frame.size.height * 1.33)
//            tabBar.frame = CGRect(x: 10, y: tabBar.frame.origin.y - 40, width: tabBar.frame.size.width, height: 80.0)
//            //        tabBar.cornerRadius = Double(tabBar.frame.size.height/2)
//            tabBar.backgroundColor = .clear
//            tabBar.clipsToBounds = true
//
//            let Container = UIView()
//            //        Container.backgroundColor = .clear
//            Container.alpha = 0.7
//            //        Container.frame = CGRect(x: 0, y: -10, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
//            Container.frame = CGRect(x: 0, y: 15, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
//            Container.clipsToBounds = true
//            self.tabBar.addSubview(Container)
//            self.tabBar.sendSubviewToBack(Container)
//
//            let first = UIView()
//            //            first.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.3215686275, alpha: 1)
//            first.backgroundColor = #colorLiteral(red: 0.08941803128, green: 0.08959422261, blue: 0.3016236722, alpha: 1)
//            first.alpha = 0.7
//            //        first.frame = CGRect(x: 0, y: 10, width: Container.frame.size.width - 20, height: Container.frame.size.height - 20)
//            first.frame = CGRect(x: 10, y: 16, width: Container.frame.size.width - 20, height: Container.frame.size.height - 25)
//
//            first.layer.cornerRadius = (Container.frame.size.height - 30)/2
//            first.clipsToBounds = true
//            Container.addSubview(first)
        }
    }
    func changeNotificationImage(notifiShow:Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("newNotificaton"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name("newNotificaton"), object: nil, queue: .main) { (noti) in
            if let notifi = noti.object as? Bool, notifi == true {
                self.tabBar.items?[1].image = #imageLiteral(resourceName: "Notification_2")
//                self.tabBar.items?[1].selectedImage = #imageLiteral(resourceName: "NotificationR_2")
                
            }else{
                self.tabBar.items?[1].image = #imageLiteral(resourceName: "Notification")
//                self.tabBar.items?[1].selectedImage = #imageLiteral(resourceName: "NotificationR")
            }
        }
    }
    func getsoundList() {
        //http://karaoke.yilstaging.com/api/get_sound_effects
        let para = [ApiKey.user_id:UserDetail.shared.getUserId()]
        WebServices.commonPostAPIModel(model: SoundEffect.self, parameters: para, endPoint: .get_sound_effects, loader: false) { (json) in

        } returnModel: { (model, error) in
            guard let model = model else{
                return
            }
            let sound_list  = model.sound_list ?? []


            for item in sound_list {
                if self.read_url(fromDocumentsWithFileName: item.sound_id! + ".mp3") != nil {
//                    print(item.sound_id! + ".mp3")
                }else{
                    AppNetworking.DOWNLOAD(endPoint: item.effect_url!, fileName: item.sound_id!, parameters: [:], mediaType: ".mp3", loader: false) { (status) in
                    } successData: { (url) in
                    } failure: { (error) in
                        print(error)
                    }
                }
            }
        } successData: { (data) in
        } failure: { (error) -> (Void) in
        }
    }
    func read_url(fromDocumentsWithFileName fileName: String) -> URL? {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(fileName) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                return pathComponent
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    func get_notification_listNew(last_id:String,refresh: Bool = false)  {
//        Para:user_id,last_id
//        URL - http://karaoke.yilstaging.com/api/get_notification_list
//        let loader = ProfileResponse == nil ? true : false
        let user = UserDetail.shared.getUserId()
        if user == "" {
            return
        }
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.last_id] = "0"
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_notification_list, loader: false) { (json) in
        } successData: { (data) in
            do {
                self.notificationData = try JSONDecoder().decode(NotificationModl.self, from: data)
                if self.notificationData?.status == "success" {
                    if let s = self.notificationData?.notification_list {
                        var tamp : [Notification_list] = []
                        if self.notification_list.count == 0 {
                            self.notification_list.append(contentsOf: s)
                            return
                        }
                        
                        for item in s {
                            if !self.notification_list.contains(where: {$0.notification_id == item.notification_id}) {
                                tamp.append(item)
                            }
                        }
                        tamp = tamp.reversed()
                        for item in tamp {
                            self.notification_list.insert(item, at: 0)
                        }
                        if tamp.count > 0 {
//                            CommonFunctions.showToastWithMessage("\(tamp.count) new notification", displayTime: 2, bottomSpace: 50) {
//                            }
                            NotificationCenter.default.post(name: NSNotification.Name("newNotificaton"), object: true)
                        }
                    }
                }else{
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}



extension UIViewController {
    func removeFromNavigationController() { navigationController?.removeController(.last) { self == $0 } }
}

extension UINavigationController {
    enum ViewControllerPosition { case first, last }
    enum ViewControllersGroupPosition { case first, last, all }

    func removeController(_ position: ViewControllerPosition, animated: Bool = true,
                          where closure: (UIViewController) -> Bool) {
        var index: Int?
        switch position {
            case .first: index = viewControllers.firstIndex(where: closure)
            case .last: index = viewControllers.lastIndex(where: closure)
        }
        if let index = index { removeControllers(animated: animated, in: Range(index...index)) }
    }

    func removeControllers(_ position: ViewControllersGroupPosition, animated: Bool = true,
                           where closure: (UIViewController) -> Bool) {
        var range: Range<Int>?
        switch position {
            case .first: range = viewControllers.firstRange(where: closure)
            case .last:
                guard let _range = viewControllers.reversed().firstRange(where: closure) else { return }
                let count = viewControllers.count - 1
                range = .init(uncheckedBounds: (lower: count - _range.min()!, upper: count - _range.max()!))
            case .all:
                let viewControllers = self.viewControllers.filter { !closure($0) }
                setViewControllers(viewControllers, animated: animated)
                return
        }
        if let range = range { removeControllers(animated: animated, in: range) }
    }

    func removeControllers(animated: Bool = true, in range: Range<Int>) {
        var viewControllers = self.viewControllers
        viewControllers.removeSubrange(range)
        setViewControllers(viewControllers, animated: animated)
    }

    func removeControllers(animated: Bool = true, in range: ClosedRange<Int>) {
        removeControllers(animated: animated, in: Range(range))
    }
}

private extension Array {
    func firstRange(where closure: (Element) -> Bool) -> Range<Int>? {
        guard var index = firstIndex(where: closure) else { return nil }
        var indexes = [Int]()
        while index < count && closure(self[index]) {
            indexes.append(index)
            index += 1
        }
        if indexes.isEmpty { return nil }
        return Range<Int>(indexes.min()!...indexes.max()!)
    }
}
