//
//  TapViewController.swift
//  Pubhub
//
//  Created by Raurnet solutions on 03/01/19.
//  Copyright © 2019 yesitlabs. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    let width = UIScreen.main.bounds.size.width
    let heigt = UIScreen.main.bounds.size.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.pushViewController(self.tabbarFunc(), animated: false)
        self.navigationController?.isNavigationBarHidden = true
        let bgView: UIImageView = UIImageView(image: UIImage(named: "base.png"))
              bgView.frame = CGRect(x: 0, y: heigt - 80, width: width, height: 80)
           bgView.frame = bgView.bounds
        tabBarController?.tabBar.bringSubviewToFront(bgView)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @objc func pushToNextVC() {
        let newVC = UIViewController()
        newVC.view.backgroundColor = UIColor.red
        self.navigationController?.pushViewController(newVC, animated:
            true)
    }
    func tabbarFunc() -> UITabBarController {
        
        let firstVC = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
        var firstNavigation: UINavigationController? = nil
        if let aVC = firstVC {
            firstNavigation = UINavigationController(rootViewController: aVC)
        }
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
        var secondNavigation: UINavigationController? = nil
        if let aVC = secondVC {
            secondNavigation = UINavigationController(rootViewController: aVC)
        }
        
        let thirdVC = storyboard?.instantiateViewController(withIdentifier: "SongBookPageVC") as? SongBookPageVC
        var thirdNavigation: UINavigationController? = nil
        if let aVC = thirdVC {
            thirdNavigation = UINavigationController(rootViewController: aVC)
        }
        
        let FourthVC = storyboard?.instantiateViewController(withIdentifier: "ExploreVC") as? ExploreVC
        var FourthNavigation: UINavigationController? = nil
        if let aVC = FourthVC {
            FourthNavigation = UINavigationController(rootViewController: aVC)
        }
        
        let FifthVC = storyboard?.instantiateViewController(withIdentifier: "AddProfileVC") as? AddProfileVC
               var FifthNavigation: UINavigationController? = nil
               if let aVC = FifthVC {
                   FifthNavigation = UINavigationController(rootViewController: aVC)
               }
        
        let tabBarCon = UITabBarController()
        tabBarCon.viewControllers = [firstNavigation,secondNavigation, thirdNavigation,FourthNavigation,FifthNavigation] as? [UIViewController]
        let tabBarItemsAll: UITabBar = tabBarCon.tabBar

//        tabBarCon.tabBar.barTintColor = setCustomColor.shared.p_ChangeColor()
        tabBarCon.tabBar.barTintColor = #colorLiteral(red: 0.3821204901, green: 0.3969963193, blue: 0.6857737303, alpha: 1)
        tabBarCon.tabBar.isTranslucent = false

        
        tabBarItemsAll.unselectedItemTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        tabBarCon.tabBar.tintColor = .clear
        
      

        let firstItem = tabBarItemsAll.items![0]

        firstItem.image = #imageLiteral(resourceName: "Home")
        firstItem.selectedImage = #imageLiteral(resourceName: "HomeR")
        firstItem.imageInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: -5, right: 0)
        
        let secondItem = tabBarItemsAll.items![1]

        secondItem.image = #imageLiteral(resourceName: "Notification_2")
        secondItem.selectedImage = #imageLiteral(resourceName: "NotificationR_2")
        secondItem.imageInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: -5, right: 0)
               
        let thirdItem = tabBarItemsAll.items![2]

        thirdItem.image = #imageLiteral(resourceName: "plusR")
        thirdItem.selectedImage = #imageLiteral(resourceName: "plusR")
        thirdItem.imageInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: -5, right: 0)
        
        let FourhItem = tabBarItemsAll.items![3]

        FourhItem.image = #imageLiteral(resourceName: "Explore")
        FourhItem.selectedImage = #imageLiteral(resourceName: "ExploreR")
        FourhItem.imageInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: -5, right: 0)
        
        let FifthItem = tabBarItemsAll.items![4]

        FifthItem.image = #imageLiteral(resourceName: "Profile")
        FifthItem.selectedImage = #imageLiteral(resourceName: "ProfileR")
        FifthItem.imageInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: -5, right: 0)
        
        tabBarCon.selectedIndex = 0
        return tabBarCon
    }

}
@objc class VideoPlayKVO: NSObject {
    @objc dynamic var playingCell : HomeDataCell? = nil
    @objc dynamic var cell : UICollectionViewCell? = nil
}
