//
//  SongsBookCell.swift
//  Karaoke
//
//  Created by Ankur  on 09/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class SongsBookCell: UITableViewCell {

    @IBOutlet weak var btn_View:UIButton!
    @IBOutlet weak var btn_Sing:UIButton!
    @IBOutlet weak var btn_share:UIButton!

    @IBOutlet weak var imgSong:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblArtest:UILabel!
    @IBOutlet weak var lblLike:UILabel!
    @IBOutlet weak var lblUser:UILabel!
    @IBOutlet weak var imgLike:UIImageView!
        
    var viewCon : UIViewController?
    var updateData : Popular_videos? {
        didSet {
            self.update()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update()  {
        guard let dict = updateData else {
            return
        }
        self.lblTitle.text = dict.song_name ?? ""
        self.lblArtest.text = dict.artist_name ?? ""
        self.lblLike.text = dict.total_likes?.formatPoints()
        self.lblUser.text = dict.user_name
        self.lblTitle.text = dict.song_name ?? ""
        self.imgLike.image = dict.user_like_status == 1 ? #imageLiteral(resourceName: "heartR") : #imageLiteral(resourceName: "heartGray")
//        self.imgSong.imageURL = dict.video_thumb ?? ""
        if let img = dict.video_thumb?.convert_to_url {
            self.imgSong.af.setImage(withURL: img)
        }
    }
    @IBAction func btnShare(_ sender: UIButton) {
        guard let dict = updateData else {
            return
        }
        SharePost.shared.generateContentLink1(video_name: dict.song_name ?? "Karaoke", video_id: dict.video_id ?? "9", video_url: dict.video_file ?? "", video_image: dict.video_thumb ?? "") { (url) in
            if url != nil {
                let activityVC = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.viewCon?.view ?? self
                self.viewCon?.present(activityVC, animated: true, completion: nil)
            }
        }
    }
}
