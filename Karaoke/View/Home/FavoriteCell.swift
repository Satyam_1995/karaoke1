//
//  FavoriteCell.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class FavoriteCell: UICollectionViewCell {
    
    @IBOutlet weak var btnComment:UIButton!
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var btnFavorite:UIButton!
    @IBOutlet weak var lblComment:UILabel!
    @IBOutlet weak var lblLike:UILabel!
    @IBOutlet weak var imgLike:UIImageView!
    @IBOutlet weak var imgFavorite:UIImageView!
    @IBOutlet weak var lblFavorite:UILabel!
    
    @IBOutlet weak var imgSong:UIImageView!
    
    var cellData : Video_list? {
        didSet {
            updateDat()
        }
    }
    func updateDat()  {
        guard let dict = cellData else {
            return
        }
        if dict.user_like_status != 1 {
            self.imgLike.image = #imageLiteral(resourceName: "heartW")
        }else{
            self.imgLike.image = #imageLiteral(resourceName: "heartR")
        }
        if dict.user_fav_status != 1 {
            self.imgFavorite.image = #imageLiteral(resourceName: "fav")
        }else{
            self.imgFavorite.image = #imageLiteral(resourceName: "favR")
        }
        if let url = dict.video_thumb?.convert_to_url {
            self.imgSong.af.setImage(withURL: url)
        }
        self.updateLike = dict.total_likes ?? "0"
        self.updateComment = dict.total_comments ?? "0"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var updateLike = "0" {
        didSet {
            lblLike.text = updateLike.formatPoints()
        }
    }
    var updateComment = "0" {
        didSet {
            lblComment.text = updateComment.formatPoints()
        }
    }
    func AddLike() {
        guard var newl = Int(updateLike) else {
            return
        }
        newl = newl + 1
        updateLike = "\(newl)"
        lblLike.text = updateLike.formatPoints()
    }
    func removeLike() {
        guard var newl = Int(updateLike) else {
            return
        }
        newl = newl - 1
        updateLike = "\(newl)"
        lblLike.text = updateLike.formatPoints()
    }
}
