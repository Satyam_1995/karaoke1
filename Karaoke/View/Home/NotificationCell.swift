//
//  NotificationCell.swift
//  Karaoke
//
//  Created by Ankur  on 09/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var btnFollow:UIButton!
    @IBOutlet weak var btnLater:UIButton!
    @IBOutlet weak var lblComment:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var img_user:UIImageView!
    @IBOutlet weak var img_video:UIImageView!
    @IBOutlet weak var img_video1:UIImageView!
    @IBOutlet weak var btn_View:UIView!
    @IBOutlet weak var heightConstraints: NSLayoutConstraint!
     let identifier = "NotificationCell"

    var likeData : Notification_list? {
        didSet {
            self.cellLike()
        }
    }
    var commentData : Notification_list? {
        didSet {
            self.cellCommen()
        }
    }
    var followData : Notification_list? {
        didSet {
            self.cellFollow()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func cellLike() {
        guard let dict = likeData else {
            return
        }
        if let url = dict.user_profile?.convert_to_url {
            self.img_user.af.setImage(withURL: url)
        }
        self.img_video.isHidden = false
        self.img_video1.isHidden = false
        self.btn_View.isHidden = true
        self.heightConstraints.constant = 0
        
        let fname = dict.user_likes.last?.name ?? ""
        let txt1 = UIViewController().customizeFont(string: "\(fname) ", font: .boldSystemFont(ofSize: 12), color: .black)
        if dict.user_likes.count > 2 {
            let txt2 = UIViewController().customizeFont(string: "and ", font: .systemFont(ofSize: 12), color: UIColor.red)
            let txt3 = UIViewController().customizeFont(string: "\(dict.user_likes.count - 1) Others ", font: .boldSystemFont(ofSize: 12), color: .black)
            txt1.append(txt2)
            txt1.append(txt3)
        }else if dict.user_likes.count == 2 {
            let txt2 = UIViewController().customizeFont(string: "and ", font: .systemFont(ofSize: 12), color: UIColor.red)
            let txt3 = UIViewController().customizeFont(string: dict.user_likes[1].name ?? "", font: .boldSystemFont(ofSize: 12), color: .black)
            txt1.append(txt2)
            txt1.append(txt3)
        }
        let txt4 = UIViewController().customizeFont(string: "liked your video.", font: .systemFont(ofSize: 12), color: UIColor.red)
        txt1.append(txt4)
        self.lblComment.attributedText = txt1
        self.lblTime.text = UIViewController().stringToDate(date: dict.created_at ?? "")
    }
    func cellCommen() {
        guard let dict = likeData else {
            return
        }
        if let url = dict.user_profile?.convert_to_url {
            self.img_user.af.setImage(withURL: url)
        }
        self.img_video.isHidden = false
        self.img_video1.isHidden = false
        self.btn_View.isHidden = true
        self.heightConstraints.constant = 0
        if dict.user_follow_status == 0 {
            if dict.friend_follow_status == 1 {
            }else{
            }
            self.heightConstraints.constant = 0
            self.btn_View.isHidden = false
            self.heightConstraints.constant = 25
        }
        let name = dict.name ?? ""
        let message = dict.message ?? ""
        let txt1 = UIViewController().customizeFont(string: "\(name) ", font: .boldSystemFont(ofSize: 12), color: .black)
        let txt2 = UIViewController().customizeFont(string: "Commented: ", font: .systemFont(ofSize: 12), color: UIColor.red)
        let txt3 = UIViewController().customizeFont(string: "\"\(message)\"", font: .boldSystemFont(ofSize: 12), color: .black)
        txt1.append(txt2)
        txt1.append(txt3)
        self.lblComment.attributedText = txt1
        self.lblTime.text = UIViewController().stringToDate(date: dict.created_at ?? "")
    }
    func cellFollow() {
        guard let dict = likeData else {
            return
        }
        if let url = dict.user_profile?.convert_to_url {
            self.img_user.af.setImage(withURL: url)
        }
        self.img_video.isHidden = true
        self.img_video1.isHidden = true
        self.btn_View.isHidden = true
        self.heightConstraints.constant = 0
        if dict.user_follow_status == 0 {
            if dict.friend_follow_status == 1 {
            }else{
            }
            
            self.heightConstraints.constant = 0
            self.btn_View.isHidden = false
            self.heightConstraints.constant = 25
        }
        let name = dict.name ?? ""
        let txt1 = UIViewController().customizeFont(string: "\(name) ", font: .boldSystemFont(ofSize: 12), color: .black)
        let txt4 = UIViewController().customizeFont(string: "started following you.", font: .systemFont(ofSize: 12), color: UIColor.red)
        txt1.append(txt4)
        self.lblComment.attributedText = txt1
        self.lblTime.text = UIViewController().stringToDate(date: dict.created_at ?? "")
    }
}
