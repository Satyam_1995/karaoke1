//
//  FirstConnectCell.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 04/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit

class FirstConnectCell: UITableViewCell {

    @IBOutlet var lblName:UILabel!
    @IBOutlet var lblTime:UILabel!
    @IBOutlet var img:UIImageView!
    @IBOutlet var lblCommect:UILabel!
    @IBOutlet var btnLike:UIButton!
    @IBOutlet var btnReply:UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
