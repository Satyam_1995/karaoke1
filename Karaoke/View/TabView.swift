//
//  TabView.swift
//  Karaoke
//
//  Created by Ankur  on 09/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class TabView: UIView {
    
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var btnTab1: UIButton!
    @IBOutlet weak var btnTab2: UIButton!
    @IBOutlet weak var btnTab3: UIButton!
    @IBOutlet weak var btnTab4: UIButton!
    @IBOutlet weak var btnTab5: UIButton!
    @IBOutlet weak var imagTab1: UIImageView!
    @IBOutlet weak var imagTab2: UIImageView!
    @IBOutlet weak var imagTab3: UIImageView!
    @IBOutlet weak var imagTab4: UIImageView!
    @IBOutlet weak var imagTab5: UIImageView!
    
    static let identifier = "TabView"
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    
    private func commitInit(){
        Bundle.main.loadNibNamed("TabView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
