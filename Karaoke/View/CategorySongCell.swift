//
//  CategorySongCell.swift
//  Karaoke
//
//  Created by Ankur  on 09/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class CategorySongCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Category:UILabel!
    
    @IBOutlet weak var CategorySelected:UILabel!
}
