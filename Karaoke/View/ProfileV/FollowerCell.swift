//
//  FollowerCell.swift
//  Karaoke
//
//  Created by Ankur  on 20/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class FollowerCell: UITableViewCell {
    
    @IBOutlet weak var imgUSer:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lbltotalFollowers:UILabel!
    @IBOutlet weak var btnFollow:UIButton!

    var dict : Follower? {
        didSet {
            updateData()
        }
    }
    var dictFollowers : Follower? {
        didSet {
            updateFollowers()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func updateData() {
        if dict?.user_follow_status == 1 {
            self.btnFollow.setTitle("Unfollow", for: .normal)
            self.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
            self.btnFollow.setTitleColor(.darkGray, for: .normal)
        }else{
            self.btnFollow.setTitle("Follow", for: .normal)
            self.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
            self.btnFollow.setTitleColor(.white, for: .normal)
        }
    }
    func updateFollowers() {
        if dictFollowers?.user_follow_status != 1 {
            self.btnFollow.setTitle("Follow Back", for: .normal)
            self.btnFollow.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2588235294, blue: 0.3333333333, alpha: 1)
            self.btnFollow.setTitleColor(.white, for: .normal)
        }else{
            self.btnFollow.setTitle("Unfollow", for: .normal)
            self.btnFollow.backgroundColor = #colorLiteral(red: 0.9097240567, green: 0.9098549485, blue: 0.9096954465, alpha: 1)
            self.btnFollow.setTitleColor(.darkGray, for: .normal)
        }
    }
}
